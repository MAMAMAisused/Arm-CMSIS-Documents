/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS", "index.html", [
    [ "Introduction", "index.html", [
      [ "CMSIS Components", "index.html#CM_Components", null ],
      [ "Motivation", "index.html#Motivation", null ],
      [ "Coding Rules", "index.html#CodingRules", null ],
      [ "Validation", "index.html#Validation", null ],
      [ "License", "index.html#License", null ],
      [ "CMSIS Software Pack", "index.html#CM_Pack_Content", [
        [ "CMSIS Directory", "index.html#autotoc_md0", null ]
      ] ]
    ] ],
    [ "Revision History", "cm_revisionHistory.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"cm_revisionHistory.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';