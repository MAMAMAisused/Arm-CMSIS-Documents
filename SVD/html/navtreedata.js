/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS-SVD", "index.html", [
    [ "System View Description", "index.html", null ],
    [ "Revision History", "svd_revisionHistory.html", null ],
    [ "SVD File Validation and Usage", "svd_validate_file_pg.html", null ],
    [ "SVDConv", "svd_SVDConv_pg.html", [
      [ "Error and Warning Messages", "svd_SVDConv_pg.html#svdconvMessages", [
        [ "Introduction", "index.html#autotoc_md0", null ],
        [ "CMSIS-SVD Benefits", "index.html#autotoc_md1", null ],
        [ "Language Specification and Conventions", "index.html#autotoc_md2", null ],
        [ "CMSIS-SVD in ARM::CMSIS Pack", "index.html#autotoc_md3", null ],
        [ "Operation", "svd_SVDConv_pg.html#autotoc_md4", null ],
        [ "Return Codes", "svd_SVDConv_pg.html#autotoc_md5", null ],
        [ "Invocation Errors", "svd_SVDConv_pg.html#autotoc_md6", null ],
        [ "Informative messages", "svd_SVDConv_pg.html#autotoc_md7", null ],
        [ "Invocation errors", "svd_SVDConv_pg.html#autotoc_md8", null ],
        [ "Validation errors", "svd_SVDConv_pg.html#autotoc_md9", null ],
        [ "Data Check Errors", "svd_SVDConv_pg.html#autotoc_md10", null ],
        [ "Data modification errors", "svd_SVDConv_pg.html#autotoc_md11", null ]
      ] ]
    ] ],
    [ "SVD Description (*.svd) Format", "svd_Format_pg.html", "svd_Format_pg" ],
    [ "CMSIS-SVD Schema File", "schema_1_2_gr.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"elem_cpu.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';