var rtx__os_8h_structosRtxInfo__t =
[
    [ "os_id", "rtx__os_8h.html#a7b7d02cdf33432995469f29878f7dcda", null ],
    [ "version", "rtx__os_8h.html#acd99bb05ca015e7d74448acb1deba7ca", null ],
    [ "kernel", "rtx__os_8h.html#a4ec26f1d72aec2c763a9abe00e9f3fe6", null ],
    [ "tick_irqn", "rtx__os_8h.html#ad88a39594ffe6ab1016cf59517219e21", null ],
    [ "thread", "rtx__os_8h.html#a6f7fcc252ea4f38383023590423c5fd1", null ],
    [ "timer", "rtx__os_8h.html#a735b504b0ea76f1722a00f315a868341", null ],
    [ "isr_queue", "rtx__os_8h.html#ad7b45254899fd2bf838a2f048d87c128", null ],
    [ "post_process", "rtx__os_8h.html#a0fa46b4a47d4cdecfe1e408abb4560d4", null ],
    [ "mem", "rtx__os_8h.html#a1a5388e0428e9fe00eb33815b172d9f9", null ],
    [ "mpi", "rtx__os_8h.html#ac3284e150c128952ada2ed7538d11cce", null ]
];