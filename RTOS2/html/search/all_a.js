var searchData=
[
  ['kernel_20information_20and_20control_236',['Kernel Information and Control',['../group__CMSIS__RTOS__KernelCtrl.html',1,'']]],
  ['kernel_237',['kernel',['../group__CMSIS__RTOS__KernelCtrl.html#ad8075d3f42141346ae2c56389358f9e7',1,'osVersion_t::kernel()'],['../rtx__os_8h.html#a4ec26f1d72aec2c763a9abe00e9f3fe6',1,'osRtxInfo_t::kernel()']]],
  ['kernel_20functions_238',['Kernel Functions',['../group__rtx__evr__kernel.html',1,'']]]
];
