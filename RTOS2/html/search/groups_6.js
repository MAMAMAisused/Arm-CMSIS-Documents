var searchData=
[
  ['memory_20management_1239',['Memory Management',['../group__CMSIS__RTOS__MemoryMgmt.html',1,'']]],
  ['message_20queue_1240',['Message Queue',['../group__CMSIS__RTOS__Message.html',1,'']]],
  ['mutex_20management_1241',['Mutex Management',['../group__CMSIS__RTOS__MutexMgmt.html',1,'']]],
  ['memory_20pool_1242',['Memory Pool',['../group__CMSIS__RTOS__PoolMgmt.html',1,'']]],
  ['macros_1243',['Macros',['../group__rtx5__specific__defines.html',1,'']]],
  ['memory_20functions_1244',['Memory Functions',['../group__rtx__evr__memory.html',1,'']]],
  ['memory_20pool_20functions_1245',['Memory Pool Functions',['../group__rtx__evr__memory__pool.html',1,'']]],
  ['message_20queue_20functions_1246',['Message Queue Functions',['../group__rtx__evr__message__queue.html',1,'']]],
  ['mutex_20functions_1247',['Mutex Functions',['../group__rtx__evr__mutex.html',1,'']]]
];
