var searchData=
[
  ['sp_876',['sp',['../rtx__os_8h.html#a382a07e24a852d580d7548b8d517384c',1,'osRtxThread_s']]],
  ['stack_5fframe_877',['stack_frame',['../rtx__os_8h.html#a1071334ebd46d95b9ab1dcad788b2adc',1,'osRtxThread_s']]],
  ['stack_5fmem_878',['stack_mem',['../group__CMSIS__RTOS__ThreadMgmt.html#ad7c9b42355a4c8b9467130ab3fb19e43',1,'osThreadAttr_t::stack_mem()'],['../rtx__os_8h.html#a447dbb69b4c036e118b3b1b8bd22fe60',1,'osRtxThread_s::stack_mem()']]],
  ['stack_5fsize_879',['stack_size',['../group__CMSIS__RTOS__ThreadMgmt.html#aacbc9a219f2d6870e9ce89bb93f975c9',1,'osThreadAttr_t::stack_size()'],['../rtx__os_8h.html#ad63716408aae5b50857ca8ce74e3a3ff',1,'osRtxThread_s::stack_size()']]],
  ['state_880',['state',['../rtx__os_8h.html#a0b57aa10271a66f3dc936bba1d2f3830',1,'osRtxThread_s::state()'],['../rtx__os_8h.html#a0b57aa10271a66f3dc936bba1d2f3830',1,'osRtxTimer_s::state()'],['../rtx__os_8h.html#a0b57aa10271a66f3dc936bba1d2f3830',1,'osRtxObject_t::state()']]]
];
