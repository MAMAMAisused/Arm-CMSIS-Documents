var searchData=
[
  ['rtx_20migration_20guide_603',['RTX Migration Guide',['../os2MigrationGuide.html',1,'os2Migration']]],
  ['reserved_604',['reserved',['../group__CMSIS__RTOS__ThreadMgmt.html#a66f538babc389986738ec70104e0562b',1,'osThreadAttr_t::reserved()'],['../rtx__os_8h.html#acb7bc06bed6f6408d719334fc41698c7',1,'osRtxEventFlags_t::reserved()'],['../rtx__os_8h.html#acb7bc06bed6f6408d719334fc41698c7',1,'osRtxSemaphore_t::reserved()'],['../rtx__os_8h.html#acb7bc06bed6f6408d719334fc41698c7',1,'osRtxMemoryPool_t::reserved()'],['../rtx__os_8h.html#acb7bc06bed6f6408d719334fc41698c7',1,'osRtxMessageQueue_t::reserved()'],['../rtx__os_8h.html#acb7bc06bed6f6408d719334fc41698c7',1,'osRtxObject_t::reserved()']]],
  ['reserved_5fstate_605',['reserved_state',['../rtx__os_8h.html#a0acbb12a103775ddce0fb5d0efa3540f',1,'osRtxEventFlags_t::reserved_state()'],['../rtx__os_8h.html#a0acbb12a103775ddce0fb5d0efa3540f',1,'osRtxMutex_s::reserved_state()'],['../rtx__os_8h.html#a0acbb12a103775ddce0fb5d0efa3540f',1,'osRtxSemaphore_t::reserved_state()'],['../rtx__os_8h.html#a0acbb12a103775ddce0fb5d0efa3540f',1,'osRtxMemoryPool_t::reserved_state()'],['../rtx__os_8h.html#a0acbb12a103775ddce0fb5d0efa3540f',1,'osRtxMessage_s::reserved_state()'],['../rtx__os_8h.html#a0acbb12a103775ddce0fb5d0efa3540f',1,'osRtxMessageQueue_t::reserved_state()']]],
  ['robin_5ftimeout_606',['robin_timeout',['../rtx__os_8h.html#a6b4c66676a5bc985c28b71ac4ac79159',1,'osRtxConfig_t']]],
  ['revision_20history_607',['Revision History',['../rtos_revisionHistory.html',1,'index']]],
  ['rtos_20validation_608',['RTOS Validation',['../rtosValidation.html',1,'index']]],
  ['rtx_20v5_20implementation_609',['RTX v5 Implementation',['../rtx5_impl.html',1,'index']]],
  ['rtx_20v5_20specific_20api_610',['RTX v5 Specific API',['../group__rtx5__specific.html',1,'']]],
  ['rtx_5fevr_2eh_611',['rtx_evr.h',['../rtx__evr_8h.html',1,'']]],
  ['rtx_5fevr_2etxt_612',['rtx_evr.txt',['../rtx__evr_8txt.html',1,'']]],
  ['rtx_5fos_2eh_613',['rtx_os.h',['../rtx__os_8h.html',1,'']]],
  ['rtx_5fos_2etxt_614',['rtx_os.txt',['../rtx__os_8txt.html',1,'']]]
];
