var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuvw",
  1: "o",
  2: "chmorv",
  3: "ops",
  4: "abcdefiklmnoprstuvw",
  5: "iot",
  6: "o",
  7: "o",
  8: "_eiot",
  9: "cdefgkmorst",
  10: "bcdfghmorst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

