var searchData=
[
  ['definitions_38',['Definitions',['../group__CMSIS__RTOS__Definitions.html',1,'']]],
  ['delay_39',['delay',['../rtx__os_8h.html#a458421a43d4f6dc515faf427bf579d00',1,'osRtxThread_s']]],
  ['delay_5fnext_40',['delay_next',['../rtx__os_8h.html#a21a74cedec66715cd5d7422797035868',1,'osRtxThread_s']]],
  ['delay_5fprev_41',['delay_prev',['../rtx__os_8h.html#adcfea33cc536708db8a3419ba0295551',1,'osRtxThread_s']]],
  ['detailed_20api_20function_20differences_42',['Detailed API Function Differences',['../os2MigrationFunctions.html',1,'os2Migration']]],
  ['directory_20structure_20and_20file_20overview_43',['Directory Structure and File Overview',['../pDirectory_Files.html',1,'technicalData5']]]
];
