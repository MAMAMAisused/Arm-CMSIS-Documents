var group__rtx__evr =
[
    [ "Memory Functions", "group__rtx__evr__memory.html", null ],
    [ "Kernel Functions", "group__rtx__evr__kernel.html", "group__rtx__evr__kernel" ],
    [ "Thread Functions", "group__rtx__evr__thread.html", "group__rtx__evr__thread" ],
    [ "Generic Wait Functions", "group__rtx__evr__wait.html", null ],
    [ "Thread Flags Functions", "group__rtx__evr__thread__flags.html", null ],
    [ "Event Flags Functions", "group__rtx__evr__event__flags.html", null ],
    [ "Timer Functions", "group__rtx__evr__timer.html", null ],
    [ "Mutex Functions", "group__rtx__evr__mutex.html", null ],
    [ "Semaphore Functions", "group__rtx__evr__semaphore.html", null ],
    [ "Memory Pool Functions", "group__rtx__evr__memory__pool.html", null ],
    [ "Message Queue Functions", "group__rtx__evr__message__queue.html", null ]
];