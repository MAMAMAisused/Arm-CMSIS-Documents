var group__rtx__evr__kernel =
[
    [ "EvrRtxKernelInitialize", "group__rtx__evr__kernel.html#ga73d4a8c33e00b724b8b54a8014525828", null ],
    [ "EvrRtxKernelInitialized", "group__rtx__evr__kernel.html#ga7023b4bbbc244e93b15472a80918feda", null ],
    [ "EvrRtxKernelStart", "group__rtx__evr__kernel.html#ga9b8806b0413c4a65fe19c09e87577d08", null ],
    [ "EvrRtxKernelStarted", "group__rtx__evr__kernel.html#ga63fffdb2fab5ae1557a1e8f8777c9d5f", null ],
    [ "EvrRtxKernelLock", "group__rtx__evr__kernel.html#ga538a77dc4acbc85eb87149a69537bd0a", null ],
    [ "EvrRtxKernelUnlock", "group__rtx__evr__kernel.html#gabccc7d963dcdab18319ab42e28f68a7c", null ],
    [ "EvrRtxKernelSuspend", "group__rtx__evr__kernel.html#ga97ee99e99d0677dfb1b248e09e3fcb6c", null ],
    [ "EvrRtxKernelResumed", "group__rtx__evr__kernel.html#gaa13c735fe54c8301bdb32b33f53f2de1", null ]
];