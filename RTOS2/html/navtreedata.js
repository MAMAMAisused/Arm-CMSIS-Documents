/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS-RTOS2", "index.html", [
    [ "Revision History", "rtos_revisionHistory.html", [
      [ "CMSIS-RTOS2 in ARM::CMSIS Pack", "index.html#autotoc_md0", null ],
      [ "CMSIS-RTOS API Version 2", "rtos_revisionHistory.html#GenRTOS2Rev", null ],
      [ "CMSIS-RTOS RTX Version 5", "rtos_revisionHistory.html#RTX5RevisionHistory", null ]
    ] ],
    [ "Generic RTOS Interface", "genRTOS2IF.html", [
      [ "cmsis_os2.h header file", "genRTOS2IF.html#cmsis_os2_h", null ],
      [ "Using a CMSIS-RTOS2 Implementation", "genRTOS2IF.html#usingOS2", null ]
    ] ],
    [ "Function Overview", "functionOverview.html", "functionOverview" ],
    [ "RTOS Validation", "rtosValidation.html", [
      [ "Sample Test Output", "rtosValidation.html#test_output", null ]
    ] ],
    [ "Migration from API v1 to API v2", "os2Migration.html", "os2Migration" ],
    [ "RTX v5 Implementation", "rtx5_impl.html", "rtx5_impl" ],
    [ "Coding Rules", "CodingRules.html", null ],
    [ "Reference", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", "annotated" ],
    [ "Data Structure Index", "classes.html", null ],
    [ "Data Fields", "functions.html", [
      [ "All", "functions.html", null ],
      [ "Variables", "functions_vars.html", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"CodingRules.html",
"group__CMSIS__RTOS__ThreadMgmt.html#gad4e3e0971b41f2d17584a8c6837342ec"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';