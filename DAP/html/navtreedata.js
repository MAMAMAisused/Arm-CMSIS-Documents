/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS-DAP", "index.html", [
    [ "Firmware for CoreSight Debug Access Port", "index.html", [
      [ "CMSIS-DAP Firmware", "index.html#autotoc_md2", [
        [ "Configuration of CMSIS-DAP Firmware", "index.html#autotoc_md0", null ],
        [ "Benefits of CMSIS-DAP", "index.html#autotoc_md1", null ],
        [ "Validation", "index.html#autotoc_md3", null ]
      ] ]
    ] ],
    [ "Revision History of CMSIS-DAP", "dap_revisionHistory.html", null ],
    [ "Reference", "modules.html", "modules" ]
  ] ]
];

var NAVTREEINDEX =
[
"dap_revisionHistory.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';