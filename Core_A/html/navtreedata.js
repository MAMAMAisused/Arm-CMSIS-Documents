/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS-Core (Cortex-A)", "index.html", [
    [ "Overview", "index.html", [
      [ "Processor Support", "index.html#ref_v7A", [
        [ "CMSIS-Core (Cortex-A) in ARM::CMSIS Pack", "index.html#autotoc_md0", null ],
        [ "Cortex-A Technical Reference Manuals", "index.html#ref_man_ca_sec", null ]
      ] ],
      [ "Tested and Verified Toolchains", "index.html#tested_tools_sec", null ]
    ] ],
    [ "Revision History of CMSIS-Core (Cortex-A)", "rev_histCoreA.html", null ],
    [ "Using CMSIS in Embedded Applications", "using_pg.html", "using_pg" ],
    [ "CMSIS-Core Device Templates", "templates_pg.html", "templates_pg" ],
    [ "MISRA-C Deviations", "coreMISRA_Exceptions_pg.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Reference", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", "annotated" ],
    [ "Data Fields", "functions.html", [
      [ "All", "functions.html", "functions_dup" ],
      [ "Variables", "functions_vars.html", "functions_vars" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group__CMSIS__DFSR__BITS.html#ga7541052737038d737fd9fe00b9815140",
"group__PL1__timer__functions.html#ga341ae7d1ae29f4dc5dae6310fa453164",
"unionCNTP__CTL__Type.html#ae29d3dcafcb0fa0a873a651e5ad2fcf7"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';