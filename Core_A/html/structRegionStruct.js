var structRegionStruct =
[
    [ "domain", "structRegionStruct.html#abdfcd371daa9bf33f88a25f3b8cdeec0", null ],
    [ "e_t", "structRegionStruct.html#a490a04e6b4a2be661137dccc77f7ab3c", null ],
    [ "g_t", "structRegionStruct.html#aaab551b9b577914a2dd63381aa8574f6", null ],
    [ "inner_norm_t", "structRegionStruct.html#abb79a425b194f3b7019b828124f74d3e", null ],
    [ "mem_t", "structRegionStruct.html#a96eea15f88c7c48f2804d9dd62f13dc1", null ],
    [ "outer_norm_t", "structRegionStruct.html#a5f2d9c482e1a5e5ae18ce1e34b940be2", null ],
    [ "priv_t", "structRegionStruct.html#aca1e31dc5efb9db558fde5d0acc392c7", null ],
    [ "rg_t", "structRegionStruct.html#af459d58d720e3c319b3ef5e877d27a2d", null ],
    [ "sec_t", "structRegionStruct.html#ae8004f07bc61c36f27bd8b881e04b597", null ],
    [ "sh_t", "structRegionStruct.html#a7c73b178aa8eadb16dbe1aba75f3c5ad", null ],
    [ "user_t", "structRegionStruct.html#a0f45fe48c95fb9674571f6f085aa862e", null ],
    [ "xn_t", "structRegionStruct.html#a3d361ef2b9edc3c7a3aa672a284555f3", null ]
];