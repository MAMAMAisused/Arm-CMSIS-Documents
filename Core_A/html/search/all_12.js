var searchData=
[
  ['radis_733',['RADIS',['../unionACTLR__Type.html#a7921e6e73e0841402a5519f09e6e2ef3',1,'ACTLR_Type']]],
  ['raw_5fint_5fstatus_734',['RAW_INT_STATUS',['../structL2C__310__TypeDef.html#a404f8453b6df3aaf5f3db4ff9b658637',1,'L2C_310_TypeDef']]],
  ['read_735',['READ',['../core__ca_8h.html#ga2ee598252f996e4f96640b096291d280acb9be765f361bb7efb9073730aac92c6',1,'core_ca.h']]],
  ['ref_5fcache_2etxt_736',['ref_cache.txt',['../ref__cache_8txt.html',1,'']]],
  ['ref_5fcore_5fregister_2etxt_737',['ref_core_register.txt',['../ref__core__register_8txt.html',1,'']]],
  ['ref_5fgic_2etxt_738',['ref_gic.txt',['../ref__gic_8txt.html',1,'']]],
  ['ref_5fmmu_2etxt_739',['ref_mmu.txt',['../ref__mmu_8txt.html',1,'']]],
  ['ref_5fsystemandclock_2etxt_740',['Ref_SystemAndClock.txt',['../Ref__SystemAndClock_8txt.html',1,'']]],
  ['ref_5ftimer_2etxt_741',['ref_timer.txt',['../ref__timer_8txt.html',1,'']]],
  ['regionstruct_742',['RegionStruct',['../structRegionStruct.html',1,'']]],
  ['reserved_743',['RESERVED',['../core__ca_8h.html#af7f66fda711fd46e157dbb6c1af88e04',1,'core_ca.h']]],
  ['reset_5fhandler_744',['Reset_Handler',['../startup__ARMCA9_8c.html#ae7ee340978f5c25f52f0cad1457c6616',1,'startup_ARMCA9.c']]],
  ['revision_20history_20of_20cmsis_2dcore_20_28cortex_2da_29_745',['Revision History of CMSIS-Core (Cortex-A)',['../rev_histCoreA.html',1,'']]],
  ['rg_5ft_746',['rg_t',['../structRegionStruct.html#af459d58d720e3c319b3ef5e877d27a2d',1,'RegionStruct']]],
  ['rpr_747',['RPR',['../structGICInterface__Type.html#a37762d42768ecb3d1302f34abc7f2821',1,'GICInterface_Type']]],
  ['rr_748',['RR',['../unionSCTLR__Type.html#a10212a8d038bb1e076cbd06a5ba0b055',1,'SCTLR_Type']]],
  ['rsdis_749',['RSDIS',['../unionACTLR__Type.html#a91288f7320d267d76b4aad4adcf8cda3',1,'ACTLR_Type']]],
  ['rtclock_5firqn_750',['RTClock_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083abdd513b1533957e93fe0d7f26024d28e',1,'ARMCA9.h']]],
  ['rw_751',['RW',['../core__ca_8h.html#ga2ee598252f996e4f96640b096291d280aec2497e0c8af01c04bec31ec0d1d7847',1,'core_ca.h']]]
];
