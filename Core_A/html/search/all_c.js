var searchData=
[
  ['l_557',['l',['../unionDFSR__Type.html#abbd40ab8ba6a857bb5070b52bfbc1279',1,'DFSR_Type::l()'],['../unionIFSR__Type.html#a78eac026444ab17b547f61ca4b98c736',1,'IFSR_Type::l()']]],
  ['l1_20cache_20functions_558',['L1 Cache Functions',['../group__L1__cache__functions.html',1,'']]],
  ['l1c_5fcleandcacheall_559',['L1C_CleanDCacheAll',['../group__L1__cache__functions.html#ga70359d824bf26f376e3d7cb9c787da27',1,'core_ca.h']]],
  ['l1c_5fcleandcachemva_560',['L1C_CleanDCacheMVA',['../group__L1__cache__functions.html#ga9eb6f0a7c9c04cc49efd964eb59ba26f',1,'core_ca.h']]],
  ['l1c_5fcleaninvalidatecache_561',['L1C_CleanInvalidateCache',['../group__L1__cache__functions.html#ga30d7632156a30a3b75064f6d15b8f850',1,'core_ca.h']]],
  ['l1c_5fcleaninvalidatedcacheall_562',['L1C_CleanInvalidateDCacheAll',['../group__L1__cache__functions.html#ga92b5babf7317abe3815f61a2731735c3',1,'core_ca.h']]],
  ['l1c_5fcleaninvalidatedcachemva_563',['L1C_CleanInvalidateDCacheMVA',['../group__L1__cache__functions.html#ga7646a5e01b529566968f393e485f46a2',1,'core_ca.h']]],
  ['l1c_5fdisablebtac_564',['L1C_DisableBTAC',['../group__L1__cache__functions.html#gab8695cf1f4a7f3789b93c41dc4eeb51d',1,'core_ca.h']]],
  ['l1c_5fdisablecaches_565',['L1C_DisableCaches',['../group__L1__cache__functions.html#ga320ef6fd1dd65f2f82e64c096a4994a6',1,'core_ca.h']]],
  ['l1c_5fenablebtac_566',['L1C_EnableBTAC',['../group__L1__cache__functions.html#gaa5fb36b4496e64472849f7811970c581',1,'core_ca.h']]],
  ['l1c_5fenablecaches_567',['L1C_EnableCaches',['../group__L1__cache__functions.html#gaff8a4966eff1ada5cba80f2b689446db',1,'core_ca.h']]],
  ['l1c_5finvalidatebtac_568',['L1C_InvalidateBTAC',['../group__L1__cache__functions.html#gad0d732293be6a928db184b59aadc1979',1,'core_ca.h']]],
  ['l1c_5finvalidatedcacheall_569',['L1C_InvalidateDCacheAll',['../group__L1__cache__functions.html#gae895f75c4f3539058232f555d79e5df3',1,'core_ca.h']]],
  ['l1c_5finvalidatedcachemva_570',['L1C_InvalidateDCacheMVA',['../group__L1__cache__functions.html#ga9209853937940991daf70edd6bc633fe',1,'core_ca.h']]],
  ['l1c_5finvalidateicacheall_571',['L1C_InvalidateICacheAll',['../group__L1__cache__functions.html#gac932810cfe83f087590859010972645e',1,'core_ca.h']]],
  ['l1pctl_572',['L1PCTL',['../unionACTLR__Type.html#a5464ac7b26943d2cb868c154b0b1375c',1,'ACTLR_Type']]],
  ['l1pe_573',['L1PE',['../unionACTLR__Type.html#aacb87aa6bf093e1ee956342e0cb5903e',1,'ACTLR_Type']]],
  ['l1radis_574',['L1RADIS',['../unionACTLR__Type.html#a3800bdd7abfab1a51dcfa7069e245d65',1,'ACTLR_Type']]],
  ['l2c_2d310_20cache_20controller_20functions_575',['L2C-310 Cache Controller Functions',['../group__L2__cache__functions.html',1,'']]],
  ['l2c_5f310_576',['L2C_310',['../group__L2__cache__functions.html#ga3b08fba5b9be921c8a971231f75f8764',1,'core_ca.h']]],
  ['l2c_5f310_5fbase_577',['L2C_310_BASE',['../ARMCA9_8h.html#a5ce89d9feb78e20a4034f025eec392b4',1,'ARMCA9.h']]],
  ['l2c_5f310_5ftypedef_578',['L2C_310_TypeDef',['../structL2C__310__TypeDef.html',1,'']]],
  ['l2c_5fcleaninvallbyway_579',['L2C_CleanInvAllByWay',['../group__L2__cache__functions.html#gabd0a9b10926537fa283c0bb30d54abc7',1,'core_ca.h']]],
  ['l2c_5fcleaninvpa_580',['L2C_CleanInvPa',['../group__L2__cache__functions.html#gaaff11c6afa9eaacb4cdfcfe5c36f57eb',1,'core_ca.h']]],
  ['l2c_5fcleanpa_581',['L2C_CleanPa',['../group__L2__cache__functions.html#ga242f6fa13f33e7d5cdd7d92935d52f5f',1,'core_ca.h']]],
  ['l2c_5fdisable_582',['L2C_Disable',['../group__L2__cache__functions.html#ga66767e7f30f52d72de72231b2d6abd34',1,'core_ca.h']]],
  ['l2c_5fenable_583',['L2C_Enable',['../group__L2__cache__functions.html#ga720c36b4cd1d6c070ed0d2c49cffd7e1',1,'core_ca.h']]],
  ['l2c_5fgetid_584',['L2C_GetID',['../group__L2__cache__functions.html#ga75af64212e1d3d0b3ade860c365e95b3',1,'core_ca.h']]],
  ['l2c_5fgettype_585',['L2C_GetType',['../group__L2__cache__functions.html#ga0c334fa25720d77e78cfa187bdf833be',1,'core_ca.h']]],
  ['l2c_5finvallbyway_586',['L2C_InvAllByWay',['../group__L2__cache__functions.html#ga5b0ea2db52d137b5531ce568479c9d17',1,'core_ca.h']]],
  ['l2c_5finvpa_587',['L2C_InvPa',['../group__L2__cache__functions.html#ga4cf213e72c97776def35ab8223face82',1,'core_ca.h']]],
  ['l2c_5fsync_588',['L2C_Sync',['../group__L2__cache__functions.html#ga164c59c55e2d18bf8a94dc91c0f4ce68',1,'core_ca.h']]],
  ['l2radis_589',['L2RADIS',['../unionACTLR__Type.html#a947f73d64ebde186b9416fd6dc66bc26',1,'ACTLR_Type']]],
  ['load_590',['LOAD',['../structTimer__Type.html#a073457d2d18c2eff93fd12aec81ef20b',1,'Timer_Type']]],
  ['lock_5fline_5fen_591',['LOCK_LINE_EN',['../structL2C__310__TypeDef.html#a58357795f3cda2b0063411abc5165804',1,'L2C_310_TypeDef']]],
  ['lpae_592',['LPAE',['../unionDFSR__Type.html#add7c7800b87cabdb4a9ecdf41e4469a7',1,'DFSR_Type::LPAE()'],['../unionIFSR__Type.html#a40c5236caf0549cc1cc78945b0b0f131',1,'IFSR_Type::LPAE()']]]
];
