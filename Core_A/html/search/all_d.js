var searchData=
[
  ['multiprocessor_20affinity_20register_20_28mpidr_29_593',['Multiprocessor Affinity Register (MPIDR)',['../group__CMSIS__MPIDR.html',1,'']]],
  ['monitor_20vector_20base_20address_20register_20_28mvbar_29_594',['Monitor Vector Base Address Register (MVBAR)',['../group__CMSIS__MVBAR.html',1,'']]],
  ['misra_2dc_20deviations_595',['MISRA-C Deviations',['../coreMISRA_Exceptions_pg.html',1,'']]],
  ['m_596',['M',['../unionCPSR__Type.html#a2bc38ab81bc2e2fd111526a58f94511f',1,'CPSR_Type::M()'],['../unionSCTLR__Type.html#a8cbfde3ba235ebd48e82cb314c9b9cc4',1,'SCTLR_Type::M()']]],
  ['masked_5fint_5fstatus_597',['MASKED_INT_STATUS',['../structL2C__310__TypeDef.html#a207e1eb35e13440241db1109790d9740',1,'L2C_310_TypeDef']]],
  ['mci0_5firqn_598',['MCI0_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083aaa5c1873412fc0ef1f7e324208671ca2',1,'ARMCA9.h']]],
  ['mci1_5firqn_599',['MCI1_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083af803de749ceabdd07a62e90d2989a67e',1,'ARMCA9.h']]],
  ['mem_5farmca9_2eh_600',['mem_ARMCA9.h',['../mem__ARMCA9_8h.html',1,'']]],
  ['memory_20configuration_20files_20mem_5f_3cdevice_3e_2eh_601',['Memory Configuration Files mem_&lt;device&gt;.h',['../mem_h_pg.html',1,'templates_pg']]],
  ['mem_5ft_602',['mem_t',['../structRegionStruct.html#a96eea15f88c7c48f2804d9dd62f13dc1',1,'RegionStruct']]],
  ['misra_2etxt_603',['MISRA.txt',['../MISRA_8txt.html',1,'']]],
  ['mmu_5faccess_5ftype_604',['mmu_access_Type',['../group__MMU__defs__gr.html#ga2ee598252f996e4f96640b096291d280',1,'core_ca.h']]],
  ['mmu_5fappage_605',['MMU_APPage',['../group__MMU__functions.html#gac7c88d4d613350059b4d77814ea2c7a0',1,'core_ca.h']]],
  ['mmu_5fapsection_606',['MMU_APSection',['../group__MMU__functions.html#ga946866c84a72690c385ee07545bf8145',1,'core_ca.h']]],
  ['memory_20management_20unit_20files_20mmu_5f_3cdevice_3e_2ec_607',['Memory Management Unit Files mmu_&lt;device&gt;.c',['../mmu_c_pg.html',1,'templates_pg']]],
  ['mmu_5fcacheability_5ftype_608',['mmu_cacheability_Type',['../group__MMU__defs__gr.html#ga11c86b7b193efb2c59b6a2179a02f584',1,'core_ca.h']]],
  ['mmu_5fcreatetranslationtable_609',['MMU_CreateTranslationTable',['../system__ARMCA9_8h.html#a4f5b024323a24b2e195c0997b9c82e72',1,'system_ARMCA9.h']]],
  ['mmu_20defines_20and_20structs_610',['MMU Defines and Structs',['../group__MMU__defs__gr.html',1,'']]],
  ['mmu_5fdisable_611',['MMU_Disable',['../group__MMU__functions.html#ga2a2badd06531e04f559b97fdb2aea154',1,'core_ca.h']]],
  ['mmu_5fdomainpage_612',['MMU_DomainPage',['../group__MMU__functions.html#ga45f5389cb1351bb2806a38ac8c32d416',1,'core_ca.h']]],
  ['mmu_5fdomainsection_613',['MMU_DomainSection',['../group__MMU__functions.html#gabd88f4c41b74365c38209692785287d0',1,'core_ca.h']]],
  ['mmu_5fecc_5fcheck_5ftype_614',['mmu_ecc_check_Type',['../group__MMU__defs__gr.html#ga06d94c0eaa22d713636acaff81485409',1,'core_ca.h']]],
  ['mmu_5fenable_615',['MMU_Enable',['../group__MMU__functions.html#ga63334cbd77d310d078eb226c7542b96b',1,'core_ca.h']]],
  ['mmu_5fexecute_5ftype_616',['mmu_execute_Type',['../group__MMU__defs__gr.html#ga2fe1157deda82e66b9a1b19772309b63',1,'core_ca.h']]],
  ['memory_20management_20unit_20functions_617',['Memory Management Unit Functions',['../group__MMU__functions.html',1,'']]],
  ['mmu_5fgetpagedescriptor_618',['MMU_GetPageDescriptor',['../group__MMU__functions.html#gaa2fcfb63c7019665b8a352d54f55d740',1,'core_ca.h']]],
  ['mmu_5fgetsectiondescriptor_619',['MMU_GetSectionDescriptor',['../group__MMU__functions.html#ga4f21eee79309cf8cde694d0d7e1205bd',1,'core_ca.h']]],
  ['mmu_5fglobal_5ftype_620',['mmu_global_Type',['../group__MMU__defs__gr.html#ga04160605fbe20914c8ef020430684a30',1,'core_ca.h']]],
  ['mmu_5fglobalpage_621',['MMU_GlobalPage',['../group__MMU__functions.html#ga14dfeaf8983de57521aaa66c19dd43c9',1,'core_ca.h']]],
  ['mmu_5fglobalsection_622',['MMU_GlobalSection',['../group__MMU__functions.html#ga3ca22117a7f2d3c4d1cd1bf832cc4d2f',1,'core_ca.h']]],
  ['mmu_5finvalidatetlb_623',['MMU_InvalidateTLB',['../group__MMU__functions.html#ga9de65bea1cabf73dc4302e0e727cc8c3',1,'core_ca.h']]],
  ['mmu_5fmemory_5ftype_624',['mmu_memory_Type',['../group__MMU__defs__gr.html#ga83ac8de9263f89879079da521e86d5f2',1,'core_ca.h']]],
  ['mmu_5fmemorypage_625',['MMU_MemoryPage',['../group__MMU__functions.html#ga9a2946f7c93bcb05cdd20be691a54b8c',1,'core_ca.h']]],
  ['mmu_5fmemorysection_626',['MMU_MemorySection',['../group__MMU__functions.html#ga353d3d794bcd1b35b3b5aeb73d6feb08',1,'core_ca.h']]],
  ['mmu_5fppage_627',['MMU_PPage',['../group__MMU__functions.html#gab15289c416609cd56dde816b39a4cea4',1,'core_ca.h']]],
  ['mmu_5fpsection_628',['MMU_PSection',['../group__MMU__functions.html#ga3577aec23189228c9f95abba50c3716d',1,'core_ca.h']]],
  ['mmu_5fregion_5fattributes_5ftype_629',['mmu_region_attributes_Type',['../structmmu__region__attributes__Type.html',1,'']]],
  ['mmu_5fregion_5fsize_5ftype_630',['mmu_region_size_Type',['../group__MMU__defs__gr.html#gab184b824a6d7cb728bd46c6abcd0c21a',1,'core_ca.h']]],
  ['mmu_5fsecure_5ftype_631',['mmu_secure_Type',['../group__MMU__defs__gr.html#gac3d277641df9fb3bb3b555e2e79dd639',1,'core_ca.h']]],
  ['mmu_5fsecurepage_632',['MMU_SecurePage',['../group__MMU__functions.html#ga2c1887ed6aaff0a51e3effc3db595c94',1,'core_ca.h']]],
  ['mmu_5fsecuresection_633',['MMU_SecureSection',['../group__MMU__functions.html#ga84a5a15ee353d70a9b904e3814bd94d8',1,'core_ca.h']]],
  ['mmu_5fshared_5ftype_634',['mmu_shared_Type',['../group__MMU__defs__gr.html#gab884a11fa8d094573ab77fb1c0f8d8a7',1,'core_ca.h']]],
  ['mmu_5fsharedpage_635',['MMU_SharedPage',['../group__MMU__functions.html#gaaa19560532778e4fdc667e56fd2dd378',1,'core_ca.h']]],
  ['mmu_5fsharedsection_636',['MMU_SharedSection',['../group__MMU__functions.html#ga29ea426394746cdd6a4b4c14164ec6b9',1,'core_ca.h']]],
  ['mmu_5fttpage4k_637',['MMU_TTPage4k',['../group__MMU__functions.html#ga823cca9649a28bab8a90f8bd9bb92d83',1,'core_ca.h']]],
  ['mmu_5fttpage64k_638',['MMU_TTPage64k',['../group__MMU__functions.html#ga48c509501f94a3f7316e79f8ccd34184',1,'core_ca.h']]],
  ['mmu_5fttsection_639',['MMU_TTSection',['../group__MMU__functions.html#gaaff28ea191391cbbd389d74327961753',1,'core_ca.h']]],
  ['mmu_5fxnpage_640',['MMU_XNPage',['../group__MMU__functions.html#gab0e0fed40d998757147beb8fcf05a890',1,'core_ca.h']]],
  ['mmu_5fxnsection_641',['MMU_XNSection',['../group__MMU__functions.html#ga9132cbfe3b2367de3db27daf4cc82ad7',1,'core_ca.h']]],
  ['mouse_5firqn_642',['Mouse_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083affc38a882d7c20bf505a160bc5eb3851',1,'ARMCA9.h']]]
];
