var searchData=
[
  ['pl1_20physical_20timer_20control_20register_20_28cntp_5fctl_29_1603',['PL1 Physical Timer Control register (CNTP_CTL)',['../group__CMSIS__CNTP__CTL.html',1,'']]],
  ['pl1_20physical_20timer_20compare_20value_20register_20_28cntp_5fcval_29_1604',['PL1 Physical Timer Compare Value register (CNTP_CVAL)',['../group__CMSIS__CNTP__CVAL.html',1,'']]],
  ['pl1_20physical_20timer_20value_20register_20_28cntp_5ftval_29_1605',['PL1 Physical Timer Value register (CNTP_TVAL)',['../group__CMSIS__CNTP__TVAL.html',1,'']]],
  ['pl1_20physical_20count_20register_20_28cntpct_29_1606',['PL1 Physical Count register (CNTPCT)',['../group__CMSIS__CNTPCT.html',1,'']]],
  ['peripheral_20access_1607',['Peripheral Access',['../group__peripheral__gr.html',1,'']]],
  ['private_20timer_20functions_1608',['Private Timer Functions',['../group__PTM__timer__functions.html',1,'']]]
];
