var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxz",
  1: "acdfgilmrst",
  2: "acimorstu",
  3: "_dgilmprsv",
  4: "_abcdefghijlmnopqrstuvwxz",
  5: "i",
  6: "im",
  7: "acdegkmnprstuvw",
  8: "_acfgilprstuv",
  9: "acdfgilmpstv",
  10: "bcdmorsu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

