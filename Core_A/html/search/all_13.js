var searchData=
[
  ['system_20control_20register_20_28sctlr_29_752',['System Control Register (SCTLR)',['../group__CMSIS__SCTLR.html',1,'']]],
  ['sctlr_20bits_753',['SCTLR Bits',['../group__CMSIS__SCTLR__BITS.html',1,'']]],
  ['stack_20pointer_20_28sp_2fr13_29_754',['Stack Pointer (SP/R13)',['../group__CMSIS__SP.html',1,'']]],
  ['s_755',['s',['../unionDFSR__Type.html#aff02e18890a8cf5391ab43e805a6426a',1,'DFSR_Type::s()'],['../unionIFSR__Type.html#a573ca75e549bc9880636615e118f88fb',1,'IFSR_Type::s()']]],
  ['sctlr_5fa_5fmsk_756',['SCTLR_A_Msk',['../group__CMSIS__SCTLR__BITS.html#ga678c919832272745678213e55211e741',1,'core_ca.h']]],
  ['sctlr_5fa_5fpos_757',['SCTLR_A_Pos',['../group__CMSIS__SCTLR__BITS.html#ga0d667a307e974515ebc15b5249f34146',1,'core_ca.h']]],
  ['sctlr_5fafe_5fmsk_758',['SCTLR_AFE_Msk',['../group__CMSIS__SCTLR__BITS.html#ga9016d6e50562d2584c1f1a95bde1e957',1,'core_ca.h']]],
  ['sctlr_5fafe_5fpos_759',['SCTLR_AFE_Pos',['../group__CMSIS__SCTLR__BITS.html#ga4ac80ef4db2641dc9e6e8df0825a151e',1,'core_ca.h']]],
  ['sctlr_5fb_5fmsk_760',['SCTLR_B_Msk',['../group__CMSIS__SCTLR__BITS.html#ga4853d6f9ccbf919fcdadb0b2a5913cc6',1,'core_ca.h']]],
  ['sctlr_5fb_5fpos_761',['SCTLR_B_Pos',['../group__CMSIS__SCTLR__BITS.html#ga5f185efbe1a9eb5738b2573f076a0859',1,'core_ca.h']]],
  ['sctlr_5fc_5fmsk_762',['SCTLR_C_Msk',['../group__CMSIS__SCTLR__BITS.html#ga2be72788d984153ded81711e20fd2d33',1,'core_ca.h']]],
  ['sctlr_5fc_5fpos_763',['SCTLR_C_Pos',['../group__CMSIS__SCTLR__BITS.html#ga8a0394c5147b8212767087e3421deffa',1,'core_ca.h']]],
  ['sctlr_5fcp15ben_5fmsk_764',['SCTLR_CP15BEN_Msk',['../group__CMSIS__SCTLR__BITS.html#ga5541a6a63db4d4d233b8f57b1d46fbac',1,'core_ca.h']]],
  ['sctlr_5fcp15ben_5fpos_765',['SCTLR_CP15BEN_Pos',['../group__CMSIS__SCTLR__BITS.html#gace284f69e1a810957665adf0cb2e4b2b',1,'core_ca.h']]],
  ['sctlr_5fee_5fmsk_766',['SCTLR_EE_Msk',['../group__CMSIS__SCTLR__BITS.html#ga8d95cd61bc40dc77f8855f40c797d044',1,'core_ca.h']]],
  ['sctlr_5fee_5fpos_767',['SCTLR_EE_Pos',['../group__CMSIS__SCTLR__BITS.html#ga0baec19421bd41277c5d8783c59942fa',1,'core_ca.h']]],
  ['sctlr_5ffi_5fmsk_768',['SCTLR_FI_Msk',['../group__CMSIS__SCTLR__BITS.html#ga316b80925b88fe3b88ec46a55655b0bc',1,'core_ca.h']]],
  ['sctlr_5ffi_5fpos_769',['SCTLR_FI_Pos',['../group__CMSIS__SCTLR__BITS.html#gad88d563fa9a8b09fe36702a5329b0360',1,'core_ca.h']]],
  ['sctlr_5fha_5fmsk_770',['SCTLR_HA_Msk',['../group__CMSIS__SCTLR__BITS.html#ga6830e9bf54a6b548f329ac047f59c179',1,'core_ca.h']]],
  ['sctlr_5fha_5fpos_771',['SCTLR_HA_Pos',['../group__CMSIS__SCTLR__BITS.html#ga316882abba6c9cdd31dbbd7ba46c9f52',1,'core_ca.h']]],
  ['sctlr_5fi_5fmsk_772',['SCTLR_I_Msk',['../group__CMSIS__SCTLR__BITS.html#gab3cc0744fb07127e3c0f18cba9d51666',1,'core_ca.h']]],
  ['sctlr_5fi_5fpos_773',['SCTLR_I_Pos',['../group__CMSIS__SCTLR__BITS.html#gaaaa818a1da51059bd979f0e768ebcc7c',1,'core_ca.h']]],
  ['sctlr_5fm_5fmsk_774',['SCTLR_M_Msk',['../group__CMSIS__SCTLR__BITS.html#gaf460824cdbf549bd914aa79762572e8e',1,'core_ca.h']]],
  ['sctlr_5fm_5fpos_775',['SCTLR_M_Pos',['../group__CMSIS__SCTLR__BITS.html#ga88e34078fa8cf719aab6f53f138c9810',1,'core_ca.h']]],
  ['sctlr_5fnmfi_5fmsk_776',['SCTLR_NMFI_Msk',['../group__CMSIS__SCTLR__BITS.html#gab92a3bd63ad9ac3d408e1b615bedc279',1,'core_ca.h']]],
  ['sctlr_5fnmfi_5fpos_777',['SCTLR_NMFI_Pos',['../group__CMSIS__SCTLR__BITS.html#gac1cf872c51ed0baa6ed23e26c1ed35a9',1,'core_ca.h']]],
  ['sctlr_5frr_5fmsk_778',['SCTLR_RR_Msk',['../group__CMSIS__SCTLR__BITS.html#ga1ff9e6766c7e1ca312b025bf34d384bc',1,'core_ca.h']]],
  ['sctlr_5frr_5fpos_779',['SCTLR_RR_Pos',['../group__CMSIS__SCTLR__BITS.html#ga86e5b78ba8f818061644688db75ddc64',1,'core_ca.h']]],
  ['sctlr_5fsw_5fmsk_780',['SCTLR_SW_Msk',['../group__CMSIS__SCTLR__BITS.html#gae4074aefcf01786fe199c82e273271b8',1,'core_ca.h']]],
  ['sctlr_5fsw_5fpos_781',['SCTLR_SW_Pos',['../group__CMSIS__SCTLR__BITS.html#ga3290be0882c1493bca9a0db6b4d0bff8',1,'core_ca.h']]],
  ['sctlr_5fte_5fmsk_782',['SCTLR_TE_Msk',['../group__CMSIS__SCTLR__BITS.html#ga4a68d6660c76951ada2541ceaf040b3b',1,'core_ca.h']]],
  ['sctlr_5fte_5fpos_783',['SCTLR_TE_Pos',['../group__CMSIS__SCTLR__BITS.html#gab0a611e2359e04624379e1ddd4dc64b1',1,'core_ca.h']]],
  ['sctlr_5ftre_5fmsk_784',['SCTLR_TRE_Msk',['../group__CMSIS__SCTLR__BITS.html#gab0481eb9812a4908601cb20c8ae84918',1,'core_ca.h']]],
  ['sctlr_5ftre_5fpos_785',['SCTLR_TRE_Pos',['../group__CMSIS__SCTLR__BITS.html#gaf76fa48119363f9b88c2c8f5b74e0a04',1,'core_ca.h']]],
  ['sctlr_5ftype_786',['SCTLR_Type',['../unionSCTLR__Type.html',1,'']]],
  ['sctlr_5fu_5fmsk_787',['SCTLR_U_Msk',['../group__CMSIS__SCTLR__BITS.html#gaa047daa7ab35b5ad5dd238c7377a232f',1,'core_ca.h']]],
  ['sctlr_5fu_5fpos_788',['SCTLR_U_Pos',['../group__CMSIS__SCTLR__BITS.html#gaa0431730d7ce929db03d8accee558e17',1,'core_ca.h']]],
  ['sctlr_5fuwxn_5fmsk_789',['SCTLR_UWXN_Msk',['../group__CMSIS__SCTLR__BITS.html#gab834e64e0da7c2a98d747ce73252c199',1,'core_ca.h']]],
  ['sctlr_5fuwxn_5fpos_790',['SCTLR_UWXN_Pos',['../group__CMSIS__SCTLR__BITS.html#ga7c7d88f3db4de438ddd069cf3fbc88b3',1,'core_ca.h']]],
  ['sctlr_5fv_5fmsk_791',['SCTLR_V_Msk',['../group__CMSIS__SCTLR__BITS.html#gaf84f3f15bf6917acdc5b5a4ad661ac11',1,'core_ca.h']]],
  ['sctlr_5fv_5fpos_792',['SCTLR_V_Pos',['../group__CMSIS__SCTLR__BITS.html#ga57778fd6afbe5b4fe8d8ea828acf833d',1,'core_ca.h']]],
  ['sctlr_5fve_5fmsk_793',['SCTLR_VE_Msk',['../group__CMSIS__SCTLR__BITS.html#gad94a7feadba850299a68c56e39c0b274',1,'core_ca.h']]],
  ['sctlr_5fve_5fpos_794',['SCTLR_VE_Pos',['../group__CMSIS__SCTLR__BITS.html#ga1372b569553a0740d881e24c0be7334f',1,'core_ca.h']]],
  ['sctlr_5fwxn_5fmsk_795',['SCTLR_WXN_Msk',['../group__CMSIS__SCTLR__BITS.html#ga510b03214d135f15ad3c5d41ec20a291',1,'core_ca.h']]],
  ['sctlr_5fwxn_5fpos_796',['SCTLR_WXN_Pos',['../group__CMSIS__SCTLR__BITS.html#gaf145654986fd6d014136580ad279d256',1,'core_ca.h']]],
  ['sctlr_5fz_5fmsk_797',['SCTLR_Z_Msk',['../group__CMSIS__SCTLR__BITS.html#ga12a05acdcb8db6e99970f26206d3067c',1,'core_ca.h']]],
  ['sctlr_5fz_5fpos_798',['SCTLR_Z_Pos',['../group__CMSIS__SCTLR__BITS.html#gaa0eade648c9a34de891af0e6f47857dd',1,'core_ca.h']]],
  ['sec_5ft_799',['sec_t',['../structRegionStruct.html#ae8004f07bc61c36f27bd8b881e04b597',1,'RegionStruct']]],
  ['section_800',['SECTION',['../core__ca_8h.html#gab184b824a6d7cb728bd46c6abcd0c21aacb7227be6a36b93e485b62e3acddae51',1,'core_ca.h']]],
  ['section_5fap2_5fshift_801',['SECTION_AP2_SHIFT',['../group__MMU__defs__gr.html#ga1b8b0d00bfc7cbeed67b82db26d98195',1,'core_ca.h']]],
  ['section_5fap_5fmask_802',['SECTION_AP_MASK',['../core__ca_8h.html#a725efc96ea9aa940fefcf013bce6ca8c',1,'core_ca.h']]],
  ['section_5fap_5fshift_803',['SECTION_AP_SHIFT',['../group__MMU__defs__gr.html#ga274fa608581b227182ce92adec4597b5',1,'core_ca.h']]],
  ['section_5fb_5fshift_804',['SECTION_B_SHIFT',['../group__MMU__defs__gr.html#gaa77545190c32bb2f4d2d86e41552daef',1,'core_ca.h']]],
  ['section_5fc_5fshift_805',['SECTION_C_SHIFT',['../group__MMU__defs__gr.html#gae0b3a2eccc4f9c249e928d359c43c20c',1,'core_ca.h']]],
  ['section_5fdescriptor_806',['SECTION_DESCRIPTOR',['../group__MMU__defs__gr.html#ga4ab4ff3ff904df46da18f5532ceb1e89',1,'core_ca.h']]],
  ['section_5fdevice_5fro_807',['section_device_ro',['../group__MMU__defs__gr.html#ga1f66b52e152895af070514528763c272',1,'core_ca.h']]],
  ['section_5fdevice_5frw_808',['section_device_rw',['../group__MMU__defs__gr.html#ga33c6ad1fc06648fe50f8b21554c9bccb',1,'core_ca.h']]],
  ['section_5fdomain_5fmask_809',['SECTION_DOMAIN_MASK',['../core__ca_8h.html#a90a30c02512cbea24791212af9f2cd9f',1,'core_ca.h']]],
  ['section_5fdomain_5fshift_810',['SECTION_DOMAIN_SHIFT',['../group__MMU__defs__gr.html#ga70cc38b984789323feecd97033a66757',1,'core_ca.h']]],
  ['section_5fmask_811',['SECTION_MASK',['../core__ca_8h.html#a16f225cca51a80c5cf1c9c002cfd2dba',1,'core_ca.h']]],
  ['section_5fng_5fmask_812',['SECTION_NG_MASK',['../core__ca_8h.html#a01ceacdb3888d7cddcfeccfea9eb3658',1,'core_ca.h']]],
  ['section_5fng_5fshift_813',['SECTION_NG_SHIFT',['../group__MMU__defs__gr.html#ga7af8adbf033d0a5c7b0889dd085041d1',1,'core_ca.h']]],
  ['section_5fnormal_814',['section_normal',['../group__MMU__defs__gr.html#ga220aab449cf3716723979d06666c2ebf',1,'core_ca.h']]],
  ['section_5fnormal_5fcod_815',['section_normal_cod',['../group__MMU__defs__gr.html#gad598239f9bb9b6ae2bec8278305640b4',1,'core_ca.h']]],
  ['section_5fnormal_5fnc_816',['section_normal_nc',['../core__ca_8h.html#a470b88645153aad94b09485f3108c641',1,'core_ca.h']]],
  ['section_5fnormal_5fro_817',['section_normal_ro',['../group__MMU__defs__gr.html#gaf95fa76d8f0f7ccfd2ebc00860af4f1d',1,'core_ca.h']]],
  ['section_5fnormal_5frw_818',['section_normal_rw',['../group__MMU__defs__gr.html#ga1f2ce84e6ec5c150a2ffc05092ea6d0e',1,'core_ca.h']]],
  ['section_5fns_5fmask_819',['SECTION_NS_MASK',['../core__ca_8h.html#a057533871fa1af6db7a27b39d976ac95',1,'core_ca.h']]],
  ['section_5fns_5fshift_820',['SECTION_NS_SHIFT',['../group__MMU__defs__gr.html#ga502d55a107c909e15be282d8fbe4a8ce',1,'core_ca.h']]],
  ['section_5fp_5fmask_821',['SECTION_P_MASK',['../core__ca_8h.html#ad32d146d84a9d7f964f28f1dadc98bcb',1,'core_ca.h']]],
  ['section_5fp_5fshift_822',['SECTION_P_SHIFT',['../group__MMU__defs__gr.html#ga8f27fa21cb70abad114374f33a562988',1,'core_ca.h']]],
  ['section_5fs_5fmask_823',['SECTION_S_MASK',['../core__ca_8h.html#a42d3645aad501af4ef447186c01685b7',1,'core_ca.h']]],
  ['section_5fs_5fshift_824',['SECTION_S_SHIFT',['../group__MMU__defs__gr.html#ga83a5fc538dad79161b122fb164d630fe',1,'core_ca.h']]],
  ['section_5fso_825',['section_so',['../group__MMU__defs__gr.html#gaf77ecb86097e6e8cf5f6c7bb9d2740c9',1,'core_ca.h']]],
  ['section_5ftex0_5fshift_826',['SECTION_TEX0_SHIFT',['../group__MMU__defs__gr.html#gad84432cb37ae093f7609f8f29f42c1f4',1,'core_ca.h']]],
  ['section_5ftex1_5fshift_827',['SECTION_TEX1_SHIFT',['../group__MMU__defs__gr.html#ga531cafc5eca8ade67a6fb83b35f8520e',1,'core_ca.h']]],
  ['section_5ftex2_5fshift_828',['SECTION_TEX2_SHIFT',['../group__MMU__defs__gr.html#ga8a6d854746a9c0049f9a91188092a55f',1,'core_ca.h']]],
  ['section_5ftexcb_5fmask_829',['SECTION_TEXCB_MASK',['../core__ca_8h.html#a3052ba3d97ad157189a6c6fce15b1b6a',1,'core_ca.h']]],
  ['section_5fxn_5fmask_830',['SECTION_XN_MASK',['../core__ca_8h.html#a83cb551c9fa708e33082c682be614334',1,'core_ca.h']]],
  ['section_5fxn_5fshift_831',['SECTION_XN_SHIFT',['../group__MMU__defs__gr.html#ga6cdc2db0ca695fd1191305a13e66c0a7',1,'core_ca.h']]],
  ['secure_832',['SECURE',['../core__ca_8h.html#gac3d277641df9fb3bb3b555e2e79dd639aa9dea2ba3f45f7d12b274eb6ab7d28d9',1,'core_ca.h']]],
  ['setspi_5fnsr_833',['SETSPI_NSR',['../structGICDistributor__Type.html#afbdd372578e2cd6f998320282cc8ed25',1,'GICDistributor_Type']]],
  ['setspi_5fsr_834',['SETSPI_SR',['../structGICDistributor__Type.html#ad55a8644bc95caf8bf53e1407ec9ed0c',1,'GICDistributor_Type']]],
  ['sgi0_5firqn_835',['SGI0_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083a056f32088a9c8bdb9309b005dfeb648e',1,'ARMCA9.h']]],
  ['sgi10_5firqn_836',['SGI10_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083a12993bed484c7a70e6281b102d0e27e9',1,'ARMCA9.h']]],
  ['sgi11_5firqn_837',['SGI11_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083a7826ded51cd379774bb076819ff93cdb',1,'ARMCA9.h']]],
  ['sgi12_5firqn_838',['SGI12_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083a3425bbf0a6da4d0398e63b48a1345d37',1,'ARMCA9.h']]],
  ['sgi13_5firqn_839',['SGI13_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083ace710506c7be1b3b7f9d4a1db2f75391',1,'ARMCA9.h']]],
  ['sgi14_5firqn_840',['SGI14_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083ae57c57a817378102db7bc66351c912f1',1,'ARMCA9.h']]],
  ['sgi15_5firqn_841',['SGI15_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083ac6958eebc9d41a42c739de555cad2321',1,'ARMCA9.h']]],
  ['sgi1_5firqn_842',['SGI1_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083ab335b8b84021cd5714807d6cd2404c3b',1,'ARMCA9.h']]],
  ['sgi2_5firqn_843',['SGI2_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083a2a1cc64c0a2dc0e7f339fbf21c9a2b07',1,'ARMCA9.h']]],
  ['sgi3_5firqn_844',['SGI3_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083a310ac1f78af36e0e3b9f6b4f15bd9b68',1,'ARMCA9.h']]],
  ['sgi4_5firqn_845',['SGI4_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083ac5c03a99c620ff116bafa4cf03dd9a07',1,'ARMCA9.h']]],
  ['sgi5_5firqn_846',['SGI5_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083adb8d49885011a278ed3c671904da7e6e',1,'ARMCA9.h']]],
  ['sgi6_5firqn_847',['SGI6_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083a5f9b1989d051c60ad69147e644853a44',1,'ARMCA9.h']]],
  ['sgi7_5firqn_848',['SGI7_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083aa1bcd760176e11cdece4386818022631',1,'ARMCA9.h']]],
  ['sgi8_5firqn_849',['SGI8_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083aabbb7ca9433c474bfeade468e8c66455',1,'ARMCA9.h']]],
  ['sgi9_5firqn_850',['SGI9_IRQn',['../ARMCA9_8h.html#a666eb0caeb12ec0e281415592ae89083afa5f21d9fd3df5817a0f871b72bde681',1,'ARMCA9.h']]],
  ['sgir_851',['SGIR',['../structGICDistributor__Type.html#a6ac65c4a5394926cc9518753a00d4da1',1,'GICDistributor_Type']]],
  ['sh_5ft_852',['sh_t',['../structRegionStruct.html#a7c73b178aa8eadb16dbe1aba75f3c5ad',1,'RegionStruct']]],
  ['shared_853',['SHARED',['../core__ca_8h.html#gab884a11fa8d094573ab77fb1c0f8d8a7a9c46e16a4ab019339596acadeefc8c53',1,'core_ca.h']]],
  ['shared_5fdevice_854',['SHARED_DEVICE',['../core__ca_8h.html#ga83ac8de9263f89879079da521e86d5f2a9b78345535e6af3288cc69a572338808',1,'core_ca.h']]],
  ['smp_855',['SMP',['../unionACTLR__Type.html#afa360e0c6bf79094d72bc78fac300149',1,'ACTLR_Type']]],
  ['spendsgir_856',['SPENDSGIR',['../structGICDistributor__Type.html#ae40b4a50d9766c2bbf57441f68094f41',1,'GICDistributor_Type']]],
  ['startup_5farmca9_2ec_857',['startup_ARMCA9.c',['../startup__ARMCA9_8c.html',1,'']]],
  ['startup_20file_20startup_5f_3cdevice_3e_2ec_858',['Startup File startup_&lt;device&gt;.c',['../startup_c_pg.html',1,'templates_pg']]],
  ['status_859',['STATUS',['../unionDFSR__Type.html#a4cb3ba7b8c8075bfbff792b7e5b88103',1,'DFSR_Type::STATUS()'],['../unionIFSR__Type.html#a543066fc60d5b63478cc85ba082524d4',1,'IFSR_Type::STATUS()']]],
  ['statusr_860',['STATUSR',['../structGICDistributor__Type.html#ae24f260e27065660a2059803293084f2',1,'GICDistributor_Type::STATUSR()'],['../structGICInterface__Type.html#abd978b408fb69b7887be2c422f48ce7e',1,'GICInterface_Type::STATUSR()']]],
  ['strongly_5fordered_861',['STRONGLY_ORDERED',['../core__ca_8h.html#ga83ac8de9263f89879079da521e86d5f2a0a4d347de23312717e6e57b04f0b014e',1,'core_ca.h']]],
  ['svc_5fmode_862',['SVC_MODE',['../startup__ARMCA9_8c.html#a312ff1d5550cc17c9907a153b04e0efa',1,'startup_ARMCA9.c']]],
  ['sw_863',['SW',['../unionSCTLR__Type.html#a6598f817304ccaef4509843ce041de1c',1,'SCTLR_Type']]],
  ['sys_5fmode_864',['SYS_MODE',['../startup__ARMCA9_8c.html#ae62d91253c85a3dff6a838244f460f70',1,'startup_ARMCA9.c']]],
  ['system_5farmca9_2eh_865',['system_ARMCA9.h',['../system__ARMCA9_8h.html',1,'']]],
  ['system_20configuration_20files_20system_5f_3cdevice_3e_2ec_20and_20system_5f_3cdevice_3e_2eh_866',['System Configuration Files system_&lt;device&gt;.c and system_&lt;device&gt;.h',['../system_c_pg.html',1,'templates_pg']]],
  ['system_20and_20clock_20configuration_867',['System and Clock Configuration',['../group__system__init__gr.html',1,'']]],
  ['systemcoreclock_868',['SystemCoreClock',['../group__system__init__gr.html#gaa3cd3e43291e81e795d642b79b6088e6',1,'SystemCoreClock():&#160;Ref_SystemAndClock.txt'],['../group__system__init__gr.html#gaa3cd3e43291e81e795d642b79b6088e6',1,'SystemCoreClock():&#160;Ref_SystemAndClock.txt']]],
  ['systemcoreclockupdate_869',['SystemCoreClockUpdate',['../group__system__init__gr.html#gae0c36a9591fe6e9c45ecb21a794f0f0f',1,'SystemCoreClockUpdate(void):&#160;Ref_SystemAndClock.txt'],['../system__ARMCA9_8h.html#ae0c36a9591fe6e9c45ecb21a794f0f0f',1,'SystemCoreClockUpdate(void):&#160;system_ARMCA9.h']]],
  ['systeminit_870',['SystemInit',['../group__system__init__gr.html#ga93f514700ccf00d08dbdcff7f1224eb2',1,'SystemInit(void):&#160;Ref_SystemAndClock.txt'],['../system__ARMCA9_8h.html#a93f514700ccf00d08dbdcff7f1224eb2',1,'SystemInit(void):&#160;system_ARMCA9.h']]]
];
