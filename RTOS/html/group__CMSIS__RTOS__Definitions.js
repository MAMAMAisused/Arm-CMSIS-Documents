var group__CMSIS__RTOS__Definitions =
[
    [ "osEvent", "group__CMSIS__RTOS__Definitions.html#structosEvent", [
      [ "def", "group__CMSIS__RTOS__Definitions.html#a78a8135990cb976d9383dfdc0c754e22", null ],
      [ "status", "group__CMSIS__RTOS__Definitions.html#ad477a289f1f03ac45407b64268d707d3", null ],
      [ "value", "group__CMSIS__RTOS__Definitions.html#a224511fffc5f05f068351e91851c0fc5", null ]
    ] ],
    [ "osEvent.value", "group__CMSIS__RTOS__Definitions.html#unionosEvent_8value", [
      [ "p", "group__CMSIS__RTOS__Definitions.html#a83878c91171338902e0fe0fb97a8c47a", null ],
      [ "signals", "group__CMSIS__RTOS__Definitions.html#aa8420f8bebd6d9578b405b6bbaa813d9", null ],
      [ "v", "group__CMSIS__RTOS__Definitions.html#a9e3669d19b675bd57058fd4664205d2a", null ]
    ] ],
    [ "osEvent.def", "group__CMSIS__RTOS__Definitions.html#unionosEvent_8def", [
      [ "mail_id", "group__CMSIS__RTOS__Definitions.html#a57aa2b69577037fb2428bc48a9fb1038", null ],
      [ "message_id", "group__CMSIS__RTOS__Definitions.html#a4ccaa172acbdcbf38fa0ea0ef0229c49", null ]
    ] ],
    [ "os_mailQ", "group__CMSIS__RTOS__Definitions.html#structos__mailQ", null ]
];