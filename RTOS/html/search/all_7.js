var searchData=
[
  ['mail_20queue_19',['Mail Queue',['../group__CMSIS__RTOS__Mail.html',1,'']]],
  ['message_20queue_20',['Message Queue',['../group__CMSIS__RTOS__Message.html',1,'']]],
  ['mutexes_21',['Mutexes',['../group__CMSIS__RTOS__MutexMgmt.html',1,'']]],
  ['memory_20pool_22',['Memory Pool',['../group__CMSIS__RTOS__PoolMgmt.html',1,'']]],
  ['misra_2dc_3a2004_20compliance_20exceptions_23',['MISRA-C:2004 Compliance Exceptions',['../misraCompliance.html',1,'rtxImplementation']]]
];
