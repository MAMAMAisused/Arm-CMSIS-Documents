var searchData=
[
  ['semaphores_183',['Semaphores',['../group__CMSIS__RTOS__SemaphoreMgmt.html',1,'']]],
  ['signal_20events_184',['Signal Events',['../group__CMSIS__RTOS__SignalMgmt.html',1,'']]],
  ['status_20and_20error_20codes_185',['Status and Error Codes',['../group__CMSIS__RTOS__Status.html',1,'']]],
  ['stacksize_186',['stacksize',['../cmsis__os_8h.html#a950b7f81ad4711959517296e63bc79d1',1,'os_thread_def']]],
  ['status_187',['status',['../group__CMSIS__RTOS__Definitions.html#ad477a289f1f03ac45407b64268d707d3',1,'osEvent']]],
  ['svc_20functions_188',['SVC Functions',['../svcFunctions.html',1,'configure']]],
  ['svcthreadgetid_189',['svcThreadGetId',['../RTX__Conf__CM_8c.html#a395cca131b7746fc43c104a3485b77f7',1,'RTX_Conf_CM.c']]],
  ['system_20configuration_190',['System Configuration',['../systemConfig.html',1,'configure']]]
];
