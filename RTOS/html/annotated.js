var annotated =
[
    [ "os_mailQ", "group__CMSIS__RTOS__Definitions.html#structos__mailQ", null ],
    [ "os_mailQ_def", "cmsis__os_8h.html#structos__mailQ__def", "cmsis__os_8h_structos__mailQ__def" ],
    [ "os_messageQ_def", "cmsis__os_8h.html#structos__messageQ__def", "cmsis__os_8h_structos__messageQ__def" ],
    [ "os_mutex_def", "cmsis__os_8h.html#structos__mutex__def", "cmsis__os_8h_structos__mutex__def" ],
    [ "os_pool_def", "cmsis__os_8h.html#structos__pool__def", "cmsis__os_8h_structos__pool__def" ],
    [ "os_semaphore_def", "cmsis__os_8h.html#structos__semaphore__def", "cmsis__os_8h_structos__semaphore__def" ],
    [ "os_thread_def", "cmsis__os_8h.html#structos__thread__def", "cmsis__os_8h_structos__thread__def" ],
    [ "os_timer_def", "cmsis__os_8h.html#structos__timer__def", "cmsis__os_8h_structos__timer__def" ],
    [ "osEvent", "group__CMSIS__RTOS__Definitions.html#structosEvent", "group__CMSIS__RTOS__Definitions_structosEvent" ],
    [ "osEvent.def", "group__CMSIS__RTOS__Definitions.html#unionosEvent_8def", "group__CMSIS__RTOS__Definitions_unionosEvent_8def" ],
    [ "osEvent.value", "group__CMSIS__RTOS__Definitions.html#unionosEvent_8value", "group__CMSIS__RTOS__Definitions_unionosEvent_8value" ]
];