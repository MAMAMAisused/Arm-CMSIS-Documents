/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS-RTOS", "index.html", [
    [ "Revision History", "rtos_revisionHistory.html", [
      [ "CMSIS-RTOS in ARM::CMSIS Pack", "index.html#autotoc_md0", null ],
      [ "CMSIS-RTOS API", "rtos_revisionHistory.html#GenRTOSRev", null ],
      [ "CMSIS-RTOS RTX", "rtos_revisionHistory.html#RTXRevisionHistory", null ]
    ] ],
    [ "Generic RTOS Interface", "genRTOSIF.html", null ],
    [ "Using a CMSIS-RTOS Implementation", "usingOS.html", [
      [ "Header File Template: cmsis_os.h", "usingOS.html#cmsis_os_h", null ]
    ] ],
    [ "Function Overview", "functionOverview.html", [
      [ "Timout Value", "functionOverview.html#CMSIS_RTOS_TimeOutValue", null ],
      [ "Calls from Interrupt Service Routines", "functionOverview.html#CMSIS_RTOS_ISR_Calls", null ]
    ] ],
    [ "RTOS Validation", "rtosValidation.html", [
      [ "Sample Test Output", "rtosValidation.html#test_output", null ]
    ] ],
    [ "RTX Implementation", "rtxImplementation.html", "rtxImplementation" ],
    [ "Reference", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", "annotated" ],
    [ "Data Structure Index", "classes.html", null ],
    [ "Data Fields", "functions.html", [
      [ "All", "functions.html", null ],
      [ "Variables", "functions_vars.html", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';