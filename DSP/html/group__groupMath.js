var group__groupMath =
[
    [ "Introduction", "index.html#autotoc_md0", null ],
    [ "Using the Library", "index.html#autotoc_md1", null ],
    [ "Examples", "index.html#autotoc_md2", null ],
    [ "Toolchain Support", "index.html#autotoc_md3", null ],
    [ "Building the Library", "index.html#autotoc_md4", null ],
    [ "Preprocessor Macros", "index.html#autotoc_md5", null ],
    [ "CMSIS-DSP in ARM::CMSIS Pack", "index.html#autotoc_md6", null ],
    [ "Revision History of CMSIS-DSP", "index.html#autotoc_md7", null ],
    [ "Vector Absolute Value", "group__BasicAbs.html", "group__BasicAbs" ],
    [ "Vector Addition", "group__BasicAdd.html", "group__BasicAdd" ],
    [ "Vector bitwise AND", "group__And.html", "group__And" ],
    [ "Vector Dot Product", "group__BasicDotProd.html", "group__BasicDotProd" ],
    [ "Vector Multiplication", "group__BasicMult.html", "group__BasicMult" ],
    [ "Vector Negate", "group__BasicNegate.html", "group__BasicNegate" ],
    [ "Vector bitwise NOT", "group__Not.html", "group__Not" ],
    [ "Vector Offset", "group__BasicOffset.html", "group__BasicOffset" ],
    [ "Vector bitwise inclusive OR", "group__Or.html", "group__Or" ],
    [ "Vector Scale", "group__BasicScale.html", "group__BasicScale" ],
    [ "Vector Shift", "group__BasicShift.html", "group__BasicShift" ],
    [ "Vector Subtraction", "group__BasicSub.html", "group__BasicSub" ],
    [ "Vector bitwise exclusive OR", "group__Xor.html", "group__Xor" ]
];