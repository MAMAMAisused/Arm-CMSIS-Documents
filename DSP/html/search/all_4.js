var searchData=
[
  ['dct_20type_20iv_20functions_1128',['DCT Type IV Functions',['../group__DCT4__IDCT4.html',1,'']]],
  ['dct_20type_20iv_20tables_1129',['DCT Type IV Tables',['../group__DCT4__IDCT4__Table.html',1,'']]],
  ['degree_1130',['degree',['../structarm__svm__polynomial__instance__f32.html#aadf0dc6d4de0f53a2f145f3ee35f29b5',1,'arm_svm_polynomial_instance_f32']]],
  ['delta_1131',['DELTA',['../arm__convolution__example__f32_8c.html#a3fd2b1bcd7ddcf506237987ad780f495',1,'DELTA():&#160;arm_convolution_example_f32.c'],['../arm__dotproduct__example__f32_8c.html#a3fd2b1bcd7ddcf506237987ad780f495',1,'DELTA():&#160;arm_dotproduct_example_f32.c'],['../arm__sin__cos__example__f32_8c.html#a3fd2b1bcd7ddcf506237987ad780f495',1,'DELTA():&#160;arm_sin_cos_example_f32.c'],['../arm__variance__example__f32_8c.html#a3fd2b1bcd7ddcf506237987ad780f495',1,'DELTA():&#160;arm_variance_example_f32.c']]],
  ['delta_5fcoeff_1132',['DELTA_COEFF',['../arm__signal__converge__example__f32_8c.html#a9156349d99957ded15d8aa3aa11723de',1,'arm_signal_converge_example_f32.c']]],
  ['delta_5ferror_1133',['DELTA_ERROR',['../arm__signal__converge__example__f32_8c.html#a6d3c6a4484dcaac72fbfe5100c39b9b6',1,'arm_signal_converge_example_f32.c']]],
  ['delta_5fq15_1134',['DELTA_Q15',['../arm__math_8h.html#a663277ff19ad0b409fb98b64b2c2750b',1,'arm_math.h']]],
  ['delta_5fq31_1135',['DELTA_Q31',['../arm__math_8h.html#aad77ae594e95c5af6ae4129bd6a483c2',1,'arm_math.h']]],
  ['deprecated_20list_1136',['Deprecated List',['../deprecated.html',1,'']]],
  ['dir_1137',['dir',['../structarm__sort__instance__f32.html#ab76adec37769943685c731d2f45fd50d',1,'arm_sort_instance_f32::dir()'],['../structarm__merge__sort__instance__f32.html#a0f0245c571bf3ca7489f088d87521fa6',1,'arm_merge_sort_instance_f32::dir()']]],
  ['distancefunctions_2ec_1138',['DistanceFunctions.c',['../DistanceFunctions_8c.html',1,'']]],
  ['dobitreverse_1139',['doBitReverse',['../arm__fft__bin__example__f32_8c.html#a4d2e31c38e8172505e0a369a6898657d',1,'arm_fft_bin_example_f32.c']]],
  ['dot_20product_20example_1140',['Dot Product Example',['../group__DotproductExample.html',1,'']]],
  ['dpi_5ff_1141',['DPI_F',['../arm__gaussian__naive__bayes__predict__f32_8c.html#af08fb3540c4d7d62da941d325144db1a',1,'arm_gaussian_naive_bayes_predict_f32.c']]],
  ['dualcoefficients_1142',['dualCoefficients',['../structarm__svm__linear__instance__f32.html#a0f661b17229d457a64834c55c606b4bd',1,'arm_svm_linear_instance_f32::dualCoefficients()'],['../structarm__svm__polynomial__instance__f32.html#abbd237eb8f091c97e855f2b3d744e354',1,'arm_svm_polynomial_instance_f32::dualCoefficients()'],['../structarm__svm__rbf__instance__f32.html#ab5bbe8dc148f9a1b64091b5df0677383',1,'arm_svm_rbf_instance_f32::dualCoefficients()'],['../structarm__svm__sigmoid__instance__f32.html#ad290fd29e1a436bfe772c28eac9ea47a',1,'arm_svm_sigmoid_instance_f32::dualCoefficients()'],['../arm__svm__example__f32_8c.html#a6bee216b9b7e7b8d18687cd692e26a8a',1,'dualCoefficients():&#160;arm_svm_example_f32.c']]],
  ['dualcoefs_1143',['dualCoefs',['../namespacetrain.html#a118b6004cd08e111096a0e45b26a0192',1,'train']]],
  ['distance_20functions_1144',['Distance functions',['../group__groupDistance.html',1,'']]]
];
