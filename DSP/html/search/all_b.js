var searchData=
[
  ['l_1209',['L',['../structarm__fir__interpolate__instance__q15.html#a5431bdc079e72a973b51d359f7f13603',1,'arm_fir_interpolate_instance_q15::L()'],['../structarm__fir__interpolate__instance__q31.html#a5cdf0a631cb74e0e9588c388abe5235c',1,'arm_fir_interpolate_instance_q31::L()'],['../structarm__fir__interpolate__instance__f32.html#ae6f94dcc0ccd8aa4bc699b20985d9df5',1,'arm_fir_interpolate_instance_f32::L()']]],
  ['levels_1210',['levels',['../namespacetrain.html#a97d66a7ad09c8d42f9a284bde3d1c2c7',1,'train']]],
  ['linear_20interpolate_20example_1211',['Linear Interpolate Example',['../group__LinearInterpExample.html',1,'']]],
  ['linear_20interpolation_1212',['Linear Interpolation',['../group__LinearInterpolate.html',1,'']]],
  ['linestyles_1213',['linestyles',['../namespacetrain.html#a6ddda3c1f9f81ce55ba345f95fc5745c',1,'train']]],
  ['list_1214',['list',['../Source_2CMakeLists_8txt.html#a831b7fa3ecb78efc7660397695c2a386',1,'CMakeLists.txt']]],
  ['least_20mean_20square_20_28lms_29_20filters_1215',['Least Mean Square (LMS) Filters',['../group__LMS.html',1,'']]],
  ['lmsnorm_5finstance_1216',['lmsNorm_instance',['../arm__signal__converge__example__f32_8c.html#a519f9b4db839245f3bf2075ff4c17605',1,'arm_signal_converge_example_f32.c']]],
  ['lmsnormcoeff_5ff32_1217',['lmsNormCoeff_f32',['../arm__signal__converge__data_8c.html#aad7c60c30c5af397bb75e603f250f9d3',1,'lmsNormCoeff_f32():&#160;arm_signal_converge_data.c'],['../arm__signal__converge__example__f32_8c.html#aad7c60c30c5af397bb75e603f250f9d3',1,'lmsNormCoeff_f32():&#160;arm_signal_converge_data.c']]],
  ['lmsstatef32_1218',['lmsStateF32',['../arm__signal__converge__example__f32_8c.html#a706980f6f654d199c61e08e7814bd0a1',1,'arm_signal_converge_example_f32.c']]],
  ['lpf_5finstance_1219',['LPF_instance',['../arm__signal__converge__example__f32_8c.html#a652d3507a776117b4860b3e18f2d2d64',1,'arm_signal_converge_example_f32.c']]]
];
