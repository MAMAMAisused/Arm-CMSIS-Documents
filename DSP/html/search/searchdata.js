var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxyz",
  1: "a",
  2: "t",
  3: "abcdefhmst",
  4: "_acegilmrstw",
  5: "abcdefghiklmnoprstvwxyz",
  6: "fq",
  7: "a",
  8: "a",
  9: "_abcdfimnpqstuvx",
  10: "bcdefghilmnprstv",
  11: "cdr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines",
  10: "groups",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros",
  10: "Modules",
  11: "Pages"
};

