var searchData=
[
  ['matrix_20functions_3019',['Matrix Functions',['../group__groupMatrix.html',1,'']]],
  ['matrix_20addition_3020',['Matrix Addition',['../group__MatrixAdd.html',1,'']]],
  ['matrix_20example_3021',['Matrix Example',['../group__MatrixExample.html',1,'']]],
  ['matrix_20initialization_3022',['Matrix Initialization',['../group__MatrixInit.html',1,'']]],
  ['matrix_20inverse_3023',['Matrix Inverse',['../group__MatrixInv.html',1,'']]],
  ['matrix_20multiplication_3024',['Matrix Multiplication',['../group__MatrixMult.html',1,'']]],
  ['matrix_20scale_3025',['Matrix Scale',['../group__MatrixScale.html',1,'']]],
  ['matrix_20subtraction_3026',['Matrix Subtraction',['../group__MatrixSub.html',1,'']]],
  ['matrix_20transpose_3027',['Matrix Transpose',['../group__MatrixTrans.html',1,'']]],
  ['maximum_3028',['Maximum',['../group__Max.html',1,'']]],
  ['mean_3029',['Mean',['../group__mean.html',1,'']]],
  ['minimum_3030',['Minimum',['../group__Min.html',1,'']]]
];
