var searchData=
[
  ['finite_20impulse_20response_20_28fir_29_20filters_3002',['Finite Impulse Response (FIR) Filters',['../group__FIR.html',1,'']]],
  ['finite_20impulse_20response_20_28fir_29_20decimator_3003',['Finite Impulse Response (FIR) Decimator',['../group__FIR__decimate.html',1,'']]],
  ['finite_20impulse_20response_20_28fir_29_20interpolator_3004',['Finite Impulse Response (FIR) Interpolator',['../group__FIR__Interpolate.html',1,'']]],
  ['finite_20impulse_20response_20_28fir_29_20lattice_20filters_3005',['Finite Impulse Response (FIR) Lattice Filters',['../group__FIR__Lattice.html',1,'']]],
  ['finite_20impulse_20response_20_28fir_29_20sparse_20filters_3006',['Finite Impulse Response (FIR) Sparse Filters',['../group__FIR__Sparse.html',1,'']]],
  ['fir_20lowpass_20filter_20example_3007',['FIR Lowpass Filter Example',['../group__FIRLPF.html',1,'']]],
  ['float_20distances_3008',['Float Distances',['../group__FloatDist.html',1,'']]],
  ['frequency_20bin_20example_3009',['Frequency Bin Example',['../group__FrequencyBin.html',1,'']]],
  ['fast_20math_20functions_3010',['Fast Math Functions',['../group__groupFastMath.html',1,'']]],
  ['filtering_20functions_3011',['Filtering Functions',['../group__groupFilters.html',1,'']]]
];
