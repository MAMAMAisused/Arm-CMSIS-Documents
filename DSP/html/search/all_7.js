var searchData=
[
  ['gaindb_1188',['gainDB',['../arm__graphic__equalizer__example__q31_8c.html#a963aee85bb41a50fc943ac9048d123ab',1,'arm_graphic_equalizer_example_q31.c']]],
  ['gamma_1189',['gamma',['../structarm__svm__polynomial__instance__f32.html#a7ff43f7deb792149696d2d2ab4c61a82',1,'arm_svm_polynomial_instance_f32::gamma()'],['../structarm__svm__rbf__instance__f32.html#ad96527324c072b64c2552214c4aa538c',1,'arm_svm_rbf_instance_f32::gamma()'],['../structarm__svm__sigmoid__instance__f32.html#a9ebdb1ce50182fda173c4b311d72f787',1,'arm_svm_sigmoid_instance_f32::gamma()']]],
  ['graphic_20audio_20equalizer_20example_1190',['Graphic Audio Equalizer Example',['../group__GEQ5Band.html',1,'']]],
  ['getinput_1191',['getinput',['../arm__signal__converge__example__f32_8c.html#afd2975c4763ec935771e6f63bfe7758b',1,'arm_signal_converge_example_f32.c']]],
  ['gnb_1192',['gnb',['../namespacetrain.html#a0fb971e3280d09b595b402b640bfeccd',1,'train']]]
];
