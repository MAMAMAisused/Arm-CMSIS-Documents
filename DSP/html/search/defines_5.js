var searchData=
[
  ['f32_5fabsmax_2912',['F32_ABSMAX',['../arm__math_8h.html#ad997d60e69e9116cb7714d14f7442c73',1,'arm_math.h']]],
  ['f32_5fabsmin_2913',['F32_ABSMIN',['../arm__math_8h.html#a30173fccebc0c990bd9d4ee7a864cade',1,'arm_math.h']]],
  ['f32_5fmax_2914',['F32_MAX',['../arm__math_8h.html#a754d8f564bd5d1fb49931b2f4c0ec00c',1,'arm_math.h']]],
  ['f32_5fmin_2915',['F32_MIN',['../arm__math_8h.html#a0f9ce95191fb035fcd53ab63b9663e20',1,'arm_math.h']]],
  ['f64_5fabsmax_2916',['F64_ABSMAX',['../arm__math_8h.html#a165289cdbeeb4f5903df34d70489ab19',1,'arm_math.h']]],
  ['f64_5fabsmin_2917',['F64_ABSMIN',['../arm__math_8h.html#a07c00c6b967495f5e56dc989d859c3db',1,'arm_math.h']]],
  ['f64_5fmax_2918',['F64_MAX',['../arm__math_8h.html#a82daf28d8d0052302ccf95c163687ce5',1,'arm_math.h']]],
  ['f64_5fmin_2919',['F64_MIN',['../arm__math_8h.html#a4ef26fd77745fadb40f370d63943d5eb',1,'arm_math.h']]],
  ['fast_5fmath_5fq15_5fshift_2920',['FAST_MATH_Q15_SHIFT',['../arm__math_8h.html#a34716b73c631e65e8dd855e08384ecb2',1,'arm_math.h']]],
  ['fast_5fmath_5fq31_5fshift_2921',['FAST_MATH_Q31_SHIFT',['../arm__math_8h.html#a4268f77b1811a0c7fc2532a0bf6108b0',1,'arm_math.h']]],
  ['fast_5fmath_5ftable_5fsize_2922',['FAST_MATH_TABLE_SIZE',['../arm__math_8h.html#afcb9147c96853bea484cfc2dde07463d',1,'arm_math.h']]],
  ['fftinit_2923',['FFTINIT',['../arm__cfft__init__f32_8c.html#a683e50ab6ad88d46aa763d21bf48ee96',1,'FFTINIT():&#160;arm_cfft_init_f32.c'],['../arm__cfft__init__f64_8c.html#a683e50ab6ad88d46aa763d21bf48ee96',1,'FFTINIT():&#160;arm_cfft_init_f64.c'],['../arm__cfft__init__q15_8c.html#a683e50ab6ad88d46aa763d21bf48ee96',1,'FFTINIT():&#160;arm_cfft_init_q15.c'],['../arm__cfft__init__q31_8c.html#a683e50ab6ad88d46aa763d21bf48ee96',1,'FFTINIT():&#160;arm_cfft_init_q31.c']]]
];
