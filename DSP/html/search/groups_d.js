var searchData=
[
  ['statistics_20functions_3038',['Statistics Functions',['../group__groupStats.html',1,'']]],
  ['support_20functions_3039',['Support Functions',['../group__groupSupport.html',1,'']]],
  ['svm_20functions_3040',['SVM Functions',['../group__groupSVM.html',1,'']]],
  ['signal_20convergence_20example_3041',['Signal Convergence Example',['../group__SignalConvergence.html',1,'']]],
  ['sine_3042',['Sine',['../group__sin.html',1,'']]],
  ['sine_20cosine_3043',['Sine Cosine',['../group__SinCos.html',1,'']]],
  ['sinecosine_20example_3044',['SineCosine Example',['../group__SinCosExample.html',1,'']]],
  ['square_20root_3045',['Square Root',['../group__SQRT.html',1,'']]],
  ['standard_20deviation_3046',['Standard deviation',['../group__STD.html',1,'']]],
  ['svm_20example_3047',['SVM Example',['../group__SVMExample.html',1,'']]]
];
