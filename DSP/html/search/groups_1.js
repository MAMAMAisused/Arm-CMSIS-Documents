var searchData=
[
  ['complex_20fft_20tables_2976',['Complex FFT Tables',['../group__CFFT__CIFFT.html',1,'']]],
  ['class_20marks_20example_2977',['Class Marks Example',['../group__ClassMarks.html',1,'']]],
  ['complex_20conjugate_2978',['Complex Conjugate',['../group__cmplx__conj.html',1,'']]],
  ['complex_20dot_20product_2979',['Complex Dot Product',['../group__cmplx__dot__prod.html',1,'']]],
  ['complex_20magnitude_2980',['Complex Magnitude',['../group__cmplx__mag.html',1,'']]],
  ['complex_20magnitude_20squared_2981',['Complex Magnitude Squared',['../group__cmplx__mag__squared.html',1,'']]],
  ['complex_2dby_2dcomplex_20multiplication_2982',['Complex-by-Complex Multiplication',['../group__CmplxByCmplxMult.html',1,'']]],
  ['complex_2dby_2dreal_20multiplication_2983',['Complex-by-Real Multiplication',['../group__CmplxByRealMult.html',1,'']]],
  ['complex_20matrix_20multiplication_2984',['Complex Matrix Multiplication',['../group__CmplxMatrixMult.html',1,'']]],
  ['complex_20fft_20functions_2985',['Complex FFT Functions',['../group__ComplexFFT.html',1,'']]],
  ['convolution_2986',['Convolution',['../group__Conv.html',1,'']]],
  ['convolution_20example_2987',['Convolution Example',['../group__ConvolutionExample.html',1,'']]],
  ['correlation_2988',['Correlation',['../group__Corr.html',1,'']]],
  ['cosine_2989',['Cosine',['../group__cos.html',1,'']]],
  ['convert_2032_2dbit_20floating_20point_20value_2990',['Convert 32-bit floating point value',['../group__float__to__x.html',1,'']]],
  ['complex_20math_20functions_2991',['Complex Math Functions',['../group__groupCmplxMath.html',1,'']]],
  ['controller_20functions_2992',['Controller Functions',['../group__groupController.html',1,'']]],
  ['convert_2016_2dbit_20integer_20value_2993',['Convert 16-bit Integer value',['../group__q15__to__x.html',1,'']]],
  ['convert_2032_2dbit_20integer_20value_2994',['Convert 32-bit Integer value',['../group__q31__to__x.html',1,'']]],
  ['convert_208_2dbit_20integer_20value_2995',['Convert 8-bit Integer value',['../group__q7__to__x.html',1,'']]],
  ['cubic_20spline_20interpolation_2996',['Cubic Spline Interpolation',['../group__SplineInterpolate.html',1,'']]]
];
