var searchData=
[
  ['l_2633',['L',['../structarm__fir__interpolate__instance__q15.html#a5431bdc079e72a973b51d359f7f13603',1,'arm_fir_interpolate_instance_q15::L()'],['../structarm__fir__interpolate__instance__q31.html#a5cdf0a631cb74e0e9588c388abe5235c',1,'arm_fir_interpolate_instance_q31::L()'],['../structarm__fir__interpolate__instance__f32.html#ae6f94dcc0ccd8aa4bc699b20985d9df5',1,'arm_fir_interpolate_instance_f32::L()']]],
  ['levels_2634',['levels',['../namespacetrain.html#a97d66a7ad09c8d42f9a284bde3d1c2c7',1,'train']]],
  ['linestyles_2635',['linestyles',['../namespacetrain.html#a6ddda3c1f9f81ce55ba345f95fc5745c',1,'train']]],
  ['lmsnorm_5finstance_2636',['lmsNorm_instance',['../arm__signal__converge__example__f32_8c.html#a519f9b4db839245f3bf2075ff4c17605',1,'arm_signal_converge_example_f32.c']]],
  ['lmsnormcoeff_5ff32_2637',['lmsNormCoeff_f32',['../arm__signal__converge__data_8c.html#aad7c60c30c5af397bb75e603f250f9d3',1,'lmsNormCoeff_f32():&#160;arm_signal_converge_data.c'],['../arm__signal__converge__example__f32_8c.html#aad7c60c30c5af397bb75e603f250f9d3',1,'lmsNormCoeff_f32():&#160;arm_signal_converge_data.c']]],
  ['lmsstatef32_2638',['lmsStateF32',['../arm__signal__converge__example__f32_8c.html#a706980f6f654d199c61e08e7814bd0a1',1,'arm_signal_converge_example_f32.c']]],
  ['lpf_5finstance_2639',['LPF_instance',['../arm__signal__converge__example__f32_8c.html#a652d3507a776117b4860b3e18f2d2d64',1,'arm_signal_converge_example_f32.c']]]
];
