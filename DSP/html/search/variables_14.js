var searchData=
[
  ['x_2816',['x',['../structarm__spline__instance__f32.html#aa5c3f088bbe9788b3378980f61c98d55',1,'arm_spline_instance_f32::x()'],['../namespacetrain.html#ae5491ddf3054c5f7a7db3cf9b253b054',1,'train.x()']]],
  ['x0_2817',['x0',['../structarm__lms__norm__instance__f32.html#aec958fe89b164a30f38bcca9f5d96218',1,'arm_lms_norm_instance_f32::x0()'],['../structarm__lms__norm__instance__q31.html#a47c4466d644e0d8ba407995adfa9b917',1,'arm_lms_norm_instance_q31::x0()'],['../structarm__lms__norm__instance__q15.html#a3fc1d6f97d2c6d5324871de6895cb7e9',1,'arm_lms_norm_instance_q15::x0()']]],
  ['x1_2818',['x1',['../structarm__linear__interp__instance__f32.html#a08352dc6ea82fbc0827408e018535481',1,'arm_linear_interp_instance_f32::x1()'],['../namespacetrain.html#a895af189b83ea658dff6d5ddc69caaab',1,'train.x1()']]],
  ['x2_2819',['x2',['../namespacetrain.html#a6bc5dd84f03860c08934b1851d4eab0c',1,'train']]],
  ['x3_2820',['x3',['../namespacetrain.html#adf0a545ff856e979d24bcfa84fdeca9f',1,'train']]],
  ['x_5ff32_2821',['X_f32',['../arm__matrix__example__f32_8c.html#a98c67c0fc0cb5f2df51b21482d31d21c',1,'arm_matrix_example_f32.c']]],
  ['x_5fmax_2822',['x_max',['../namespacetrain.html#a8b755332c5a18687cb4f09b44be813eb',1,'train']]],
  ['x_5fmin_2823',['x_min',['../namespacetrain.html#a63168ffe86d314e543793edfe243581c',1,'train']]],
  ['x_5ftrain_2824',['X_train',['../namespacetrain.html#a583070356cd0862e60ec4f64841e1447',1,'train']]],
  ['xa_2825',['xa',['../namespacetrain.html#a5a838c01ae194bd8cab21f0dc8e47751',1,'train']]],
  ['xref_5ff32_2826',['xRef_f32',['../arm__matrix__example__f32_8c.html#a6184758419722fa16bb883097c2f596b',1,'arm_matrix_example_f32.c']]],
  ['xspacing_2827',['xSpacing',['../structarm__linear__interp__instance__f32.html#aa8e2d686b5434a406d390b347b183511',1,'arm_linear_interp_instance_f32']]],
  ['xx_2828',['XX',['../namespacetrain.html#a65e8255c83cf0b1289b4f2306200a3ca',1,'train']]]
];
