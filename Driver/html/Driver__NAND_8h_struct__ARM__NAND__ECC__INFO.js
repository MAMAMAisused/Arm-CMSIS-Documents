var Driver__NAND_8h_struct__ARM__NAND__ECC__INFO =
[
    [ "type", "Driver__NAND_8h.html#ad44b615021ed3ccb734fcaf583ef4a03", null ],
    [ "page_layout", "Driver__NAND_8h.html#a5952ba4313bda7833fefd358f5aff979", null ],
    [ "page_count", "Driver__NAND_8h.html#aa993bc236650aa405b01d00b7ca72904", null ],
    [ "page_size", "Driver__NAND_8h.html#a9dd3e47e968a8f6beb5d88c6d1b7ebe9", null ],
    [ "reserved", "Driver__NAND_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ],
    [ "correctable_bits", "Driver__NAND_8h.html#ae65f920c4ad99fd0c6bdf5fd8c4d161a", null ],
    [ "codeword_size", "Driver__NAND_8h.html#ae8cff208d9efb5067d38ced675916c66", null ],
    [ "ecc_size", "Driver__NAND_8h.html#a22365f6a2af1171a1c3629c8ae5fe001", null ],
    [ "ecc_offset", "Driver__NAND_8h.html#a22d6a1813a47a7044f7acb478f8e9eb8", null ],
    [ "virtual_page_size", "Driver__NAND_8h.html#aa270f95e67fdf1e9137c61f2045b7636", null ],
    [ "codeword_offset", "Driver__NAND_8h.html#a31c5b0e899b2d60adb6cdef971633db0", null ],
    [ "codeword_gap", "Driver__NAND_8h.html#ae0a2b8415bddd99dade9cbcf8c52186a", null ],
    [ "ecc_gap", "Driver__NAND_8h.html#a94d6b62b24d96ff83c985325d8825dd3", null ]
];