var Driver__CAN_8h_struct__ARM__CAN__STATUS =
[
    [ "unit_state", "Driver__CAN_8h.html#a96ec94acab56690b3801e3c5fbd09fa2", null ],
    [ "last_error_code", "Driver__CAN_8h.html#a2171ea8dff5e4b54e84728aa134854b6", null ],
    [ "tx_error_count", "Driver__CAN_8h.html#a8941505f6f3ebd69825c4382184c580f", null ],
    [ "rx_error_count", "Driver__CAN_8h.html#ab7e8b863b379b786ad1af935aa3ef2e8", null ],
    [ "reserved", "Driver__CAN_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ]
];