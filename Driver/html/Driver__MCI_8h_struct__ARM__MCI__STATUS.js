var Driver__MCI_8h_struct__ARM__MCI__STATUS =
[
    [ "command_active", "Driver__MCI_8h.html#aa22ef7c7597e90835bd67d5795ba757e", null ],
    [ "command_timeout", "Driver__MCI_8h.html#a56e426979c3872254c156e9ae7eead5b", null ],
    [ "command_error", "Driver__MCI_8h.html#afca11cd2ce661c67455a6d75328848cc", null ],
    [ "transfer_active", "Driver__MCI_8h.html#a2655d3422b720097b091a28e8bbcea8f", null ],
    [ "transfer_timeout", "Driver__MCI_8h.html#a598ae4a196316d6dcb97d07fd337ecdd", null ],
    [ "transfer_error", "Driver__MCI_8h.html#a21d4bc1a03e161bd33693619039a6afa", null ],
    [ "sdio_interrupt", "Driver__MCI_8h.html#a61e2a440b27d7d22c866ad4427f4b825", null ],
    [ "ccs", "Driver__MCI_8h.html#a13c956ba993083f1e59379968e2badbe", null ],
    [ "reserved", "Driver__MCI_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ]
];