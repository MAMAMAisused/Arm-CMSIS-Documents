var Driver__USART_8h_struct__ARM__USART__STATUS =
[
    [ "tx_busy", "Driver__USART_8h.html#a2c6d2b67fba3f3e084e96a6bc7fcac6b", null ],
    [ "rx_busy", "Driver__USART_8h.html#a9f5baee58ed41b382628a82a0b1cbcb4", null ],
    [ "tx_underflow", "Driver__USART_8h.html#a048f45e9d2257a21821f81d9edd17b72", null ],
    [ "rx_overflow", "Driver__USART_8h.html#ac403aefd9bce8b0172e1996c0f3dd8aa", null ],
    [ "rx_break", "Driver__USART_8h.html#aa5e3fa74f444688f9e727ffc1e988e5d", null ],
    [ "rx_framing_error", "Driver__USART_8h.html#af1d1cfd8b231843d5cc23e6a2b1ca8d0", null ],
    [ "rx_parity_error", "Driver__USART_8h.html#affb21b610e2d0d71727702441c238f4f", null ],
    [ "reserved", "Driver__USART_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ]
];