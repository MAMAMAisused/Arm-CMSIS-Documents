var group__wifi__interface__gr =
[
    [ "WiFi Control", "group__wifi__control__gr.html", "group__wifi__control__gr" ],
    [ "WiFi Management", "group__wifi__management__gr.html", "group__wifi__management__gr" ],
    [ "WiFi Bypass Mode", "group__wifi__bypass__gr.html", "group__wifi__bypass__gr" ],
    [ "WiFi Socket", "group__wifi__socket__gr.html", "group__wifi__socket__gr" ],
    [ "ARM_DRIVER_WIFI", "group__wifi__interface__gr.html#structARM__DRIVER__WIFI", null ]
];