var Driver__I2C_8h_struct__ARM__I2C__STATUS =
[
    [ "busy", "Driver__I2C_8h.html#a50c88f3c1d787773e2ac1b59533f034a", null ],
    [ "mode", "Driver__I2C_8h.html#a6b29e4f37f4482274af785ad5ffe96a7", null ],
    [ "direction", "Driver__I2C_8h.html#a2148ffb99828aeaced6a5655502434ac", null ],
    [ "general_call", "Driver__I2C_8h.html#ab65804439f6f5beda8da30381b0ad22d", null ],
    [ "arbitration_lost", "Driver__I2C_8h.html#ab3e3c8eeeae7fbe3c51dcb3d4104af24", null ],
    [ "bus_error", "Driver__I2C_8h.html#a43b1d210c48f4361c5054ba69bcae702", null ],
    [ "reserved", "Driver__I2C_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ]
];