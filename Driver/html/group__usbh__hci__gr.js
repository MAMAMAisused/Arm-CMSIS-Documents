var group__usbh__hci__gr =
[
    [ "ARM_DRIVER_USBH_HCI", "group__usbh__hci__gr.html#structARM__DRIVER__USBH__HCI", null ],
    [ "ARM_USBH_HCI_CAPABILITIES", "group__usbh__hci__gr.html#structARM__USBH__HCI__CAPABILITIES", null ],
    [ "ARM_USBH_HCI_Interrupt_t", "group__usbh__hci__gr.html#gac60df9d1f2b3a769f2c30141800a9806", null ],
    [ "ARM_USBH_HCI_GetVersion", "group__usbh__hci__gr.html#ga10109d0c2a9a128225b5e893d3f72d08", null ],
    [ "ARM_USBH_HCI_GetCapabilities", "group__usbh__hci__gr.html#gae607c49ca97202500631473a901e8c2b", null ],
    [ "ARM_USBH_HCI_Initialize", "group__usbh__hci__gr.html#gabc1392a544cb64491b5ea5ce6590d832", null ],
    [ "ARM_USBH_HCI_Uninitialize", "group__usbh__hci__gr.html#gaacb68fdf201cdb1846b31642a760f041", null ],
    [ "ARM_USBH_HCI_PowerControl", "group__usbh__hci__gr.html#ga27fa5ec8854cd9877bbef4abffe9a12b", null ],
    [ "ARM_USBH_HCI_PortVbusOnOff", "group__usbh__hci__gr.html#gade1e83403c6ea965fe3e6c4c21fbbded", null ],
    [ "ARM_USBH_HCI_Interrupt", "group__usbh__hci__gr.html#ga79d3c2509ed869c8d7d1485acad7b6c6", null ]
];