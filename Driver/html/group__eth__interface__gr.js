var group__eth__interface__gr =
[
    [ "Media Interface Types", "group__eth__interface__types1.html", "group__eth__interface__types1" ],
    [ "Ethernet MAC Interface", "group__eth__mac__interface__gr.html", "group__eth__mac__interface__gr" ],
    [ "Ethernet PHY Interface", "group__eth__phy__interface__gr.html", "group__eth__phy__interface__gr" ],
    [ "ARM_ETH_LINK_INFO", "group__eth__interface__gr.html#structARM__ETH__LINK__INFO", null ],
    [ "ARM_ETH_MAC_ADDR", "group__eth__interface__gr.html#structARM__ETH__MAC__ADDR", null ]
];