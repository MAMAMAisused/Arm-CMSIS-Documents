var group__common__drv__gr =
[
    [ "Status Error Codes", "group__execution__status.html", "group__execution__status" ],
    [ "Status Error Codes", "group__nand__execution__status.html", "group__nand__execution__status" ],
    [ "Status Error Codes", "group__sai__execution__status.html", "group__sai__execution__status" ],
    [ "Status Error Codes", "group__spi__execution__status.html", "group__spi__execution__status" ],
    [ "Status Error Codes", "group__usart__execution__status.html", "group__usart__execution__status" ],
    [ "ARM_DRIVER_VERSION", "group__common__drv__gr.html#structARM__DRIVER__VERSION", null ]
];