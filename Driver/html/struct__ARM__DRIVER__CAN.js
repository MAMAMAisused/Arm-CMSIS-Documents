var struct__ARM__DRIVER__CAN =
[
    [ "GetVersion", "struct__ARM__DRIVER__CAN.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__CAN.html#afe188d71bae11125f0f3de75f8c36380", null ],
    [ "Initialize", "struct__ARM__DRIVER__CAN.html#aa722bf7d2684355f4d58145d5f56c63a", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__CAN.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__CAN.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "GetClock", "struct__ARM__DRIVER__CAN.html#ab01820b7a50df239afff191a8fb451cf", null ],
    [ "SetBitrate", "struct__ARM__DRIVER__CAN.html#a28de13da9786cacb573b3f68f6b44fda", null ],
    [ "SetMode", "struct__ARM__DRIVER__CAN.html#afdb1988eb653ef7f8bdfcf52e73b63e4", null ],
    [ "ObjectGetCapabilities", "struct__ARM__DRIVER__CAN.html#aea0cc23e7116eb2cbd988aa413506912", null ],
    [ "ObjectSetFilter", "struct__ARM__DRIVER__CAN.html#aa985845507551f929bbb2bcd780d790d", null ],
    [ "ObjectConfigure", "struct__ARM__DRIVER__CAN.html#a8baa3080cc13a24e5b968249a14b233a", null ],
    [ "MessageSend", "struct__ARM__DRIVER__CAN.html#ab087744bd0751ad34706497658d650b1", null ],
    [ "MessageRead", "struct__ARM__DRIVER__CAN.html#a01363c5adf7bab23b44d62eb26c61238", null ],
    [ "Control", "struct__ARM__DRIVER__CAN.html#aada421ec80747aaff075970a86743689", null ],
    [ "GetStatus", "struct__ARM__DRIVER__CAN.html#a5bb744b9bdfb2949b151cf45b8a1a1e3", null ]
];