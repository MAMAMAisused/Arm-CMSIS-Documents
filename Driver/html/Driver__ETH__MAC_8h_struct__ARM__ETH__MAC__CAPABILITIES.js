var Driver__ETH__MAC_8h_struct__ARM__ETH__MAC__CAPABILITIES =
[
    [ "checksum_offload_rx_ip4", "Driver__ETH__MAC_8h.html#a0051111be2e389c3161da1c444746216", null ],
    [ "checksum_offload_rx_ip6", "Driver__ETH__MAC_8h.html#a674b2306c64901e924b3cb7bb882f32f", null ],
    [ "checksum_offload_rx_udp", "Driver__ETH__MAC_8h.html#a5a447f05a5fbfd35896aad9cd769511c", null ],
    [ "checksum_offload_rx_tcp", "Driver__ETH__MAC_8h.html#a730d6be6a7b868e0690d9548e77b7aae", null ],
    [ "checksum_offload_rx_icmp", "Driver__ETH__MAC_8h.html#a142179445bfdbaaaf0d451f277fb0e96", null ],
    [ "checksum_offload_tx_ip4", "Driver__ETH__MAC_8h.html#ac787d70407ce70e28724932fb32ef0ba", null ],
    [ "checksum_offload_tx_ip6", "Driver__ETH__MAC_8h.html#a8f7a154565e652d976b9e65bf3516504", null ],
    [ "checksum_offload_tx_udp", "Driver__ETH__MAC_8h.html#ab3f9560668a087606c40cd81b935396b", null ],
    [ "checksum_offload_tx_tcp", "Driver__ETH__MAC_8h.html#a6c2b80bbfe520f3e7808cf3d4aaedb45", null ],
    [ "checksum_offload_tx_icmp", "Driver__ETH__MAC_8h.html#a7b701bac9d66886b5c6964b20c6ca55a", null ],
    [ "media_interface", "Driver__ETH__MAC_8h.html#a3c5cb74e086417a01d0079f847a3fc8d", null ],
    [ "mac_address", "Driver__ETH__MAC_8h.html#a7fdea04bacd9c0e12792751055ef6238", null ],
    [ "event_rx_frame", "Driver__ETH__MAC_8h.html#a8c8f1ac2bf053a9bac98c476646a6018", null ],
    [ "event_tx_frame", "Driver__ETH__MAC_8h.html#a1b4af3590d59ea4f8e845b4239a4e445", null ],
    [ "event_wakeup", "Driver__ETH__MAC_8h.html#a7536d9b9818b20b6974a712e0449439b", null ],
    [ "precision_timer", "Driver__ETH__MAC_8h.html#a881a863974d32f95d7829f768ac47aa2", null ],
    [ "reserved", "Driver__ETH__MAC_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ]
];