var Driver__WiFi_8h_structARM__WIFI__CONFIG__s =
[
    [ "ssid", "Driver__WiFi_8h.html#a587ba0cb07f02913598610049a3bbb79", null ],
    [ "pass", "Driver__WiFi_8h.html#a3bef28806edf8f4c8cb82584b7e8c3cb", null ],
    [ "security", "Driver__WiFi_8h.html#a5cd97d88131cf38bcc75189f9569f9b7", null ],
    [ "ch", "Driver__WiFi_8h.html#acbcf5f8de7895ca456bb39fbcccde1d1", null ],
    [ "reserved", "Driver__WiFi_8h.html#acb7bc06bed6f6408d719334fc41698c7", null ],
    [ "wps_method", "Driver__WiFi_8h.html#a5e263198499fb98f97d40a9b82579175", null ],
    [ "wps_pin", "Driver__WiFi_8h.html#a2047d60666390ea4755ce22603969c4c", null ]
];