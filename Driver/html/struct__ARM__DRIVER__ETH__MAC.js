var struct__ARM__DRIVER__ETH__MAC =
[
    [ "GetVersion", "struct__ARM__DRIVER__ETH__MAC.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__ETH__MAC.html#aa6b2862d479637795f2697302d2151c4", null ],
    [ "Initialize", "struct__ARM__DRIVER__ETH__MAC.html#a8ca4a1b7a1fdf1b80f5ce62a7c96ec46", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__ETH__MAC.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__ETH__MAC.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "GetMacAddress", "struct__ARM__DRIVER__ETH__MAC.html#ac5ea64e26c9fb388da143623e141ea2b", null ],
    [ "SetMacAddress", "struct__ARM__DRIVER__ETH__MAC.html#a72c0a17baa8c8963f4b82e85047408c2", null ],
    [ "SetAddressFilter", "struct__ARM__DRIVER__ETH__MAC.html#ad05dc75d7f2651954f8b94845beedbc8", null ],
    [ "SendFrame", "struct__ARM__DRIVER__ETH__MAC.html#aa6842f1db08163d00d55ce5baf45d06c", null ],
    [ "ReadFrame", "struct__ARM__DRIVER__ETH__MAC.html#a1d90978073503dbc3633a3c67803c2bd", null ],
    [ "GetRxFrameSize", "struct__ARM__DRIVER__ETH__MAC.html#a87e7a83dd6682ad86c0239219ea7633b", null ],
    [ "GetRxFrameTime", "struct__ARM__DRIVER__ETH__MAC.html#a0d1289b86208cadaf02c33b3394d1afe", null ],
    [ "GetTxFrameTime", "struct__ARM__DRIVER__ETH__MAC.html#a555742134057689bf7bf517d3e718920", null ],
    [ "ControlTimer", "struct__ARM__DRIVER__ETH__MAC.html#a3f9cf722d704472c14e6793cb5bf2a8f", null ],
    [ "Control", "struct__ARM__DRIVER__ETH__MAC.html#aada421ec80747aaff075970a86743689", null ],
    [ "PHY_Read", "struct__ARM__DRIVER__ETH__MAC.html#adf4236d1a763166cdbd9e37da5d43a4c", null ],
    [ "PHY_Write", "struct__ARM__DRIVER__ETH__MAC.html#ae62095eabf28022911d41ea847e2c321", null ]
];