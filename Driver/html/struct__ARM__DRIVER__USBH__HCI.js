var struct__ARM__DRIVER__USBH__HCI =
[
    [ "GetVersion", "struct__ARM__DRIVER__USBH__HCI.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__USBH__HCI.html#a2a80e86ae7c54a0274135641d8d84887", null ],
    [ "Initialize", "struct__ARM__DRIVER__USBH__HCI.html#ad1ada53057d3613bedd01c6af6a5a7fe", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__USBH__HCI.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__USBH__HCI.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "PortVbusOnOff", "struct__ARM__DRIVER__USBH__HCI.html#afa156c566bc82e4fd2039035d6f48809", null ]
];