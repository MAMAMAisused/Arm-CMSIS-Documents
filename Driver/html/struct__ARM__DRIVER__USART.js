var struct__ARM__DRIVER__USART =
[
    [ "GetVersion", "struct__ARM__DRIVER__USART.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__USART.html#a029519e00dcf45d93ac69118059a3da4", null ],
    [ "Initialize", "struct__ARM__DRIVER__USART.html#a203b063b707975b54c14d6a8ca635fce", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__USART.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__USART.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "Send", "struct__ARM__DRIVER__USART.html#aa78ed8e1bb613524235f026ef3d83d8d", null ],
    [ "Receive", "struct__ARM__DRIVER__USART.html#a60be22e030c474232cf4725eddc1c4f1", null ],
    [ "Transfer", "struct__ARM__DRIVER__USART.html#a548618483a25cba5edf0dbddc815b1bc", null ],
    [ "GetTxCount", "struct__ARM__DRIVER__USART.html#a565faa6d5504335da50e919fb330c496", null ],
    [ "GetRxCount", "struct__ARM__DRIVER__USART.html#a0665c2c90545f8d1a2fdfddca90a55f0", null ],
    [ "Control", "struct__ARM__DRIVER__USART.html#aada421ec80747aaff075970a86743689", null ],
    [ "GetStatus", "struct__ARM__DRIVER__USART.html#ae7cdc185795ece317dbbdf3860afc0e4", null ],
    [ "SetModemControl", "struct__ARM__DRIVER__USART.html#a9e287c5cbf5ad28f992f2a00e973bcf9", null ],
    [ "GetModemStatus", "struct__ARM__DRIVER__USART.html#a53148eb1ec44c69e93631a3b40e22897", null ]
];