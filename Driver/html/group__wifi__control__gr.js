var group__wifi__control__gr =
[
    [ "WiFi Events", "group__wifi__event.html", "group__wifi__event" ],
    [ "ARM_WIFI_CAPABILITIES", "group__wifi__control__gr.html#structARM__WIFI__CAPABILITIES", null ],
    [ "ARM_WIFI_SignalEvent_t", "group__wifi__control__gr.html#gac0f04bbdd431c87a680626154c3e0a41", null ],
    [ "ARM_WIFI_GetVersion", "group__wifi__control__gr.html#gad349fb835926a508e2a33501c0dff6a2", null ],
    [ "ARM_WIFI_GetCapabilities", "group__wifi__control__gr.html#ga3496996799a4762e9958a8b6af683b4b", null ],
    [ "ARM_WIFI_Initialize", "group__wifi__control__gr.html#gadc4b9aaae0fdf7d489b1d81e3c2f3474", null ],
    [ "ARM_WIFI_Uninitialize", "group__wifi__control__gr.html#gae1e488d4ecc365e30712e6b1711a9c5d", null ],
    [ "ARM_WIFI_PowerControl", "group__wifi__control__gr.html#ga48d4cb7c00e96d2b928cc4468d4e623b", null ],
    [ "ARM_WIFI_GetModuleInfo", "group__wifi__control__gr.html#gabdc4c9966a3f22ac6e53119e71e67579", null ],
    [ "ARM_WIFI_SignalEvent", "group__wifi__control__gr.html#ga0c8243dee4bb161132692516f048bcec", null ]
];