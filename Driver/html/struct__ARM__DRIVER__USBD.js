var struct__ARM__DRIVER__USBD =
[
    [ "GetVersion", "struct__ARM__DRIVER__USBD.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__USBD.html#a101d0e13cbf31828147303ed81de98c6", null ],
    [ "Initialize", "struct__ARM__DRIVER__USBD.html#a25ae429ebb85114b0a96500b0d83eb47", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__USBD.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__USBD.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "DeviceConnect", "struct__ARM__DRIVER__USBD.html#a7072f86097c1cf34bcca8efd6b4ab2d0", null ],
    [ "DeviceDisconnect", "struct__ARM__DRIVER__USBD.html#a712bdda19781405ea9eb9d0b323f3159", null ],
    [ "DeviceGetState", "struct__ARM__DRIVER__USBD.html#a96253af0f634c80588008b129a6b9375", null ],
    [ "DeviceRemoteWakeup", "struct__ARM__DRIVER__USBD.html#af7269367d32e65bf0c63ffff3738a5f6", null ],
    [ "DeviceSetAddress", "struct__ARM__DRIVER__USBD.html#ac460354afc1555d494294169f502e47b", null ],
    [ "ReadSetupPacket", "struct__ARM__DRIVER__USBD.html#a3c32e1e5ed5e75dcdb2da4b56589e4d5", null ],
    [ "EndpointConfigure", "struct__ARM__DRIVER__USBD.html#aca66892724fab92218474d9a79f0a201", null ],
    [ "EndpointUnconfigure", "struct__ARM__DRIVER__USBD.html#aa57c0df3b131afd09086c98cc2642f56", null ],
    [ "EndpointStall", "struct__ARM__DRIVER__USBD.html#a35791368c85d42ff7629b3422a5c478f", null ],
    [ "EndpointTransfer", "struct__ARM__DRIVER__USBD.html#a7092587778f73ec3da0f1fae1f71d0fe", null ],
    [ "EndpointTransferGetResult", "struct__ARM__DRIVER__USBD.html#adfa070cf36125e64b8083aeaded83cf3", null ],
    [ "EndpointTransferAbort", "struct__ARM__DRIVER__USBD.html#af453dd2e303b6afc483e0683d013685b", null ],
    [ "GetFrameNumber", "struct__ARM__DRIVER__USBD.html#a1a08872b9d918d32d8ef48fc072d49c5", null ]
];