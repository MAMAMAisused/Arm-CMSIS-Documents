var annotated =
[
    [ "_ARM_CAN_CAPABILITIES", "Driver__CAN_8h.html#struct__ARM__CAN__CAPABILITIES", "Driver__CAN_8h_struct__ARM__CAN__CAPABILITIES" ],
    [ "_ARM_CAN_MSG_INFO", "Driver__CAN_8h.html#struct__ARM__CAN__MSG__INFO", "Driver__CAN_8h_struct__ARM__CAN__MSG__INFO" ],
    [ "_ARM_CAN_OBJ_CAPABILITIES", "Driver__CAN_8h.html#struct__ARM__CAN__OBJ__CAPABILITIES", "Driver__CAN_8h_struct__ARM__CAN__OBJ__CAPABILITIES" ],
    [ "_ARM_CAN_STATUS", "Driver__CAN_8h.html#struct__ARM__CAN__STATUS", "Driver__CAN_8h_struct__ARM__CAN__STATUS" ],
    [ "_ARM_DRIVER_CAN", "struct__ARM__DRIVER__CAN.html", "struct__ARM__DRIVER__CAN" ],
    [ "_ARM_DRIVER_ETH_MAC", "struct__ARM__DRIVER__ETH__MAC.html", "struct__ARM__DRIVER__ETH__MAC" ],
    [ "_ARM_DRIVER_ETH_PHY", "struct__ARM__DRIVER__ETH__PHY.html", "struct__ARM__DRIVER__ETH__PHY" ],
    [ "_ARM_DRIVER_FLASH", "struct__ARM__DRIVER__FLASH.html", "struct__ARM__DRIVER__FLASH" ],
    [ "_ARM_DRIVER_I2C", "struct__ARM__DRIVER__I2C.html", "struct__ARM__DRIVER__I2C" ],
    [ "_ARM_DRIVER_MCI", "struct__ARM__DRIVER__MCI.html", "struct__ARM__DRIVER__MCI" ],
    [ "_ARM_DRIVER_NAND", "struct__ARM__DRIVER__NAND.html", "struct__ARM__DRIVER__NAND" ],
    [ "_ARM_DRIVER_SAI", "struct__ARM__DRIVER__SAI.html", "struct__ARM__DRIVER__SAI" ],
    [ "_ARM_DRIVER_SPI", "struct__ARM__DRIVER__SPI.html", "struct__ARM__DRIVER__SPI" ],
    [ "_ARM_DRIVER_STORAGE", "struct__ARM__DRIVER__STORAGE.html", "struct__ARM__DRIVER__STORAGE" ],
    [ "_ARM_DRIVER_USART", "struct__ARM__DRIVER__USART.html", "struct__ARM__DRIVER__USART" ],
    [ "_ARM_DRIVER_USBD", "struct__ARM__DRIVER__USBD.html", "struct__ARM__DRIVER__USBD" ],
    [ "_ARM_DRIVER_USBH", "struct__ARM__DRIVER__USBH.html", "struct__ARM__DRIVER__USBH" ],
    [ "_ARM_DRIVER_USBH_HCI", "struct__ARM__DRIVER__USBH__HCI.html", "struct__ARM__DRIVER__USBH__HCI" ],
    [ "_ARM_DRIVER_VERSION", "Driver__Common_8h.html#struct__ARM__DRIVER__VERSION", "Driver__Common_8h_struct__ARM__DRIVER__VERSION" ],
    [ "_ARM_DRIVER_WIFI", "struct__ARM__DRIVER__WIFI.html", "struct__ARM__DRIVER__WIFI" ],
    [ "_ARM_ETH_LINK_INFO", "Driver__ETH_8h.html#struct__ARM__ETH__LINK__INFO", "Driver__ETH_8h_struct__ARM__ETH__LINK__INFO" ],
    [ "_ARM_ETH_MAC_ADDR", "Driver__ETH_8h.html#struct__ARM__ETH__MAC__ADDR", "Driver__ETH_8h_struct__ARM__ETH__MAC__ADDR" ],
    [ "_ARM_ETH_MAC_CAPABILITIES", "Driver__ETH__MAC_8h.html#struct__ARM__ETH__MAC__CAPABILITIES", "Driver__ETH__MAC_8h_struct__ARM__ETH__MAC__CAPABILITIES" ],
    [ "_ARM_ETH_MAC_TIME", "Driver__ETH__MAC_8h.html#struct__ARM__ETH__MAC__TIME", "Driver__ETH__MAC_8h_struct__ARM__ETH__MAC__TIME" ],
    [ "_ARM_FLASH_CAPABILITIES", "Driver__Flash_8h.html#struct__ARM__FLASH__CAPABILITIES", "Driver__Flash_8h_struct__ARM__FLASH__CAPABILITIES" ],
    [ "_ARM_FLASH_INFO", "Driver__Flash_8h.html#struct__ARM__FLASH__INFO", "Driver__Flash_8h_struct__ARM__FLASH__INFO" ],
    [ "_ARM_FLASH_SECTOR", "Driver__Flash_8h.html#struct__ARM__FLASH__SECTOR", "Driver__Flash_8h_struct__ARM__FLASH__SECTOR" ],
    [ "_ARM_FLASH_STATUS", "Driver__Flash_8h.html#struct__ARM__FLASH__STATUS", "Driver__Flash_8h_struct__ARM__FLASH__STATUS" ],
    [ "_ARM_I2C_CAPABILITIES", "Driver__I2C_8h.html#struct__ARM__I2C__CAPABILITIES", "Driver__I2C_8h_struct__ARM__I2C__CAPABILITIES" ],
    [ "_ARM_I2C_STATUS", "Driver__I2C_8h.html#struct__ARM__I2C__STATUS", "Driver__I2C_8h_struct__ARM__I2C__STATUS" ],
    [ "_ARM_MCI_CAPABILITIES", "Driver__MCI_8h.html#struct__ARM__MCI__CAPABILITIES", "Driver__MCI_8h_struct__ARM__MCI__CAPABILITIES" ],
    [ "_ARM_MCI_STATUS", "Driver__MCI_8h.html#struct__ARM__MCI__STATUS", "Driver__MCI_8h_struct__ARM__MCI__STATUS" ],
    [ "_ARM_NAND_CAPABILITIES", "Driver__NAND_8h.html#struct__ARM__NAND__CAPABILITIES", "Driver__NAND_8h_struct__ARM__NAND__CAPABILITIES" ],
    [ "_ARM_NAND_ECC_INFO", "Driver__NAND_8h.html#struct__ARM__NAND__ECC__INFO", "Driver__NAND_8h_struct__ARM__NAND__ECC__INFO" ],
    [ "_ARM_NAND_STATUS", "Driver__NAND_8h.html#struct__ARM__NAND__STATUS", "Driver__NAND_8h_struct__ARM__NAND__STATUS" ],
    [ "_ARM_SAI_CAPABILITIES", "Driver__SAI_8h.html#struct__ARM__SAI__CAPABILITIES", "Driver__SAI_8h_struct__ARM__SAI__CAPABILITIES" ],
    [ "_ARM_SAI_STATUS", "Driver__SAI_8h.html#struct__ARM__SAI__STATUS", "Driver__SAI_8h_struct__ARM__SAI__STATUS" ],
    [ "_ARM_SPI_CAPABILITIES", "Driver__SPI_8h.html#struct__ARM__SPI__CAPABILITIES", "Driver__SPI_8h_struct__ARM__SPI__CAPABILITIES" ],
    [ "_ARM_SPI_STATUS", "Driver__SPI_8h.html#struct__ARM__SPI__STATUS", "Driver__SPI_8h_struct__ARM__SPI__STATUS" ],
    [ "_ARM_STORAGE_BLOCK", "Driver__Storage_8h.html#struct__ARM__STORAGE__BLOCK", "Driver__Storage_8h_struct__ARM__STORAGE__BLOCK" ],
    [ "_ARM_STORAGE_BLOCK_ATTRIBUTES", "Driver__Storage_8h.html#struct__ARM__STORAGE__BLOCK__ATTRIBUTES", "Driver__Storage_8h_struct__ARM__STORAGE__BLOCK__ATTRIBUTES" ],
    [ "_ARM_STORAGE_CAPABILITIES", "Driver__Storage_8h.html#struct__ARM__STORAGE__CAPABILITIES", "Driver__Storage_8h_struct__ARM__STORAGE__CAPABILITIES" ],
    [ "_ARM_STORAGE_INFO", "Driver__Storage_8h.html#struct__ARM__STORAGE__INFO", "Driver__Storage_8h_struct__ARM__STORAGE__INFO" ],
    [ "_ARM_STORAGE_SECURITY_FEATURES", "Driver__Storage_8h.html#struct__ARM__STORAGE__SECURITY__FEATURES", "Driver__Storage_8h_struct__ARM__STORAGE__SECURITY__FEATURES" ],
    [ "_ARM_STORAGE_STATUS", "Driver__Storage_8h.html#struct__ARM__STORAGE__STATUS", "Driver__Storage_8h_struct__ARM__STORAGE__STATUS" ],
    [ "_ARM_USART_CAPABILITIES", "Driver__USART_8h.html#struct__ARM__USART__CAPABILITIES", "Driver__USART_8h_struct__ARM__USART__CAPABILITIES" ],
    [ "_ARM_USART_MODEM_STATUS", "Driver__USART_8h.html#struct__ARM__USART__MODEM__STATUS", "Driver__USART_8h_struct__ARM__USART__MODEM__STATUS" ],
    [ "_ARM_USART_STATUS", "Driver__USART_8h.html#struct__ARM__USART__STATUS", "Driver__USART_8h_struct__ARM__USART__STATUS" ],
    [ "_ARM_USBD_CAPABILITIES", "Driver__USBD_8h.html#struct__ARM__USBD__CAPABILITIES", "Driver__USBD_8h_struct__ARM__USBD__CAPABILITIES" ],
    [ "_ARM_USBD_STATE", "Driver__USBD_8h.html#struct__ARM__USBD__STATE", "Driver__USBD_8h_struct__ARM__USBD__STATE" ],
    [ "_ARM_USBH_CAPABILITIES", "Driver__USBH_8h.html#struct__ARM__USBH__CAPABILITIES", "Driver__USBH_8h_struct__ARM__USBH__CAPABILITIES" ],
    [ "_ARM_USBH_HCI_CAPABILITIES", "Driver__USBH_8h.html#struct__ARM__USBH__HCI__CAPABILITIES", "Driver__USBH_8h_struct__ARM__USBH__HCI__CAPABILITIES" ],
    [ "_ARM_USBH_PORT_STATE", "Driver__USBH_8h.html#struct__ARM__USBH__PORT__STATE", "Driver__USBH_8h_struct__ARM__USBH__PORT__STATE" ],
    [ "_ARM_WIFI_CAPABILITIES", "Driver__WiFi_8h.html#struct__ARM__WIFI__CAPABILITIES", "Driver__WiFi_8h_struct__ARM__WIFI__CAPABILITIES" ],
    [ "ARM_CAN_CAPABILITIES", "group__can__interface__gr.html#structARM__CAN__CAPABILITIES", null ],
    [ "ARM_CAN_MSG_INFO", "group__can__interface__gr.html#structARM__CAN__MSG__INFO", null ],
    [ "ARM_CAN_OBJ_CAPABILITIES", "group__can__interface__gr.html#structARM__CAN__OBJ__CAPABILITIES", null ],
    [ "ARM_CAN_STATUS", "group__can__interface__gr.html#structARM__CAN__STATUS", null ],
    [ "ARM_DRIVER_CAN", "group__can__interface__gr.html#structARM__DRIVER__CAN", null ],
    [ "ARM_DRIVER_ETH_MAC", "group__eth__mac__interface__gr.html#structARM__DRIVER__ETH__MAC", null ],
    [ "ARM_DRIVER_ETH_PHY", "group__eth__phy__interface__gr.html#structARM__DRIVER__ETH__PHY", null ],
    [ "ARM_DRIVER_FLASH", "group__flash__interface__gr.html#structARM__DRIVER__FLASH", null ],
    [ "ARM_DRIVER_I2C", "group__i2c__interface__gr.html#structARM__DRIVER__I2C", null ],
    [ "ARM_DRIVER_MCI", "group__mci__interface__gr.html#structARM__DRIVER__MCI", null ],
    [ "ARM_DRIVER_NAND", "group__nand__interface__gr.html#structARM__DRIVER__NAND", null ],
    [ "ARM_DRIVER_SAI", "group__sai__interface__gr.html#structARM__DRIVER__SAI", null ],
    [ "ARM_DRIVER_SPI", "group__spi__interface__gr.html#structARM__DRIVER__SPI", null ],
    [ "ARM_DRIVER_STORAGE", "group__storage__interface__gr.html#structARM__DRIVER__STORAGE", null ],
    [ "ARM_DRIVER_USART", "group__usart__interface__gr.html#structARM__DRIVER__USART", null ],
    [ "ARM_DRIVER_USBD", "group__usbd__interface__gr.html#structARM__DRIVER__USBD", null ],
    [ "ARM_DRIVER_USBH", "group__usbh__host__gr.html#structARM__DRIVER__USBH", null ],
    [ "ARM_DRIVER_USBH_HCI", "group__usbh__hci__gr.html#structARM__DRIVER__USBH__HCI", null ],
    [ "ARM_DRIVER_VERSION", "group__common__drv__gr.html#structARM__DRIVER__VERSION", null ],
    [ "ARM_DRIVER_WIFI", "group__wifi__interface__gr.html#structARM__DRIVER__WIFI", null ],
    [ "ARM_ETH_LINK_INFO", "group__eth__interface__gr.html#structARM__ETH__LINK__INFO", null ],
    [ "ARM_ETH_MAC_ADDR", "group__eth__interface__gr.html#structARM__ETH__MAC__ADDR", null ],
    [ "ARM_ETH_MAC_CAPABILITIES", "group__eth__mac__interface__gr.html#structARM__ETH__MAC__CAPABILITIES", null ],
    [ "ARM_ETH_MAC_TIME", "group__eth__mac__interface__gr.html#structARM__ETH__MAC__TIME", null ],
    [ "ARM_FLASH_CAPABILITIES", "group__flash__interface__gr.html#structARM__FLASH__CAPABILITIES", null ],
    [ "ARM_FLASH_INFO", "group__flash__interface__gr.html#structARM__FLASH__INFO", null ],
    [ "ARM_FLASH_SECTOR", "group__flash__interface__gr.html#structARM__FLASH__SECTOR", null ],
    [ "ARM_FLASH_STATUS", "group__flash__interface__gr.html#structARM__FLASH__STATUS", null ],
    [ "ARM_I2C_CAPABILITIES", "group__i2c__interface__gr.html#structARM__I2C__CAPABILITIES", null ],
    [ "ARM_I2C_STATUS", "group__i2c__interface__gr.html#structARM__I2C__STATUS", null ],
    [ "ARM_MCI_CAPABILITIES", "group__mci__interface__gr.html#structARM__MCI__CAPABILITIES", null ],
    [ "ARM_MCI_STATUS", "group__mci__interface__gr.html#structARM__MCI__STATUS", null ],
    [ "ARM_NAND_CAPABILITIES", "group__nand__interface__gr.html#structARM__NAND__CAPABILITIES", null ],
    [ "ARM_NAND_ECC_INFO", "group__nand__interface__gr.html#structARM__NAND__ECC__INFO", null ],
    [ "ARM_NAND_STATUS", "group__nand__interface__gr.html#structARM__NAND__STATUS", null ],
    [ "ARM_SAI_CAPABILITIES", "group__sai__interface__gr.html#structARM__SAI__CAPABILITIES", null ],
    [ "ARM_SAI_STATUS", "group__sai__interface__gr.html#structARM__SAI__STATUS", null ],
    [ "ARM_SPI_CAPABILITIES", "group__spi__interface__gr.html#structARM__SPI__CAPABILITIES", null ],
    [ "ARM_SPI_STATUS", "group__spi__interface__gr.html#structARM__SPI__STATUS", null ],
    [ "ARM_STORAGE_BLOCK", "group__storage__interface__gr.html#structARM__STORAGE__BLOCK", null ],
    [ "ARM_STORAGE_BLOCK_ATTRIBUTES", "group__storage__interface__gr.html#structARM__STORAGE__BLOCK__ATTRIBUTES", null ],
    [ "ARM_STORAGE_CAPABILITIES", "group__storage__interface__gr.html#structARM__STORAGE__CAPABILITIES", null ],
    [ "ARM_STORAGE_INFO", "group__storage__interface__gr.html#structARM__STORAGE__INFO", null ],
    [ "ARM_STORAGE_STATUS", "group__storage__interface__gr.html#structARM__STORAGE__STATUS", null ],
    [ "ARM_USART_CAPABILITIES", "group__usart__interface__gr.html#structARM__USART__CAPABILITIES", null ],
    [ "ARM_USART_MODEM_STATUS", "group__usart__interface__gr.html#structARM__USART__MODEM__STATUS", null ],
    [ "ARM_USART_STATUS", "group__usart__interface__gr.html#structARM__USART__STATUS", null ],
    [ "ARM_USBD_CAPABILITIES", "group__usbd__interface__gr.html#structARM__USBD__CAPABILITIES", null ],
    [ "ARM_USBD_STATE", "group__usbd__interface__gr.html#structARM__USBD__STATE", null ],
    [ "ARM_USBH_CAPABILITIES", "group__usbh__host__gr.html#structARM__USBH__CAPABILITIES", null ],
    [ "ARM_USBH_HCI_CAPABILITIES", "group__usbh__hci__gr.html#structARM__USBH__HCI__CAPABILITIES", null ],
    [ "ARM_USBH_PORT_STATE", "group__usbh__host__gr.html#structARM__USBH__PORT__STATE", null ],
    [ "ARM_WIFI_CAPABILITIES", "group__wifi__control__gr.html#structARM__WIFI__CAPABILITIES", null ],
    [ "ARM_WIFI_CONFIG_s", "Driver__WiFi_8h.html#structARM__WIFI__CONFIG__s", "Driver__WiFi_8h_structARM__WIFI__CONFIG__s" ],
    [ "ARM_WIFI_CONFIG_t", "group__wifi__management__gr.html#structARM__WIFI__CONFIG__t", null ],
    [ "ARM_WIFI_NET_INFO_s", "Driver__WiFi_8h.html#structARM__WIFI__NET__INFO__s", "Driver__WiFi_8h_structARM__WIFI__NET__INFO__s" ],
    [ "ARM_WIFI_NET_INFO_t", "group__wifi__management__gr.html#structARM__WIFI__NET__INFO__t", null ],
    [ "ARM_WIFI_SCAN_INFO_s", "Driver__WiFi_8h.html#structARM__WIFI__SCAN__INFO__s", "Driver__WiFi_8h_structARM__WIFI__SCAN__INFO__s" ],
    [ "ARM_WIFI_SCAN_INFO_t", "group__wifi__management__gr.html#structARM__WIFI__SCAN__INFO__t", null ]
];