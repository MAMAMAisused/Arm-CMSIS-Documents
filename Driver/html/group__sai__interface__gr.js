var group__sai__interface__gr =
[
    [ "Status Error Codes", "group__sai__execution__status.html", "group__sai__execution__status" ],
    [ "SAI Events", "group__SAI__events.html", "group__SAI__events" ],
    [ "SAI Control Codes", "group__sai__control.html", "group__sai__control" ],
    [ "ARM_DRIVER_SAI", "group__sai__interface__gr.html#structARM__DRIVER__SAI", null ],
    [ "ARM_SAI_CAPABILITIES", "group__sai__interface__gr.html#structARM__SAI__CAPABILITIES", null ],
    [ "ARM_SAI_STATUS", "group__sai__interface__gr.html#structARM__SAI__STATUS", null ],
    [ "ARM_SAI_SignalEvent_t", "group__sai__interface__gr.html#gad8ca8e2459e540928f6315b3df6da0ee", null ],
    [ "ARM_SAI_GetVersion", "group__sai__interface__gr.html#ga786b1970a788a4dfc6156b42364e52f8", null ],
    [ "ARM_SAI_GetCapabilities", "group__sai__interface__gr.html#gac6c636757944eaf25aebf312a67665aa", null ],
    [ "ARM_SAI_Initialize", "group__sai__interface__gr.html#ga89622a02ca1e7affb1a01eefacb6f54c", null ],
    [ "ARM_SAI_Uninitialize", "group__sai__interface__gr.html#gabdefafaba6f072cfd7ed6f8f132422b6", null ],
    [ "ARM_SAI_PowerControl", "group__sai__interface__gr.html#gacdec50a3dd5902de601caa7397c1dabc", null ],
    [ "ARM_SAI_Send", "group__sai__interface__gr.html#ga8bb6866c535adeb930bc4a847d476fcd", null ],
    [ "ARM_SAI_Receive", "group__sai__interface__gr.html#ga2d55f506cef9d2849cbe418146086d98", null ],
    [ "ARM_SAI_GetTxCount", "group__sai__interface__gr.html#gaa9805f9d32aee205f787e625a58e8898", null ],
    [ "ARM_SAI_GetRxCount", "group__sai__interface__gr.html#ga2c571fcc8b9632c25a64043bc2b2baec", null ],
    [ "ARM_SAI_Control", "group__sai__interface__gr.html#ga405a0769c33da6801055db0fb9b6c869", null ],
    [ "ARM_SAI_GetStatus", "group__sai__interface__gr.html#ga6a202b57697f0f7a9742e76b33d5eeec", null ],
    [ "ARM_SAI_SignalEvent", "group__sai__interface__gr.html#gaedf3347cb25d6bf2faad1bbb35ad79f4", null ]
];