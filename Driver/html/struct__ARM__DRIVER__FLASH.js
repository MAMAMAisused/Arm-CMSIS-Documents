var struct__ARM__DRIVER__FLASH =
[
    [ "GetVersion", "struct__ARM__DRIVER__FLASH.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__FLASH.html#a0cf3a83dc816135c28b4b41b2b0cd78c", null ],
    [ "Initialize", "struct__ARM__DRIVER__FLASH.html#a20410f02ef61c58403974ca78b8a624d", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__FLASH.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__FLASH.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "ReadData", "struct__ARM__DRIVER__FLASH.html#a969eab1615584c2c4ab7dc5e9ab14aa0", null ],
    [ "ProgramData", "struct__ARM__DRIVER__FLASH.html#a53b8915c1d727da744ace4108f05279d", null ],
    [ "EraseSector", "struct__ARM__DRIVER__FLASH.html#a01daf29b2d6e464191bcd1e2957a0028", null ],
    [ "EraseChip", "struct__ARM__DRIVER__FLASH.html#aa1350937073553636866a22aa86cf85f", null ],
    [ "GetStatus", "struct__ARM__DRIVER__FLASH.html#a7aa4bf360e7f8435cb4ba44729bcd66c", null ],
    [ "GetInfo", "struct__ARM__DRIVER__FLASH.html#a22a438e69bb2992170c7c118290f603f", null ]
];