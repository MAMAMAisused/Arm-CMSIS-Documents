var struct__ARM__DRIVER__SAI =
[
    [ "GetVersion", "struct__ARM__DRIVER__SAI.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__SAI.html#a896ae8111cffa7ac1b331f06d08deb4d", null ],
    [ "Initialize", "struct__ARM__DRIVER__SAI.html#aa6d94b3964e5dc0ce5b2e7d3444146e5", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__SAI.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__SAI.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "Send", "struct__ARM__DRIVER__SAI.html#aa78ed8e1bb613524235f026ef3d83d8d", null ],
    [ "Receive", "struct__ARM__DRIVER__SAI.html#a60be22e030c474232cf4725eddc1c4f1", null ],
    [ "GetTxCount", "struct__ARM__DRIVER__SAI.html#a565faa6d5504335da50e919fb330c496", null ],
    [ "GetRxCount", "struct__ARM__DRIVER__SAI.html#a0665c2c90545f8d1a2fdfddca90a55f0", null ],
    [ "Control", "struct__ARM__DRIVER__SAI.html#a9b0dfc10bacbdd8326e18f565691a4d2", null ],
    [ "GetStatus", "struct__ARM__DRIVER__SAI.html#a48656ff22cf0930adb75805fa450f77b", null ]
];