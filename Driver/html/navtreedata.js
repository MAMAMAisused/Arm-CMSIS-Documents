/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS-Driver", "index.html", [
    [ "Overview", "index.html", null ],
    [ "Revision History of CMSIS-Driver", "driver_revisionHistory.html", null ],
    [ "Theory of Operation", "theoryOperation.html", [
      [ "Common Driver Functions", "theoryOperation.html#DriverFunctions", [
        [ "CMSIS-Driver in ARM::CMSIS Pack", "index.html#autotoc_md1", null ],
        [ "Cortex-M Processor Mode", "theoryOperation.html#ProcessorMode", null ]
      ] ],
      [ "Function Call Sequence", "theoryOperation.html#CallSequence", [
        [ "Start Sequence", "theoryOperation.html#CS_start", null ],
        [ "Stop Sequence", "theoryOperation.html#CS_stop", null ]
      ] ],
      [ "Shared I/O Pins", "theoryOperation.html#Share_IO", null ],
      [ "Data Transfer Functions", "theoryOperation.html#Data_Xfer_Functions", null ],
      [ "Access Struct", "theoryOperation.html#AccessStruct", [
        [ "Driver Instances", "theoryOperation.html#DriverInstances", null ]
      ] ],
      [ "Driver Configuration", "theoryOperation.html#DriverConfiguration", null ],
      [ "Code Example", "theoryOperation.html#CodeExample", null ]
    ] ],
    [ "Reference Implementation", "referenceImplementation.html", [
      [ "Driver Header Files", "referenceImplementation.html#DriverHeaderFiles", null ],
      [ "Driver Template Files", "referenceImplementation.html#DriverTemplates", null ],
      [ "Driver Examples", "referenceImplementation.html#DriverExamples", null ]
    ] ],
    [ "Driver Validation", "driverValidation.html", [
      [ "Sample Test Output", "driverValidation.html#test_output", null ],
      [ "Setup for Loop Back Communication", "driverValidation.html#loop_back_setup", null ]
    ] ],
    [ "Reference", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", "annotated" ],
    [ "Data Structure Index", "classes.html", null ],
    [ "Data Fields", "functions.html", [
      [ "All", "functions.html", "functions_dup" ],
      [ "Variables", "functions_vars.html", "functions_vars" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group__eth__mac__configuration__ctrls.html#ga8acefed744d8397a1777b9fd0e6230d2",
"group__nand__bus__mode__codes.html#gac7743aeb6411b97f9fc6a24b556f4963",
"group__storage__interface__gr.html#gabd20d561854c06918b0515b6c1bba230",
"group__wifi__socket__type.html",
"theoryOperation.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';