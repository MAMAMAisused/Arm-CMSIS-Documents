var Driver__CAN_8h_struct__ARM__CAN__CAPABILITIES =
[
    [ "num_objects", "Driver__CAN_8h.html#a69bd1a164443cf6f501489f4d31f4681", null ],
    [ "reentrant_operation", "Driver__CAN_8h.html#ae0514834750c7452431717a881471e2b", null ],
    [ "fd_mode", "Driver__CAN_8h.html#a15d22d5906d419ed1a7ca0968be00a04", null ],
    [ "restricted_mode", "Driver__CAN_8h.html#a93008ac105806db484e78e0582ca118c", null ],
    [ "monitor_mode", "Driver__CAN_8h.html#a176f42e68d9cba86b3594c40044b86c6", null ],
    [ "internal_loopback", "Driver__CAN_8h.html#af19cdbb26d3496ed7dd63a59a7c7711f", null ],
    [ "external_loopback", "Driver__CAN_8h.html#a2b76df7e4bfbdd9866cc906415e626c9", null ],
    [ "reserved", "Driver__CAN_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ]
];