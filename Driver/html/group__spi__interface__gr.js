var group__spi__interface__gr =
[
    [ "Status Error Codes", "group__spi__execution__status.html", "group__spi__execution__status" ],
    [ "SPI Events", "group__SPI__events.html", "group__SPI__events" ],
    [ "SPI Control Codes", "group__SPI__control.html", "group__SPI__control" ],
    [ "ARM_DRIVER_SPI", "group__spi__interface__gr.html#structARM__DRIVER__SPI", null ],
    [ "ARM_SPI_CAPABILITIES", "group__spi__interface__gr.html#structARM__SPI__CAPABILITIES", null ],
    [ "ARM_SPI_STATUS", "group__spi__interface__gr.html#structARM__SPI__STATUS", null ],
    [ "ARM_SPI_SignalEvent_t", "group__spi__interface__gr.html#gafde9205364241ee81290adc0481c6640", null ],
    [ "ARM_SPI_GetVersion", "group__spi__interface__gr.html#gad5db9209ef1d64a7915a7278d6a402c8", null ],
    [ "ARM_SPI_GetCapabilities", "group__spi__interface__gr.html#gaf4823a11ab5efcd47c79b13801513ddc", null ],
    [ "ARM_SPI_Initialize", "group__spi__interface__gr.html#ga1a3c11ed523a4355cd91069527945906", null ],
    [ "ARM_SPI_Uninitialize", "group__spi__interface__gr.html#ga0c480ee3eabb82fc746e89741ed2e03e", null ],
    [ "ARM_SPI_PowerControl", "group__spi__interface__gr.html#ga1a1e7e80ea32ae381b75213c32aa8067", null ],
    [ "ARM_SPI_Send", "group__spi__interface__gr.html#gab2a303d1071e926280d50682f4808479", null ],
    [ "ARM_SPI_Receive", "group__spi__interface__gr.html#ga726aff54e782ed9b47f7ba1280a3d8f6", null ],
    [ "ARM_SPI_Transfer", "group__spi__interface__gr.html#gaa24026b3822c10272e301f1505136ec2", null ],
    [ "ARM_SPI_GetDataCount", "group__spi__interface__gr.html#gaaaecaaf4ec1922f22e7f9de63af5ccdb", null ],
    [ "ARM_SPI_Control", "group__spi__interface__gr.html#gad18d229992598d6677bec250015e5d1a", null ],
    [ "ARM_SPI_GetStatus", "group__spi__interface__gr.html#ga60d33d8788a76c388cc36e066240b817", null ],
    [ "ARM_SPI_SignalEvent", "group__spi__interface__gr.html#ga505b2d787348d51351d38fee98ccba7e", null ]
];