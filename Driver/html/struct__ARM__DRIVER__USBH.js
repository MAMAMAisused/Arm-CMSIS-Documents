var struct__ARM__DRIVER__USBH =
[
    [ "GetVersion", "struct__ARM__DRIVER__USBH.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__USBH.html#a7baf2dc4ce5b0de7e26fd7afceee781f", null ],
    [ "Initialize", "struct__ARM__DRIVER__USBH.html#a99d8b962bcac5363aa018c5dde11986a", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__USBH.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__USBH.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "PortVbusOnOff", "struct__ARM__DRIVER__USBH.html#afa156c566bc82e4fd2039035d6f48809", null ],
    [ "PortReset", "struct__ARM__DRIVER__USBH.html#ab4fd95a5e53517c8e8406f4074888ef1", null ],
    [ "PortSuspend", "struct__ARM__DRIVER__USBH.html#ab671bdec65a9ac1b7a64c59a60424b0a", null ],
    [ "PortResume", "struct__ARM__DRIVER__USBH.html#a6601b33486086ce9f8185a99488da70b", null ],
    [ "PortGetState", "struct__ARM__DRIVER__USBH.html#a069eb56a336879446539d8a7368aebbe", null ],
    [ "PipeCreate", "struct__ARM__DRIVER__USBH.html#a8064347fd39d5e679f581de2b4a939bf", null ],
    [ "PipeModify", "struct__ARM__DRIVER__USBH.html#a41fa2286b1765f449d590e01f6fbbb86", null ],
    [ "PipeDelete", "struct__ARM__DRIVER__USBH.html#a590038b57b73318e844f56ee16750d93", null ],
    [ "PipeReset", "struct__ARM__DRIVER__USBH.html#a40abaa0bbb4f165dabbaf6767c02d1a6", null ],
    [ "PipeTransfer", "struct__ARM__DRIVER__USBH.html#a7fc93680845edab59e26005f8d13355c", null ],
    [ "PipeTransferGetResult", "struct__ARM__DRIVER__USBH.html#af257d9e9a2341ac5e3b2826c58195352", null ],
    [ "PipeTransferAbort", "struct__ARM__DRIVER__USBH.html#a9ee40c317ad2e370ae2b064552f1b5fa", null ],
    [ "GetFrameNumber", "struct__ARM__DRIVER__USBH.html#a1a08872b9d918d32d8ef48fc072d49c5", null ]
];