var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuvw",
  1: "_a",
  2: "dg",
  3: "a",
  4: "abcdefghilmnoprstuvw",
  5: "a",
  6: "_",
  7: "a",
  8: "_a",
  9: "cefimnsuw",
  10: "dort"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

