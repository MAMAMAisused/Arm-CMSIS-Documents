var searchData=
[
  ['vbus_1611',['vbus',['../Driver__USBD_8h.html#aa961d5fb2bd3d2960578f1ac3b903070',1,'_ARM_USBD_STATE']]],
  ['vbus_5fdetection_1612',['vbus_detection',['../Driver__USBD_8h.html#a6673fc1aa13f62122ecf51e52a605c6e',1,'_ARM_USBD_CAPABILITIES']]],
  ['vcc_1613',['vcc',['../Driver__NAND_8h.html#a35cfa22b2140b109fe24b97c42d5a5ed',1,'_ARM_NAND_CAPABILITIES']]],
  ['vcc_5f1v8_1614',['vcc_1v8',['../Driver__NAND_8h.html#a0e7d3b9258d468492b22de55d855a06e',1,'_ARM_NAND_CAPABILITIES']]],
  ['vccq_1615',['vccq',['../Driver__MCI_8h.html#ab1cdfce6eb051bed7b904e0fd1719afa',1,'_ARM_MCI_CAPABILITIES::vccq()'],['../Driver__NAND_8h.html#ab1cdfce6eb051bed7b904e0fd1719afa',1,'_ARM_NAND_CAPABILITIES::vccq()']]],
  ['vccq_5f1v2_1616',['vccq_1v2',['../Driver__MCI_8h.html#af4f95215005e38700ef527714932b361',1,'_ARM_MCI_CAPABILITIES']]],
  ['vccq_5f1v8_1617',['vccq_1v8',['../Driver__MCI_8h.html#a1896a7548bb6fab285f23cc0d0b23d7d',1,'_ARM_MCI_CAPABILITIES::vccq_1v8()'],['../Driver__NAND_8h.html#a1896a7548bb6fab285f23cc0d0b23d7d',1,'_ARM_NAND_CAPABILITIES::vccq_1v8()']]],
  ['vdd_1618',['vdd',['../Driver__MCI_8h.html#a414baec222a72be862e262f02b821dce',1,'_ARM_MCI_CAPABILITIES']]],
  ['vdd_5f1v8_1619',['vdd_1v8',['../Driver__MCI_8h.html#abeb0330f882ebed8cabde782652233dd',1,'_ARM_MCI_CAPABILITIES']]],
  ['virtual_5fpage_5fsize_1620',['virtual_page_size',['../Driver__NAND_8h.html#aa270f95e67fdf1e9137c61f2045b7636',1,'_ARM_NAND_ECC_INFO']]],
  ['vpp_1621',['vpp',['../Driver__NAND_8h.html#a75b97f7c917bba90b2f5c747d6857d23',1,'_ARM_NAND_CAPABILITIES']]]
];
