var Driver__SAI_8h_struct__ARM__SAI__STATUS =
[
    [ "tx_busy", "Driver__SAI_8h.html#a2c6d2b67fba3f3e084e96a6bc7fcac6b", null ],
    [ "rx_busy", "Driver__SAI_8h.html#a9f5baee58ed41b382628a82a0b1cbcb4", null ],
    [ "tx_underflow", "Driver__SAI_8h.html#a048f45e9d2257a21821f81d9edd17b72", null ],
    [ "rx_overflow", "Driver__SAI_8h.html#ac403aefd9bce8b0172e1996c0f3dd8aa", null ],
    [ "frame_error", "Driver__SAI_8h.html#a1b4f69a2caf19ef9fd75cf27ae3932f9", null ],
    [ "reserved", "Driver__SAI_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ]
];