var struct__ARM__DRIVER__MCI =
[
    [ "GetVersion", "struct__ARM__DRIVER__MCI.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__MCI.html#aeff37b01231930d051cc348849d22544", null ],
    [ "Initialize", "struct__ARM__DRIVER__MCI.html#af107d5c0bdf8ac83000ff4d882e04473", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__MCI.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__MCI.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "CardPower", "struct__ARM__DRIVER__MCI.html#a87908778b996f45e733fe3bfabfc2e3b", null ],
    [ "ReadCD", "struct__ARM__DRIVER__MCI.html#ad509d0b09cfcd7b22fbb4c5c35e3afda", null ],
    [ "ReadWP", "struct__ARM__DRIVER__MCI.html#a8721c514dd035780c62e8bc59e0fc0a3", null ],
    [ "SendCommand", "struct__ARM__DRIVER__MCI.html#a20ed13fd6aa57782c21a51e87d319471", null ],
    [ "SetupTransfer", "struct__ARM__DRIVER__MCI.html#a831c410e4fd2d2a34cf704cea2185e64", null ],
    [ "AbortTransfer", "struct__ARM__DRIVER__MCI.html#abf93846b57f586a7216103aa2bc72455", null ],
    [ "Control", "struct__ARM__DRIVER__MCI.html#aada421ec80747aaff075970a86743689", null ],
    [ "GetStatus", "struct__ARM__DRIVER__MCI.html#a92e789969cc0e7f39a5df188f3e7fe80", null ]
];