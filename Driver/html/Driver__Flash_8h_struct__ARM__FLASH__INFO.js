var Driver__Flash_8h_struct__ARM__FLASH__INFO =
[
    [ "sector_info", "Driver__Flash_8h.html#a8dfb9d5160358e45293bba527762238d", null ],
    [ "sector_count", "Driver__Flash_8h.html#a50947f9a42bbaa2d68d6e5079150d7bf", null ],
    [ "sector_size", "Driver__Flash_8h.html#a7d37def484362c6e97a2d75144080b1d", null ],
    [ "page_size", "Driver__Flash_8h.html#a9dd3e47e968a8f6beb5d88c6d1b7ebe9", null ],
    [ "program_unit", "Driver__Flash_8h.html#a483c41066757e2865bf3a27a2a627a54", null ],
    [ "erased_value", "Driver__Flash_8h.html#a85c3826bf20669d38e466dfd376994db", null ],
    [ "reserved", "Driver__Flash_8h.html#a72aca6ea6d8153b28ea8f139b932ec3e", null ]
];