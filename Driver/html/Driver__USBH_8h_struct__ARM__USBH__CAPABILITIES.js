var Driver__USBH_8h_struct__ARM__USBH__CAPABILITIES =
[
    [ "port_mask", "Driver__USBH_8h.html#ac37c09b54483c2a1e41fa8a976721fc4", null ],
    [ "auto_split", "Driver__USBH_8h.html#a37eab684b9a8aa496bfec9fede42fe27", null ],
    [ "event_connect", "Driver__USBH_8h.html#ae76b779cb9fdf447b20c8b6beed2d534", null ],
    [ "event_disconnect", "Driver__USBH_8h.html#ab83941051cac8e19807b887354dc42fc", null ],
    [ "event_overcurrent", "Driver__USBH_8h.html#acd3087b3a4a7691595dd75568c12d696", null ],
    [ "reserved", "Driver__USBH_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ]
];