var struct__ARM__DRIVER__SPI =
[
    [ "GetVersion", "struct__ARM__DRIVER__SPI.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__SPI.html#a782611d054c4570a7bf258d2d11c9186", null ],
    [ "Initialize", "struct__ARM__DRIVER__SPI.html#aca8c87a078e30b7f260c1b6e75a19dc5", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__SPI.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__SPI.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "Send", "struct__ARM__DRIVER__SPI.html#aa78ed8e1bb613524235f026ef3d83d8d", null ],
    [ "Receive", "struct__ARM__DRIVER__SPI.html#a60be22e030c474232cf4725eddc1c4f1", null ],
    [ "Transfer", "struct__ARM__DRIVER__SPI.html#a548618483a25cba5edf0dbddc815b1bc", null ],
    [ "GetDataCount", "struct__ARM__DRIVER__SPI.html#a3dea78d5b878ae81576a7d64dc2bc959", null ],
    [ "Control", "struct__ARM__DRIVER__SPI.html#aada421ec80747aaff075970a86743689", null ],
    [ "GetStatus", "struct__ARM__DRIVER__SPI.html#a2f6028e091883fdbf7dcc3c7c9e39710", null ]
];