var group__i2c__interface__gr =
[
    [ "I2C Events", "group__I2C__events.html", "group__I2C__events" ],
    [ "I2C Control Codes", "group__i2c__control__gr.html", "group__i2c__control__gr" ],
    [ "I2C Address Flags", "group__i2c__address__flags.html", "group__i2c__address__flags" ],
    [ "ARM_DRIVER_I2C", "group__i2c__interface__gr.html#structARM__DRIVER__I2C", null ],
    [ "ARM_I2C_CAPABILITIES", "group__i2c__interface__gr.html#structARM__I2C__CAPABILITIES", null ],
    [ "ARM_I2C_STATUS", "group__i2c__interface__gr.html#structARM__I2C__STATUS", null ],
    [ "ARM_I2C_SignalEvent_t", "group__i2c__interface__gr.html#ga24277c48248a09b0dd7f12bbe22ce13c", null ],
    [ "ARM_I2C_GetVersion", "group__i2c__interface__gr.html#ga956bd87590c7fb6e23609a0abfb5412c", null ],
    [ "ARM_I2C_GetCapabilities", "group__i2c__interface__gr.html#gad20e6731f627aa7b9d6e99a50806122e", null ],
    [ "ARM_I2C_Initialize", "group__i2c__interface__gr.html#ga79d2f7d01b3a681d1cf0d70ac6692696", null ],
    [ "ARM_I2C_Uninitialize", "group__i2c__interface__gr.html#ga30d8bf600b6b3182a1f867407b3d6e75", null ],
    [ "ARM_I2C_PowerControl", "group__i2c__interface__gr.html#ga734a69200e063fdbfb5110062afe9329", null ],
    [ "ARM_I2C_MasterTransmit", "group__i2c__interface__gr.html#ga8bf4214580149d5a5d2360f71f0feb94", null ],
    [ "ARM_I2C_MasterReceive", "group__i2c__interface__gr.html#gafa22504bcf88a85584dfe6e0dd270ad5", null ],
    [ "ARM_I2C_SlaveTransmit", "group__i2c__interface__gr.html#gafe164f30eba78f066272373b98a62cd4", null ],
    [ "ARM_I2C_SlaveReceive", "group__i2c__interface__gr.html#gae3c9abccd1d377385d3d4cfe29035164", null ],
    [ "ARM_I2C_GetDataCount", "group__i2c__interface__gr.html#ga19db20ad8d7fde84d07f6db4d75f4b7c", null ],
    [ "ARM_I2C_Control", "group__i2c__interface__gr.html#ga828f5fa289d065675ef78a9a73d129dc", null ],
    [ "ARM_I2C_GetStatus", "group__i2c__interface__gr.html#gaba4e0f3eb4018e7dafd51b675c465f3e", null ],
    [ "ARM_I2C_SignalEvent", "group__i2c__interface__gr.html#gad4f93d2895794b416dc8d8e9de91c05e", null ]
];