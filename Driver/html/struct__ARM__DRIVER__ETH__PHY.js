var struct__ARM__DRIVER__ETH__PHY =
[
    [ "GetVersion", "struct__ARM__DRIVER__ETH__PHY.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "Initialize", "struct__ARM__DRIVER__ETH__PHY.html#a5f8503500a3ae4e89c5438f905390e70", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__ETH__PHY.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__ETH__PHY.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "SetInterface", "struct__ARM__DRIVER__ETH__PHY.html#a9a337b7659c6f77a769ec488200a89f2", null ],
    [ "SetMode", "struct__ARM__DRIVER__ETH__PHY.html#ad2792988ee6f4292ef3c34e20375032c", null ],
    [ "GetLinkState", "struct__ARM__DRIVER__ETH__PHY.html#a4559a47b09932778491d8be62e4d4cee", null ],
    [ "GetLinkInfo", "struct__ARM__DRIVER__ETH__PHY.html#a05cf301bac6cd3dce83ad4960eecfe4a", null ]
];