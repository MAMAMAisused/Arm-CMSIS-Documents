var struct__ARM__DRIVER__NAND =
[
    [ "GetVersion", "struct__ARM__DRIVER__NAND.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__NAND.html#a8811430d6e238db9ec24599777ed919f", null ],
    [ "Initialize", "struct__ARM__DRIVER__NAND.html#a59ed188bc9651c6fcb24b53de54888d5", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__NAND.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__NAND.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "DevicePower", "struct__ARM__DRIVER__NAND.html#ac0d1a0c0fcd622c343b28b53fb40ca2a", null ],
    [ "WriteProtect", "struct__ARM__DRIVER__NAND.html#acff56f461153459a3e34193b8c77b794", null ],
    [ "ChipEnable", "struct__ARM__DRIVER__NAND.html#a13f8d5c44931ed1bbcdab21aa1bb47b5", null ],
    [ "GetDeviceBusy", "struct__ARM__DRIVER__NAND.html#a8e6366f18da9db8a2fe8184462ae3966", null ],
    [ "SendCommand", "struct__ARM__DRIVER__NAND.html#abc99af74f5ec91640ed38b072546ad53", null ],
    [ "SendAddress", "struct__ARM__DRIVER__NAND.html#a04a177f16027822d9fcc16f4216065cd", null ],
    [ "ReadData", "struct__ARM__DRIVER__NAND.html#afc243d4540998915eeb7843694cf0732", null ],
    [ "WriteData", "struct__ARM__DRIVER__NAND.html#ab5bfc5db363466d03ef7ccf5ddb8ebc3", null ],
    [ "ExecuteSequence", "struct__ARM__DRIVER__NAND.html#aa99fc0af36639cceb948f15f7deea1f0", null ],
    [ "AbortSequence", "struct__ARM__DRIVER__NAND.html#a21afadc1af74b8132309e6618a99e406", null ],
    [ "Control", "struct__ARM__DRIVER__NAND.html#ae44a90a8d614797f79dd0ee522f9ea49", null ],
    [ "GetStatus", "struct__ARM__DRIVER__NAND.html#a1680162338fde8af4c775401af7d89e1", null ],
    [ "InquireECC", "struct__ARM__DRIVER__NAND.html#a49bdba28d601a360236108457212f39d", null ]
];