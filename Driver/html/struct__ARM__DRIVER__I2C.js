var struct__ARM__DRIVER__I2C =
[
    [ "GetVersion", "struct__ARM__DRIVER__I2C.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__I2C.html#a86f82e267df2bfdec296fb854879d9fe", null ],
    [ "Initialize", "struct__ARM__DRIVER__I2C.html#a66df734118a947c6af78cee691acd874", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__I2C.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__I2C.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "MasterTransmit", "struct__ARM__DRIVER__I2C.html#a46fe1f2c6a46d58f6a60a85eb114f310", null ],
    [ "MasterReceive", "struct__ARM__DRIVER__I2C.html#a9f08b2b4fbce55d14e0014fb7e439555", null ],
    [ "SlaveTransmit", "struct__ARM__DRIVER__I2C.html#aea696adcad73d09634c8697962dd235a", null ],
    [ "SlaveReceive", "struct__ARM__DRIVER__I2C.html#af60056ebbd9c8fda5df7c31ab98218d5", null ],
    [ "GetDataCount", "struct__ARM__DRIVER__I2C.html#abf92b62f99a9267793e17a650b672c92", null ],
    [ "Control", "struct__ARM__DRIVER__I2C.html#aada421ec80747aaff075970a86743689", null ],
    [ "GetStatus", "struct__ARM__DRIVER__I2C.html#acf05386a26cce90b30e4ef797e3b16a2", null ]
];