var Driver__Storage_8h_struct__ARM__STORAGE__BLOCK__ATTRIBUTES =
[
    [ "erasable", "Driver__Storage_8h.html#ab350afb2119388ebd3f096bd24f019ef", null ],
    [ "programmable", "Driver__Storage_8h.html#a7f4aebaeeb818b0e2a0592f96559fa1b", null ],
    [ "executable", "Driver__Storage_8h.html#a910da8e2e909faab1d32cca7f8b05656", null ],
    [ "protectable", "Driver__Storage_8h.html#a7c8899f46a471e1b7698a75a759e67c1", null ],
    [ "reserved", "Driver__Storage_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ],
    [ "erase_unit", "Driver__Storage_8h.html#aac21d3f798c9fcad415b99cf4157935a", null ],
    [ "protection_unit", "Driver__Storage_8h.html#ae319384dfb356cbb0b08ccbf8c04a590", null ]
];