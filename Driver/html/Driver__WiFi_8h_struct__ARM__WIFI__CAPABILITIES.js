var Driver__WiFi_8h_struct__ARM__WIFI__CAPABILITIES =
[
    [ "station", "Driver__WiFi_8h.html#a3c9e76f6560429efd6dcc41ffd4b67b9", null ],
    [ "ap", "Driver__WiFi_8h.html#ae980bcb85233b9c5904072246ac71dd7", null ],
    [ "station_ap", "Driver__WiFi_8h.html#ad280904e2429375205c09e53fe8cdcb1", null ],
    [ "wps_station", "Driver__WiFi_8h.html#a87476544767703fc9f45f81b16cd4429", null ],
    [ "wps_ap", "Driver__WiFi_8h.html#a7689c7dd824016b1962854a86d23d141", null ],
    [ "event_ap_connect", "Driver__WiFi_8h.html#a9fc191093a9bc356c3d85e9cbf185620", null ],
    [ "event_ap_disconnect", "Driver__WiFi_8h.html#a18a7e7fe3a4f48548af6db6c825cc427", null ],
    [ "event_eth_rx_frame", "Driver__WiFi_8h.html#ac4c6eadec013cf8a11b8eca66b809431", null ],
    [ "bypass_mode", "Driver__WiFi_8h.html#adbcc47408fb8c50d262a3be535108186", null ],
    [ "ip", "Driver__WiFi_8h.html#a69ddb9c845da426f636d9dd0dbed4e7e", null ],
    [ "ip6", "Driver__WiFi_8h.html#aad13478539ad093fabafd1804acac4a2", null ],
    [ "ping", "Driver__WiFi_8h.html#aef01bbaa48bdb4fba71684d8f31eb0b1", null ],
    [ "reserved", "Driver__WiFi_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ]
];