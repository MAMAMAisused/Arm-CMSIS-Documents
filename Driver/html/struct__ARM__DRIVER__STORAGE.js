var struct__ARM__DRIVER__STORAGE =
[
    [ "GetVersion", "struct__ARM__DRIVER__STORAGE.html#a6b4d67690bd7558999ebf652fdf7cd0c", null ],
    [ "GetCapabilities", "struct__ARM__DRIVER__STORAGE.html#a16f868c6c59b5c33d8a9c6ebd0d76532", null ],
    [ "Initialize", "struct__ARM__DRIVER__STORAGE.html#af49fac4188eeee51d397449b1af9e6bb", null ],
    [ "Uninitialize", "struct__ARM__DRIVER__STORAGE.html#ad3dd52a2d92927acdafbcc89e2d0b71b", null ],
    [ "PowerControl", "struct__ARM__DRIVER__STORAGE.html#a5acc5c92826d90b3a9cb1f1688d472fe", null ],
    [ "ReadData", "struct__ARM__DRIVER__STORAGE.html#afb52d80def8729e1b6fd098461b34ce9", null ],
    [ "ProgramData", "struct__ARM__DRIVER__STORAGE.html#a8b34a6f69fedc5ec64e96b4ac0bb3d9e", null ],
    [ "Erase", "struct__ARM__DRIVER__STORAGE.html#a57041cb16627979c340f46c01a8bebdc", null ],
    [ "EraseAll", "struct__ARM__DRIVER__STORAGE.html#a58acffddaf55863789ed5a9a1e62c28f", null ],
    [ "GetStatus", "struct__ARM__DRIVER__STORAGE.html#a132e9e6d768e1c3cf159e2dff26ee354", null ],
    [ "GetInfo", "struct__ARM__DRIVER__STORAGE.html#ad8b4f7c3abf331af51069e7b01018a7c", null ],
    [ "ResolveAddress", "struct__ARM__DRIVER__STORAGE.html#a9d951d8df4a16a9d089eed8876b0195f", null ],
    [ "GetNextBlock", "struct__ARM__DRIVER__STORAGE.html#aff014205f3859012e888b1f86e084fa9", null ],
    [ "GetBlock", "struct__ARM__DRIVER__STORAGE.html#a55eabb1771dbc1f040eff67f30695385", null ]
];