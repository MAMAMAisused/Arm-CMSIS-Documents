var Driver__CAN_8h_struct__ARM__CAN__OBJ__CAPABILITIES =
[
    [ "tx", "Driver__CAN_8h.html#a9706173b2ed538efeb5ee4a952e2272f", null ],
    [ "rx", "Driver__CAN_8h.html#a895532773c3204e1538191f155c7bac8", null ],
    [ "rx_rtr_tx_data", "Driver__CAN_8h.html#a8a41139926d7c032247458d055071fda", null ],
    [ "tx_rtr_rx_data", "Driver__CAN_8h.html#a1debac19545140bdfe3c5fa8d53f1863", null ],
    [ "multiple_filters", "Driver__CAN_8h.html#a3662fb9a8fb81212043cadd90da704af", null ],
    [ "exact_filtering", "Driver__CAN_8h.html#a886337af58da4f995529eba228fb9b7a", null ],
    [ "range_filtering", "Driver__CAN_8h.html#a96dcf869f4adc9cec686630082c7c60a", null ],
    [ "mask_filtering", "Driver__CAN_8h.html#a2aa0e772d6cb8c30bb76ce1324423464", null ],
    [ "message_depth", "Driver__CAN_8h.html#a5a782fc223b0ea5034c6676eaec6f2d4", null ],
    [ "reserved", "Driver__CAN_8h.html#aa43c4c21b173ada1b6b7568956f0d650", null ]
];