/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS-Core (Cortex-M)", "index.html", [
    [ "Overview", "index.html", [
      [ "Processor Support", "index.html#ref_v6-v8M", [
        [ "CMSIS-Core (Cortex-M) in ARM::CMSIS Pack", "index.html#autotoc_md0", null ],
        [ "Cortex-M Reference Manuals", "index.html#ref_man_sec", null ],
        [ "Armv8-M and Armv8.1-M Architecture", "index.html#ARMv8M", null ]
      ] ],
      [ "Tested and Verified Toolchains", "index.html#tested_tools_sec", null ]
    ] ],
    [ "Revision History of CMSIS-Core (Cortex-M)", "core_revisionHistory.html", null ],
    [ "Using CMSIS in Embedded Applications", "using_pg.html", "using_pg" ],
    [ "Using TrustZone for Armv8-M", "using_TrustZone_pg.html", [
      [ "Simplified Use Case with TrustZone", "using_TrustZone_pg.html#useCase_TrustZone", [
        [ "Program Examples", "using_TrustZone_pg.html#Example_TrustZone", null ]
      ] ],
      [ "Programmers Model with TrustZone", "using_TrustZone_pg.html#Model_TrustZone", null ],
      [ "CMSIS Files for TrustZone", "using_TrustZone_pg.html#CMSIS_Files_TrustZone", [
        [ "RTOS Thread Context Management", "using_TrustZone_pg.html#RTOS_TrustZone", null ]
      ] ]
    ] ],
    [ "CMSIS-Core Device Templates", "templates_pg.html", "templates_pg" ],
    [ "MISRA-C Deviations", "coreMISRA_Exceptions_pg.html", null ],
    [ "Register Mapping", "regMap_pg.html", null ],
    [ "Todo List", "todo.html", null ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Reference", "modules.html", "modules" ],
    [ "Data Structures", "annotated.html", "annotated" ],
    [ "Data Fields", "functions.html", [
      [ "All", "functions.html", null ],
      [ "Variables", "functions_vars.html", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"group__intrinsic__SIMD__gr.html#gad0bf46373a1c05aabf64517e84be5984",
"structTPI__Type.html#a3f80dd93f6bab6524603a7aa58de9a30"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';