var indexSectionsWithContent =
{
  0: "_abcdefhilmnopqrstuvwxz",
  1: "acdfimnstx",
  2: "cmortu",
  3: "_ainst",
  4: "_abcdefhilmnpqrstuvwz",
  5: "i",
  6: "bdhmnpsuw",
  7: "_",
  8: "cdfimnrstv",
  9: "bcdmorstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

