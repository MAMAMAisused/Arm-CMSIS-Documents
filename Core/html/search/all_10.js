var searchData=
[
  ['sau_20functions_420',['SAU Functions',['../group__sau__trustzone__functions.html',1,'']]],
  ['scb_5fcleandcache_421',['SCB_CleanDCache',['../group__Dcache__functions__m7.html#ga55583e3065c6eabca204b8b89b121c4c',1,'core_cm7.txt']]],
  ['scb_5fcleandcache_5fby_5faddr_422',['SCB_CleanDCache_by_Addr',['../group__Dcache__functions__m7.html#ga696fadbf7b9cc71dad42fab61873a40d',1,'core_cm7.txt']]],
  ['scb_5fcleaninvalidatedcache_423',['SCB_CleanInvalidateDCache',['../group__Dcache__functions__m7.html#ga1b741def9e3b2ca97dc9ea49b8ce505c',1,'core_cm7.txt']]],
  ['scb_5fcleaninvalidatedcache_5fby_5faddr_424',['SCB_CleanInvalidateDCache_by_Addr',['../group__Dcache__functions__m7.html#ga630131b2572eaa16b569ed364dfc895e',1,'core_cm7.txt']]],
  ['scb_5fdisabledcache_425',['SCB_DisableDCache',['../group__Dcache__functions__m7.html#ga6468170f90d270caab8116e7a4f0b5fe',1,'core_cm7.txt']]],
  ['scb_5fdisableicache_426',['SCB_DisableICache',['../group__Icache__functions__m7.html#gaba757390852f95b3ac2d8638c717d8d8',1,'core_cm7.txt']]],
  ['scb_5fenabledcache_427',['SCB_EnableDCache',['../group__Dcache__functions__m7.html#ga63aa640d9006021a796a5dcf9c7180b6',1,'core_cm7.txt']]],
  ['scb_5fenableicache_428',['SCB_EnableICache',['../group__Icache__functions__m7.html#gaf9e7c6c8e16ada1f95e5bf5a03505b68',1,'core_cm7.txt']]],
  ['scb_5fgetfputype_429',['SCB_GetFPUType',['../group__fpu__functions.html#ga6bcad99ce80a0e7e4ddc6f2379081756',1,'Ref_FPU.txt']]],
  ['scb_5finvalidatedcache_430',['SCB_InvalidateDCache',['../group__Dcache__functions__m7.html#gace2d30db08887d0bdb818b8a785a5ce6',1,'core_cm7.txt']]],
  ['scb_5finvalidatedcache_5fby_5faddr_431',['SCB_InvalidateDCache_by_Addr',['../group__Dcache__functions__m7.html#ga503ef7ef58c0773defd15a82f6336c09',1,'core_cm7.txt']]],
  ['scb_5finvalidateicache_432',['SCB_InvalidateICache',['../group__Icache__functions__m7.html#ga50d373a785edd782c5de5a3b55e30ff3',1,'core_cm7.txt']]],
  ['scb_5ftype_433',['SCB_Type',['../structSCB__Type.html',1,'']]],
  ['scnscb_5ftype_434',['SCnSCB_Type',['../structSCnSCB__Type.html',1,'']]],
  ['scr_435',['SCR',['../structSCB__Type.html#a3a4840c6fa4d1ee75544f4032c88ec34',1,'SCB_Type']]],
  ['securefault_5firqn_436',['SecureFault_IRQn',['../group__NVIC__gr.html#gga666eb0caeb12ec0e281415592ae89083a9cda5594d898247bfa9d16ad966724da',1,'Ref_NVIC.txt']]],
  ['shcsr_437',['SHCSR',['../structSCB__Type.html#a7b5ae9741a99808043394c4743b635c4',1,'SCB_Type']]],
  ['shp_438',['SHP',['../structSCB__Type.html#a85768f4b3dbbc41fd760041ee1202162',1,'SCB_Type']]],
  ['sleepcnt_439',['SLEEPCNT',['../structDWT__Type.html#a416a54e2084ce66e5ca74f152a5ecc70',1,'DWT_Type']]],
  ['sppr_440',['SPPR',['../structTPI__Type.html#a12f79d4e3ddc69893ba8bff890d04cc5',1,'TPI_Type']]],
  ['spsel_441',['SPSEL',['../unionCONTROL__Type.html#a8cc085fea1c50a8bd9adea63931ee8e2',1,'CONTROL_Type']]],
  ['sspsr_442',['SSPSR',['../structTPI__Type.html#a7b72598e20066133e505bb781690dc22',1,'TPI_Type']]],
  ['startup_20file_20startup_5f_3cdevice_3e_2ec_443',['Startup File startup_&lt;device&gt;.c',['../startup_c_pg.html',1,'templates_pg']]],
  ['startup_20file_20startup_5f_3cdevice_3e_2es_20_28deprecated_29_444',['Startup File startup_&lt;device&gt;.s (deprecated)',['../startup_s_pg.html',1,'templates_pg']]],
  ['stir_445',['STIR',['../structNVIC__Type.html#a37de89637466e007171c6b135299bc75',1,'NVIC_Type']]],
  ['svcall_5firqn_446',['SVCall_IRQn',['../group__NVIC__gr.html#gga666eb0caeb12ec0e281415592ae89083a4ce820b3cc6cf3a796b41aadc0cf1237',1,'Ref_NVIC.txt']]],
  ['system_20configuration_20files_20system_5f_3cdevice_3e_2ec_20and_20system_5f_3cdevice_3e_2eh_447',['System Configuration Files system_&lt;device&gt;.c and system_&lt;device&gt;.h',['../system_c_pg.html',1,'templates_pg']]],
  ['system_20and_20clock_20configuration_448',['System and Clock Configuration',['../group__system__init__gr.html',1,'']]],
  ['systemcoreclock_449',['SystemCoreClock',['../group__system__init__gr.html#gaa3cd3e43291e81e795d642b79b6088e6',1,'Ref_SystemAndClock.txt']]],
  ['systemcoreclockupdate_450',['SystemCoreClockUpdate',['../group__system__init__gr.html#gae0c36a9591fe6e9c45ecb21a794f0f0f',1,'Ref_SystemAndClock.txt']]],
  ['systeminit_451',['SystemInit',['../group__system__init__gr.html#ga93f514700ccf00d08dbdcff7f1224eb2',1,'Ref_SystemAndClock.txt']]],
  ['systick_5fconfig_452',['SysTick_Config',['../group__SysTick__gr.html#gabe47de40e9b0ad465b752297a9d9f427',1,'Ref_Systick.txt']]],
  ['systick_20timer_20_28systick_29_453',['Systick Timer (SYSTICK)',['../group__SysTick__gr.html',1,'']]],
  ['systick_5firqn_454',['SysTick_IRQn',['../group__NVIC__gr.html#gga666eb0caeb12ec0e281415592ae89083a6dbff8f8543325f3474cbae2446776e7',1,'Ref_NVIC.txt']]],
  ['systick_20functions_455',['SysTick Functions',['../group__systick__trustzone__functions.html',1,'']]],
  ['systick_5ftype_456',['SysTick_Type',['../structSysTick__Type.html',1,'']]]
];
