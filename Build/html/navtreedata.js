/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS-Build", "index.html", [
    [ "Overview", "index.html", [
      [ "Components of CMSIS-Build", "index.html#CB_Components", null ]
    ] ],
    [ "Revision history", "build_revisionHistory.html", null ],
    [ "Command Line Build", "CmdLineBuild.html", "CmdLineBuild" ],
    [ "Project Description (*.cprj) Format", "cprjFormat_pg.html", "cprjFormat_pg" ],
    [ "CMSIS-VIO Virtual I/O", "vio_pg.html", "vio_pg" ],
    [ "Todo List", "todo.html", null ],
    [ "Data Structures", "annotated.html", "annotated" ],
    [ "Data Fields", "functions.html", [
      [ "All", "functions.html", null ],
      [ "Variables", "functions_vars.html", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"CmdLineBuild.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';