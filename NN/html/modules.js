var modules =
[
    [ "Convolutional Neural Network Example", "group__CNNExample.html", [
      [ "Introduction", "index.html#autotoc_md0", null ],
      [ "Function Classification", "index.html#autotoc_md1", null ],
      [ "Block Diagram", "index.html#autotoc_md2", null ],
      [ "Examples", "index.html#autotoc_md3", null ],
      [ "Pre-processor Macros", "index.html#autotoc_md4", null ],
      [ "Copyright Notice", "index.html#autotoc_md5", null ]
    ] ],
    [ "Gated Recurrent Unit Example", "group__GRUExample.html", null ],
    [ "Neural Network Functions", "group__groupNN.html", "group__groupNN" ],
    [ "Neural Network Data Conversion Functions", "group__nndata__convert.html", "group__nndata__convert" ],
    [ "Basic Math Functions for Neural Network Computation", "group__NNBasicMath.html", "group__NNBasicMath" ]
];