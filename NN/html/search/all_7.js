var searchData=
[
  ['half_5fwords_235',['half_words',['../unionarm__nnword.html#a9b5e49e4e2c4b7203e07b305386bb2ba',1,'arm_nnword']]],
  ['hidden_5fbias_236',['hidden_bias',['../namespacepara__gen.html#a6c3df4809966df5892a62f2d45b7296e',1,'para_gen']]],
  ['hidden_5fstate_5fbias_237',['HIDDEN_STATE_BIAS',['../arm__nnexamples__gru__test__data_8h.html#a73d4e38a4aa9fa9d761d08d33b3a07fd',1,'HIDDEN_STATE_BIAS():&#160;arm_nnexamples_gru_test_data.h'],['../arm__nnexamples__gru_8cpp.html#a40dda695923891899cb86b2f01bfd98a',1,'hidden_state_bias():&#160;arm_nnexamples_gru.cpp']]],
  ['hidden_5fstate_5fweight_5fx2_238',['HIDDEN_STATE_WEIGHT_X2',['../arm__nnexamples__gru__test__data_8h.html#a9ab9fa603208647d992b3367a757979b',1,'arm_nnexamples_gru_test_data.h']]],
  ['hidden_5fstate_5fweight_5fx4_239',['HIDDEN_STATE_WEIGHT_X4',['../arm__nnexamples__gru__test__data_8h.html#a619947523492a72ab306da514e843ae3',1,'arm_nnexamples_gru_test_data.h']]],
  ['hidden_5fstate_5fweights_240',['hidden_state_weights',['../arm__nnexamples__gru_8cpp.html#ab18783e8d8449d7222ec4a64dfcc92e6',1,'arm_nnexamples_gru.cpp']]],
  ['hidden_5fweight_241',['hidden_weight',['../namespacepara__gen.html#a7207adeae39eafb274cdc7bbeeab342b',1,'para_gen']]],
  ['history_2etxt_242',['history.txt',['../history_8txt.html',1,'']]],
  ['history_5fdata_243',['history_data',['../namespacepara__gen.html#a60d92bbdf1559b05d5d14e52146cec51',1,'para_gen.history_data()'],['../arm__nnexamples__gru__test__data_8h.html#a55c93e4e823bca1e77f4487d1ab9c056',1,'HISTORY_DATA():&#160;arm_nnexamples_gru_test_data.h']]]
];
