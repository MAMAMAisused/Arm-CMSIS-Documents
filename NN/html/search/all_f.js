var searchData=
[
  ['revision_20history_20of_20cmsis_2dnn_294',['Revision History of CMSIS-NN',['../ChangeLog_pg.html',1,'']]],
  ['cmakelists_2etxt_295',['CMakeLists.txt',['../ReshapeFunctions_2CMakeLists_8txt.html',1,'']]],
  ['reset_5fbias_296',['reset_bias',['../namespacepara__gen.html#a2c6d0d2efdbea88b6090a1a4c3c13a52',1,'para_gen']]],
  ['reset_5fgate_5fbias_297',['RESET_GATE_BIAS',['../arm__nnexamples__gru__test__data_8h.html#a8b4b6522d1ab0d6c51a1fe3c51d0020d',1,'RESET_GATE_BIAS():&#160;arm_nnexamples_gru_test_data.h'],['../arm__nnexamples__gru_8cpp.html#a2a9d5c9f16ee778ecc8170d8664722c7',1,'reset_gate_bias():&#160;arm_nnexamples_gru.cpp']]],
  ['reset_5fgate_5fweight_5fx2_298',['RESET_GATE_WEIGHT_X2',['../arm__nnexamples__gru__test__data_8h.html#a52f158cb32bf2b46e3d2c4b94876db75',1,'arm_nnexamples_gru_test_data.h']]],
  ['reset_5fgate_5fweight_5fx4_299',['RESET_GATE_WEIGHT_X4',['../arm__nnexamples__gru__test__data_8h.html#a177dc986b0c728df8628a205719be0ee',1,'arm_nnexamples_gru_test_data.h']]],
  ['reset_5fgate_5fweights_300',['reset_gate_weights',['../arm__nnexamples__gru_8cpp.html#ac2ae1ff19167c2bb359db2b319ca1060',1,'arm_nnexamples_gru.cpp']]],
  ['reset_5fweight_301',['reset_weight',['../namespacepara__gen.html#a9d940eaccbc2ec8d1da7d57dfabe7a6a',1,'para_gen']]],
  ['reshape_20functions_302',['Reshape Functions',['../group__Reshape.html',1,'']]],
  ['right_5fshift_303',['RIGHT_SHIFT',['../arm__nnsupportfunctions_8h.html#a26af54489c1401b91595bf0c92ef87c4',1,'arm_nnsupportfunctions.h']]],
  ['row_5fdim_304',['row_dim',['../namespacepara__gen.html#a26cc0a96186b2e6b238d783d9364f14d',1,'para_gen']]]
];
