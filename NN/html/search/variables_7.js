var searchData=
[
  ['reset_5fbias_541',['reset_bias',['../namespacepara__gen.html#a2c6d0d2efdbea88b6090a1a4c3c13a52',1,'para_gen']]],
  ['reset_5fgate_5fbias_542',['reset_gate_bias',['../arm__nnexamples__gru_8cpp.html#a2a9d5c9f16ee778ecc8170d8664722c7',1,'arm_nnexamples_gru.cpp']]],
  ['reset_5fgate_5fweights_543',['reset_gate_weights',['../arm__nnexamples__gru_8cpp.html#ac2ae1ff19167c2bb359db2b319ca1060',1,'arm_nnexamples_gru.cpp']]],
  ['reset_5fweight_544',['reset_weight',['../namespacepara__gen.html#a9d940eaccbc2ec8d1da7d57dfabe7a6a',1,'para_gen']]],
  ['row_5fdim_545',['row_dim',['../namespacepara__gen.html#a26cc0a96186b2e6b238d783d9364f14d',1,'para_gen']]]
];
