var searchData=
[
  ['scratch_5fbuffer_546',['scratch_buffer',['../arm__nnexamples__cifar10_8cpp.html#a19548b1fad7cb85d95f5f276873a5de3',1,'scratch_buffer():&#160;arm_nnexamples_cifar10.cpp'],['../arm__nnexamples__gru_8cpp.html#a935afa741bcc39e4c4c48b019d415d97',1,'scratch_buffer():&#160;arm_nnexamples_gru.cpp']]],
  ['sep_547',['sep',['../namespacepara__gen.html#a5efaadf5b7405970389e76e8893caa49',1,'para_gen']]],
  ['sigmoidhtable_5fq15_548',['sigmoidHTable_q15',['../arm__nn__tables_8h.html#a107a16eaa36f4392fa3ed3792ad6c2cb',1,'sigmoidHTable_q15():&#160;arm_nntables.c'],['../arm__nntables_8c.html#a107a16eaa36f4392fa3ed3792ad6c2cb',1,'sigmoidHTable_q15():&#160;arm_nntables.c']]],
  ['sigmoidltable_5fq15_549',['sigmoidLTable_q15',['../arm__nn__tables_8h.html#a69dc528cb377690d5b37accfdfd226b1',1,'sigmoidLTable_q15():&#160;arm_nntables.c'],['../arm__nntables_8c.html#a69dc528cb377690d5b37accfdfd226b1',1,'sigmoidLTable_q15():&#160;arm_nntables.c']]],
  ['sigmoidtable_5fq15_550',['sigmoidTable_q15',['../arm__nn__tables_8h.html#ab4d7b07c387a3537d4efe21da86115b0',1,'sigmoidTable_q15():&#160;arm_nntables.c'],['../arm__nntables_8c.html#ab4d7b07c387a3537d4efe21da86115b0',1,'sigmoidTable_q15():&#160;arm_nntables.c']]],
  ['sigmoidtable_5fq7_551',['sigmoidTable_q7',['../arm__nn__tables_8h.html#ae2ff3f4e7014f535358167150bdd544f',1,'sigmoidTable_q7():&#160;arm_nntables.c'],['../arm__nntables_8c.html#ae2ff3f4e7014f535358167150bdd544f',1,'sigmoidTable_q7():&#160;arm_nntables.c']]]
];
