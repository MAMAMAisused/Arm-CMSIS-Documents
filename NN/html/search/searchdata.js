var indexSectionsWithContent =
{
  0: "abcdefghilmnopqrstuvw",
  1: "a",
  2: "p",
  3: "abcfghnprs",
  4: "acdglm",
  5: "bcfhinorstuvw",
  6: "a",
  7: "a",
  8: "acdehilmnoprsu",
  9: "abcfgnprs",
  10: "crt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

