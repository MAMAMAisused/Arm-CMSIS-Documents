var searchData=
[
  ['update_5fbias_324',['update_bias',['../namespacepara__gen.html#a6a760de2d5d35ba12aa7ed6ac274a7a2',1,'para_gen']]],
  ['update_5fgate_5fbias_325',['update_gate_bias',['../arm__nnexamples__gru_8cpp.html#ac5569d687768d693618f987a91e8aee5',1,'update_gate_bias():&#160;arm_nnexamples_gru.cpp'],['../arm__nnexamples__gru__test__data_8h.html#a89f5a888f55a353a914ccf6542c41f0b',1,'UPDATE_GATE_BIAS():&#160;arm_nnexamples_gru_test_data.h']]],
  ['update_5fgate_5fweight_5fx2_326',['UPDATE_GATE_WEIGHT_X2',['../arm__nnexamples__gru__test__data_8h.html#ab753489637dfba855233303733416a73',1,'arm_nnexamples_gru_test_data.h']]],
  ['update_5fgate_5fweight_5fx4_327',['UPDATE_GATE_WEIGHT_X4',['../arm__nnexamples__gru__test__data_8h.html#abb89228c76ad50a997938f676153b77f',1,'arm_nnexamples_gru_test_data.h']]],
  ['update_5fgate_5fweights_328',['update_gate_weights',['../arm__nnexamples__gru_8cpp.html#aa2fc9b2b0449790ed7c37bab7fd3093e',1,'arm_nnexamples_gru.cpp']]],
  ['update_5fweight_329',['update_weight',['../namespacepara__gen.html#af930c2c37b3d6cc8f272f4425247d627',1,'para_gen']]],
  ['use_5fintrinsic_330',['USE_INTRINSIC',['../arm__nnfunctions_8h.html#a710b6e009261290c6151f329cf409530',1,'arm_nnfunctions.h']]],
  ['use_5fx4_331',['USE_X4',['../arm__nnexamples__gru_8cpp.html#a206812c9f4afd792e23a8d842ffa2984',1,'arm_nnexamples_gru.cpp']]]
];
