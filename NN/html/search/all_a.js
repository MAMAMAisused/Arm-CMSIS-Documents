var searchData=
[
  ['main_261',['main',['../arm__nnexamples__cifar10_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main():&#160;arm_nnexamples_cifar10.cpp'],['../arm__nnexamples__gru_8cpp.html#ae66f6b31b5ad750f1fe042a706a4e3d4',1,'main():&#160;arm_nnexamples_gru.cpp']]],
  ['mask_5fif_5fnon_5fzero_262',['MASK_IF_NON_ZERO',['../arm__nnsupportfunctions_8h.html#a0bc8558b22558613ed3ad3eb624ec7a5',1,'arm_nnsupportfunctions.h']]],
  ['mask_5fif_5fzero_263',['MASK_IF_ZERO',['../arm__nnsupportfunctions_8h.html#a76b700415a2c22cea484103b7123739b',1,'arm_nnsupportfunctions.h']]],
  ['max_264',['MAX',['../arm__nnsupportfunctions_8h.html#ad935f1ff1a50822e317bdb321ce991ad',1,'arm_nnsupportfunctions.h']]],
  ['min_265',['MIN',['../arm__nnsupportfunctions_8h.html#adcd021ac91d43a62b2cdecf9a5b971a7',1,'arm_nnsupportfunctions.h']]],
  ['mul_5fpow2_266',['MUL_POW2',['../arm__nnsupportfunctions_8h.html#a9d7332bc4331d55ada7ea19407e2a8c3',1,'arm_nnsupportfunctions.h']]],
  ['mul_5fsat_267',['MUL_SAT',['../arm__nnsupportfunctions_8h.html#a518f7e0db18bea6b61a2b88f266aef20',1,'arm_nnsupportfunctions.h']]],
  ['mul_5fsat_5fmve_268',['MUL_SAT_MVE',['../arm__nnsupportfunctions_8h.html#a6349818fec8167dff87c3fb7ca81fc1c',1,'arm_nnsupportfunctions.h']]]
];
