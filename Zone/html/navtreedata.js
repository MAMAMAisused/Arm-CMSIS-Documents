/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS-Zone", "index.html", [
    [ "Overview", "index.html#Overview", null ],
    [ "Manual Sections", "index.html#ManualSections", null ],
    [ "Introduction Video", "index.html#mainIntroVideo", null ],
    [ "Revision History of CMSIS-Zone", "zone_revisionHistory.html", null ],
    [ "CMSIS-Zone Use Cases", "UseCases.html", [
      [ "MPU Protection", "UseCases.html#UseCase_MPU", null ],
      [ "TrustZone Partitioning", "UseCases.html#UseCase_TrustZone", null ],
      [ "Multi-Core Partitioning", "UseCases.html#UseCase_MultiCore", null ]
    ] ],
    [ "CMSIS-Zone Utility", "zoneToolUsage.html", "zoneToolUsage" ],
    [ "Zone Description Format", "zoneFormat.html", "zoneFormat" ],
    [ "Generator Data Model", "GenDataModel.html", "GenDataModel" ]
  ] ]
];

var NAVTREEINDEX =
[
"GenDataModel.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';