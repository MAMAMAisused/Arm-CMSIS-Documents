var zoneFormat =
[
    [ "Overall XML structure", "zoneFormat.html#XML_Format_Schema", null ],
    [ "Top-level .rzone XML elements", "zoneFormat.html#autotoc_md0", [
      [ "Structure of .rzone XML file", "zoneFormat.html#autotoc_md1", null ]
    ] ],
    [ "Top-level .azone XML elements", "zoneFormat.html#autotoc_md2", null ],
    [ "Security Type", "zoneFormat.html#autotoc_md3", null ],
    [ "Access Type", "zoneFormat.html#autotoc_md4", null ],
    [ "Privilege Type", "zoneFormat.html#autotoc_md5", null ],
    [ "/rzone element", "xml_rzone_pg.html", "xml_rzone_pg" ],
    [ "/azone element", "xml_azone_pg.html", "xml_azone_pg" ]
];