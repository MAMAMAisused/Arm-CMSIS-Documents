/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "CMSIS-Pack", "index.html", [
    [ "Software Pack Use Cases", "index.html#SWPackVariants", [
      [ "CMSIS-Pack in ARM::CMSIS Pack", "index.html#autotoc_md0", null ]
    ] ],
    [ "Revision History of CMSIS-Pack", "pack_revisionHistory.html", null ],
    [ "Overview", "cp_Packs.html", [
      [ "Software Components", "cp_Packs.html#cp_Components", [
        [ "Technical Details", "cp_Packs.html#cp_CompDetails", null ],
        [ "Software Component Files", "cp_Packs.html#cp_CompFiles", null ],
        [ "File naming", "cp_Packs.html#cp_CompFileNaming", null ]
      ] ],
      [ "References to other software components", "cp_Packs.html#cp_Cond", null ],
      [ "Requirements", "cp_Packs.html#cp_Requirements", null ],
      [ "Central API definition", "cp_Packs.html#cp_APIDef", null ],
      [ "Inventory header file: RTE_Components.h", "cp_Packs.html#cp_RTECompH", null ],
      [ "Combining software components", "cp_Packs.html#cp_Combine", null ],
      [ "Steps to Create a Pack", "cp_Packs.html#cp_PackSteps", null ],
      [ "Pack Tutorial", "cp_Packs.html#PackTutorials", null ],
      [ "Pack Conventions", "cp_Packs.html#PackFilenames", [
        [ "Pack File Name", "cp_Packs.html#pack_filename", null ],
        [ "Software Component Cclasses", "cp_Packs.html#pack_Cclass", null ]
      ] ]
    ] ],
    [ "Pack with Software Components", "cp_SWComponents.html", [
      [ "Create PDSC File", "cp_SWComponents.html#cp_CreatePDSC", null ],
      [ "Generate a Pack", "cp_SWComponents.html#cp_GeneratePack", [
        [ "Software Component Files", "cp_SWComponents.html#autotoc_md1", null ]
      ] ],
      [ "Conditions", "cp_SWComponents.html#cp_Conditions", null ],
      [ "Variants", "cp_SWComponents.html#cp_Variants", null ],
      [ "RTE_Components.h", "cp_SWComponents.html#cp_RTEComponents_h", null ],
      [ "Bundles", "cp_SWComponents.html#cp_Bundles", null ],
      [ "Instances", "cp_SWComponents.html#cp_Instances", null ],
      [ "API Interface", "cp_SWComponents.html#cp_API", null ],
      [ "User Code Templates", "cp_SWComponents.html#cp_CodeTemplates", null ],
      [ "Example Projects", "cp_SWComponents.html#cp_Examples", null ]
    ] ],
    [ "Pack with Device Support", "createPack_DFP.html", [
      [ "Basic Device Family Pack", "createPack_DFP.html#cp_BasicDFP", null ],
      [ "System and Startup Files", "createPack_DFP.html#cp_System_Startp", null ],
      [ "System View Description File", "createPack_DFP.html#cp_SVD", null ],
      [ "Flash Programming Algorithms", "createPack_DFP.html#cp_FlashProgrammingAlgorithm", null ],
      [ "Debug Descriptions", "createPack_DFP.html#cp_debugdescription", null ],
      [ "Device Properties", "createPack_DFP.html#cp_DeviceProperties", [
        [ "DFP Use Cases", "createPack_DFP.html#autotoc_md2", null ],
        [ "Steps to Create a DFP", "createPack_DFP.html#autotoc_md3", null ],
        [ "Device Specific Software Components", "createPack_DFP.html#cp_DeviceSWComp", null ]
      ] ]
    ] ],
    [ "Pack with Board Support", "createPackBoard.html", [
      [ "Specify a Development Board", "createPackBoard.html#cp_SpecDevBoard", null ],
      [ "Create a BSP Bundle", "createPackBoard.html#cp_BundleExample", null ]
    ] ],
    [ "Pack Example", "pack_Example.html", null ],
    [ "Utilities for Creating Packs", "createPackUtil.html", "createPackUtil" ],
    [ "Publish a Pack", "createPackPublish.html", [
      [ "Prerequisites", "createPackPublish.html#cp_prerequisites", null ],
      [ "Local Installation", "createPackPublish.html#cp_LocalInstallation", null ],
      [ "Web Download", "createPackPublish.html#cp_WebDownload", null ],
      [ "Publish with Pack Index Service", "createPackPublish.html#cp_KeilComPack", null ],
      [ "Rehost pack to different URL", "createPackPublish.html#cp_PackRehosting", null ],
      [ "Web Infra-structure", "createPackPublish.html#cp_HowWebDownloadWorks", null ]
    ] ],
    [ "Pack Description (*.pdsc) Format", "packFormat.html", "packFormat" ],
    [ "Configuration Wizard Annotations", "configWizard.html", [
      [ "Annotation Rules", "configWizard.html#configWizard_annotations", null ],
      [ "Code Example", "configWizard.html#configWizard_codeExample", null ],
      [ "Tool-specific display", "configWizard.html#configWizard_display", null ]
    ] ],
    [ "Flash Programming", "flashAlgorithm.html", "flashAlgorithm" ],
    [ "Debug Setup with CMSIS-Pack", "coresight_setup.html", "coresight_setup" ],
    [ "System Description File (*.SDF) Format", "sdf_pg.html", [
      [ "SDF top level structure", "sdf_pg.html#sdf_structure", null ],
      [ "/system_description", "sdf_pg.html#sdf_element_system_description", null ],
      [ "/system_description/debug_and_trace_config", "sdf_pg.html#sdf_element_debug_and_trace_config", null ],
      [ "/system_description/debug_and_trace_config/debug", "sdf_pg.html#sdf_element_debug", null ],
      [ "/system_description/.../config_item", "sdf_pg.html#sdf_element_config_item", [
        [ "Debug element", "sdf_pg.html#autotoc_md17", null ],
        [ "Device element", "sdf_pg.html#autotoc_md18", null ],
        [ "Trace element", "sdf_pg.html#autotoc_md19", null ]
      ] ],
      [ "/system_description/debug_and_trace_config/trace", "sdf_pg.html#sdf_element_trace", null ],
      [ "/system_description/debug_and_trace_config/trace/trace_capture", "sdf_pg.html#sdf_element_trace_capture", null ],
      [ "/system_description/platform", "sdf_pg.html#sdf_element_platform", null ],
      [ "/system_description/platform/scanchain", "sdf_pg.html#sdf_element_scanchain", null ],
      [ "/system_description/platform/scanchain/dap", "sdf_pg.html#sdf_element_dap", null ],
      [ "/system_description/platform/scanchain/device", "sdf_pg.html#sdf_element_device", null ],
      [ "/system_description/platform/scanchain/../../config_items", "sdf_pg.html#sdf_element_config_items", null ],
      [ "/system_description/platform/scanchain/../../device_info_items", "sdf_pg.html#sdf_element_device_info_items", null ],
      [ "/system_description/platform/scanchain/../../device_info_items/device_info_item", "sdf_pg.html#sdf_element_device_info_item", null ],
      [ "/system_description/platform/topology", "sdf_pg.html#sdf_element_topology", null ],
      [ "/system_description/platform/topology/topology_link", "sdf_pg.html#sdf_element_topology_link", null ],
      [ "/system_description/platform/clusters", "sdf_pg.html#sdf_element_clusters", null ],
      [ "/system_description/platform/clusters/cluster", "sdf_pg.html#sdf_element_cluster", null ]
    ] ],
    [ "Pack Index Files", "packIndexFile.html", [
      [ "Package Index File (pidx)", "packIndexFile.html#pidxFile", null ],
      [ "Vendor Index File (vidx)", "packIndexFile.html#vidxFile", null ],
      [ "CMSIS-Pack Index Schema File", "packIndexFile.html#packIndexSchema", null ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"algorithmFunc.html",
"sdf_pg.html#sdf_element_config_item"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';