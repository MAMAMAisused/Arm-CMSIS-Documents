var createPackUtil =
[
    [ "packChk", "packChk.html", [
      [ "BSP Use Cases", "createPackBoard.html#autotoc_md4", null ],
      [ "PDSC Example File", "pack_Example.html#autotoc_md5", null ],
      [ "Directory CMSIS_Driver", "pack_Example.html#autotoc_md6", null ],
      [ "Directory Device", "pack_Example.html#autotoc_md7", null ],
      [ "Directory Documents", "pack_Example.html#autotoc_md8", [
        [ "Operation", "packChk.html#autotoc_md9", null ]
      ] ],
      [ "Error and Warning Messages", "packChk.html#packChkMessages", [
        [ "Invocation Errors", "packChk.html#autotoc_md10", null ],
        [ "Validation Messages", "packChk.html#autotoc_md11", null ],
        [ "XML Reader Errors", "packChk.html#autotoc_md12", null ],
        [ "Model Errors", "packChk.html#autotoc_md13", null ]
      ] ]
    ] ],
    [ "XML Validation and Editors", "cp_Editors.html", null ],
    [ "ZIP Compression Tools", "cp_ZIPTool.html", null ],
    [ "Bash Script gen_pack.sh", "bash_script.html", null ]
];